1. Open an EAST-ADL model
2. Right mouse-click on the system model element and select "Generate ReSA Ref. Model" from the context menu.
The command generates the datatype specification in platform:/plugin/org.verispec.resa.refmodel/models/system_model.resa
3. Right mouse-click on the a requirement element and select "Edit in ReSA" to specify a requirement in ReSA
The command opens a pop-up window (the embedded ReSA editor)
4. To refer model elements from the EAST-ADL model (or datatyped elements), do Ctrl+SPACE