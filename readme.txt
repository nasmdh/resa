ReSA Toolchain
---------------
The ReSA Toolchain contains a requirements specifications editor plugin that is developed for EATOP IDE. The edior implements the constrained natural language, ReSA, which is tailored to embedded systems domain. The language is desined to improve comprehensibility and reduce ambiguity of specifications via syntactic rules. Moreover, the language implements a minimal type system that checks the semantic soundness of clauses by checkings arugments of predicates (that is, the main verb) and operators (such as prepositions and conjunctions).

The toolchain has the following features:
	- A rich text editor
	- A content-completion feature that triggeres when Ctrl+SPACE is pressed on the editor
	- A type-checking error display that shows error message whenever there are syntact and semantic errors (or type errors)
	- A consistency-checking feature that checks the consistency of specifications using a SAT-based approach
	
INSTALLATION
-------------
1. Pre-requisite: 
	1. install Xtext Redistributable from the update site http://download.eclipse.org/modeling/tmf/xtext/updates/composite/releases/
	2. click the "Install New Software" command, located under the Help menu to open the Install-Available Software window
	3. insert the update site address in the "Work with" property of the Available Software window
	4. locate the Xtext redistributable by inserting "Xtext Redis" key word in filter field, install the latest version
	5. follow the wizard to install the plugin
2. Get the ReSA Toolchain plugin offline
3. Install the plugin via the "Install New Software", following 1.1 and 1.2
	1. click Add button, found to the right of "Available Software" window
	2. In the "Add Repository" pop-up window, click the "Archieve" button and locate the ReSA Toolchain plugin from local disk
	3. follow the wizard to install the plugin
	
USAGE
------
1. Open an EAST-ADL model
2. Do right-mouse-click on the system model element and select "Generate ReSA Reference Model" from the context menu.
	- The command generates the datatype specification in platform:/plugin/org.verispec.resa.refmodel/models/system_model.resa
3. Do right-mouse-click on the requirement element and select "Edit in ReSA" or press the pen icon in the toolbar to specify requirements in ReSA
	- The command opens a pop-up window (the embedded ReSA editor)
4. Specify requirements using the editor. Save it as a draft and close it when you are done
5. To refer to model elements from the EAST-ADL in-memory model (or datatyped elements), do Ctrl+SPACE on the editor view

note: the consistency-checking feature is not available in this version. Rather, it is available as as standalow in the form mimal Eclipse ide, to be found from http://resaapplication-env-tomcat.us-east-2.elasticbeanstalk.com/

CONTACT
-------
nesredin.mahmud@mdh.se/gmail.se