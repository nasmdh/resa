/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.ide;

import com.google.inject.Injector;
import org.verispec.resa.ResaStandaloneSetup;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class ResaIdeSetup extends ResaStandaloneSetup {
  @Override
  public Injector createInjector() {
    throw new Error("Unresolved compilation problems:"
      + "\nType mismatch: cannot convert from ResaIdeModule to Module");
  }
}
