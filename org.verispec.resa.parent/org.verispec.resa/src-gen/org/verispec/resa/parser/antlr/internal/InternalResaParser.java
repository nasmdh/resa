package org.verispec.resa.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.verispec.resa.services.ResaGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalResaParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_ANY_OTHER", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "'.'", "'.*'", "'>'", "'if'", "'then'", "'else'", "'endif'", "'>>'", "','", "'['", "']'", "'and'", "'or'", "'('", "')'", "'not'", "'be'", "'set_to'", "'in_order_to'", "'The_status_of'", "'The_state_of'", "'The_mode_of'", "'does_not'", "'do_not'", "'doesn\\'t'", "'don\\'t'", "'to'", "'for'", "'by'", "'with'", "'is'", "'shall'", "':var'", "'equal_to'", "'less_than'", "'greater_than'", "'less_than_or_equalto'", "'greater_than_or_equal_to'", "'between'", "'within'", "'after'", "'before'", "'at'", "'every'", "'micsec'", "'msec'", "'sec'", "'min'", "'at_least'", "'at_most'", "'times'", "'because'", "'as'", "'in_order_that'", "'since'", "'in_case'", "'provided_that'", "'unless'", "'as_soon_as'", "'as_long_as'", "'once'", "'till'", "'until'", "'when'", "'while'", "':system'", "':device'", "':parameter'", "':signal'", "':outsignal'", "':insignal'", "':state'", "':mode'", "':event'", "':status'", "':user'", "':entity'", "':inparameter'", "':outparameter'", "':indevice'", "':outdevice'", "':attribute'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=7;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=5;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalResaParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalResaParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalResaParser.tokenNames; }
    public String getGrammarFileName() { return "InternalResa.g"; }



     	private ResaGrammarAccess grammarAccess;

        public InternalResaParser(TokenStream input, ResaGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ReqsSpecs";
       	}

       	@Override
       	protected ResaGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleReqsSpecs"
    // InternalResa.g:64:1: entryRuleReqsSpecs returns [EObject current=null] : iv_ruleReqsSpecs= ruleReqsSpecs EOF ;
    public final EObject entryRuleReqsSpecs() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReqsSpecs = null;


        try {
            // InternalResa.g:64:50: (iv_ruleReqsSpecs= ruleReqsSpecs EOF )
            // InternalResa.g:65:2: iv_ruleReqsSpecs= ruleReqsSpecs EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReqsSpecsRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReqsSpecs=ruleReqsSpecs();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReqsSpecs; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReqsSpecs"


    // $ANTLR start "ruleReqsSpecs"
    // InternalResa.g:71:1: ruleReqsSpecs returns [EObject current=null] : ( (lv_reqspecs_0_0= ruleReqSpec ) )* ;
    public final EObject ruleReqsSpecs() throws RecognitionException {
        EObject current = null;

        EObject lv_reqspecs_0_0 = null;



        	enterRule();

        try {
            // InternalResa.g:77:2: ( ( (lv_reqspecs_0_0= ruleReqSpec ) )* )
            // InternalResa.g:78:2: ( (lv_reqspecs_0_0= ruleReqSpec ) )*
            {
            // InternalResa.g:78:2: ( (lv_reqspecs_0_0= ruleReqSpec ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=13 && LA1_0<=14)||LA1_0==18) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalResa.g:79:3: (lv_reqspecs_0_0= ruleReqSpec )
            	    {
            	    // InternalResa.g:79:3: (lv_reqspecs_0_0= ruleReqSpec )
            	    // InternalResa.g:80:4: lv_reqspecs_0_0= ruleReqSpec
            	    {
            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getReqsSpecsAccess().getReqspecsReqSpecParserRuleCall_0());
            	      			
            	    }
            	    pushFollow(FOLLOW_3);
            	    lv_reqspecs_0_0=ruleReqSpec();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				if (current==null) {
            	      					current = createModelElementForParent(grammarAccess.getReqsSpecsRule());
            	      				}
            	      				add(
            	      					current,
            	      					"reqspecs",
            	      					lv_reqspecs_0_0,
            	      					"org.verispec.resa.Resa.ReqSpec");
            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReqsSpecs"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalResa.g:100:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalResa.g:100:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalResa.g:101:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalResa.g:107:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalResa.g:113:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalResa.g:114:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalResa.g:114:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalResa.g:115:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
              		
            }
            // InternalResa.g:122:3: (kw= '.' this_ID_2= RULE_ID )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalResa.g:123:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,11,FOLLOW_5); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalResa.g:140:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalResa.g:140:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalResa.g:141:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedNameWithWildcard.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalResa.g:147:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalResa.g:153:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalResa.g:154:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalResa.g:154:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalResa.g:155:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_6);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_QualifiedName_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalResa.g:165:3: (kw= '.*' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalResa.g:166:4: kw= '.*'
                    {
                    kw=(Token)match(input,12,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleReqSpec"
    // InternalResa.g:176:1: entryRuleReqSpec returns [EObject current=null] : iv_ruleReqSpec= ruleReqSpec EOF ;
    public final EObject entryRuleReqSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReqSpec = null;


        try {
            // InternalResa.g:176:48: (iv_ruleReqSpec= ruleReqSpec EOF )
            // InternalResa.g:177:2: iv_ruleReqSpec= ruleReqSpec EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReqSpecRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReqSpec=ruleReqSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReqSpec; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReqSpec"


    // $ANTLR start "ruleReqSpec"
    // InternalResa.g:183:1: ruleReqSpec returns [EObject current=null] : ( ( (lv_complexspec_0_0= ruleComplexSpec ) ) | ( (lv_simplespec_1_0= ruleSimpleSpec ) ) | ( (lv_structuredspec_2_0= ruleStructuredSpec ) ) ) ;
    public final EObject ruleReqSpec() throws RecognitionException {
        EObject current = null;

        EObject lv_complexspec_0_0 = null;

        EObject lv_simplespec_1_0 = null;

        EObject lv_structuredspec_2_0 = null;



        	enterRule();

        try {
            // InternalResa.g:189:2: ( ( ( (lv_complexspec_0_0= ruleComplexSpec ) ) | ( (lv_simplespec_1_0= ruleSimpleSpec ) ) | ( (lv_structuredspec_2_0= ruleStructuredSpec ) ) ) )
            // InternalResa.g:190:2: ( ( (lv_complexspec_0_0= ruleComplexSpec ) ) | ( (lv_simplespec_1_0= ruleSimpleSpec ) ) | ( (lv_structuredspec_2_0= ruleStructuredSpec ) ) )
            {
            // InternalResa.g:190:2: ( ( (lv_complexspec_0_0= ruleComplexSpec ) ) | ( (lv_simplespec_1_0= ruleSimpleSpec ) ) | ( (lv_structuredspec_2_0= ruleStructuredSpec ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt4=1;
                }
                break;
            case 13:
                {
                alt4=2;
                }
                break;
            case 14:
                {
                alt4=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalResa.g:191:3: ( (lv_complexspec_0_0= ruleComplexSpec ) )
                    {
                    // InternalResa.g:191:3: ( (lv_complexspec_0_0= ruleComplexSpec ) )
                    // InternalResa.g:192:4: (lv_complexspec_0_0= ruleComplexSpec )
                    {
                    // InternalResa.g:192:4: (lv_complexspec_0_0= ruleComplexSpec )
                    // InternalResa.g:193:5: lv_complexspec_0_0= ruleComplexSpec
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getReqSpecAccess().getComplexspecComplexSpecParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_complexspec_0_0=ruleComplexSpec();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getReqSpecRule());
                      					}
                      					set(
                      						current,
                      						"complexspec",
                      						lv_complexspec_0_0,
                      						"org.verispec.resa.Resa.ComplexSpec");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:211:3: ( (lv_simplespec_1_0= ruleSimpleSpec ) )
                    {
                    // InternalResa.g:211:3: ( (lv_simplespec_1_0= ruleSimpleSpec ) )
                    // InternalResa.g:212:4: (lv_simplespec_1_0= ruleSimpleSpec )
                    {
                    // InternalResa.g:212:4: (lv_simplespec_1_0= ruleSimpleSpec )
                    // InternalResa.g:213:5: lv_simplespec_1_0= ruleSimpleSpec
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getReqSpecAccess().getSimplespecSimpleSpecParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_simplespec_1_0=ruleSimpleSpec();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getReqSpecRule());
                      					}
                      					set(
                      						current,
                      						"simplespec",
                      						lv_simplespec_1_0,
                      						"org.verispec.resa.Resa.SimpleSpec");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalResa.g:231:3: ( (lv_structuredspec_2_0= ruleStructuredSpec ) )
                    {
                    // InternalResa.g:231:3: ( (lv_structuredspec_2_0= ruleStructuredSpec ) )
                    // InternalResa.g:232:4: (lv_structuredspec_2_0= ruleStructuredSpec )
                    {
                    // InternalResa.g:232:4: (lv_structuredspec_2_0= ruleStructuredSpec )
                    // InternalResa.g:233:5: lv_structuredspec_2_0= ruleStructuredSpec
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getReqSpecAccess().getStructuredspecStructuredSpecParserRuleCall_2_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_structuredspec_2_0=ruleStructuredSpec();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getReqSpecRule());
                      					}
                      					set(
                      						current,
                      						"structuredspec",
                      						lv_structuredspec_2_0,
                      						"org.verispec.resa.Resa.StructuredSpec");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReqSpec"


    // $ANTLR start "entryRuleSimpleSpec"
    // InternalResa.g:254:1: entryRuleSimpleSpec returns [EObject current=null] : iv_ruleSimpleSpec= ruleSimpleSpec EOF ;
    public final EObject entryRuleSimpleSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleSpec = null;


        try {
            // InternalResa.g:254:51: (iv_ruleSimpleSpec= ruleSimpleSpec EOF )
            // InternalResa.g:255:2: iv_ruleSimpleSpec= ruleSimpleSpec EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleSpecRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSimpleSpec=ruleSimpleSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleSpec; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleSpec"


    // $ANTLR start "ruleSimpleSpec"
    // InternalResa.g:261:1: ruleSimpleSpec returns [EObject current=null] : ( () otherlv_1= '>' ( (lv_clause_2_0= ruleClause ) ) otherlv_3= '.' ) ;
    public final EObject ruleSimpleSpec() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_clause_2_0 = null;



        	enterRule();

        try {
            // InternalResa.g:267:2: ( ( () otherlv_1= '>' ( (lv_clause_2_0= ruleClause ) ) otherlv_3= '.' ) )
            // InternalResa.g:268:2: ( () otherlv_1= '>' ( (lv_clause_2_0= ruleClause ) ) otherlv_3= '.' )
            {
            // InternalResa.g:268:2: ( () otherlv_1= '>' ( (lv_clause_2_0= ruleClause ) ) otherlv_3= '.' )
            // InternalResa.g:269:3: () otherlv_1= '>' ( (lv_clause_2_0= ruleClause ) ) otherlv_3= '.'
            {
            // InternalResa.g:269:3: ()
            // InternalResa.g:270:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getSimpleSpecAccess().getSimpleSpecAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,13,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getSimpleSpecAccess().getGreaterThanSignKeyword_1());
              		
            }
            // InternalResa.g:280:3: ( (lv_clause_2_0= ruleClause ) )
            // InternalResa.g:281:4: (lv_clause_2_0= ruleClause )
            {
            // InternalResa.g:281:4: (lv_clause_2_0= ruleClause )
            // InternalResa.g:282:5: lv_clause_2_0= ruleClause
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSimpleSpecAccess().getClauseClauseParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_clause_2_0=ruleClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSimpleSpecRule());
              					}
              					set(
              						current,
              						"clause",
              						lv_clause_2_0,
              						"org.verispec.resa.Resa.Clause");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,11,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getSimpleSpecAccess().getFullStopKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleSpec"


    // $ANTLR start "entryRuleStructuredSpec"
    // InternalResa.g:307:1: entryRuleStructuredSpec returns [EObject current=null] : iv_ruleStructuredSpec= ruleStructuredSpec EOF ;
    public final EObject entryRuleStructuredSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructuredSpec = null;


        try {
            // InternalResa.g:307:55: (iv_ruleStructuredSpec= ruleStructuredSpec EOF )
            // InternalResa.g:308:2: iv_ruleStructuredSpec= ruleStructuredSpec EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStructuredSpecRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStructuredSpec=ruleStructuredSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStructuredSpec; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructuredSpec"


    // $ANTLR start "ruleStructuredSpec"
    // InternalResa.g:314:1: ruleStructuredSpec returns [EObject current=null] : this_Conditional_0= ruleConditional ;
    public final EObject ruleStructuredSpec() throws RecognitionException {
        EObject current = null;

        EObject this_Conditional_0 = null;



        	enterRule();

        try {
            // InternalResa.g:320:2: (this_Conditional_0= ruleConditional )
            // InternalResa.g:321:2: this_Conditional_0= ruleConditional
            {
            if ( state.backtracking==0 ) {

              		newCompositeNode(grammarAccess.getStructuredSpecAccess().getConditionalParserRuleCall());
              	
            }
            pushFollow(FOLLOW_2);
            this_Conditional_0=ruleConditional();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current = this_Conditional_0;
              		afterParserOrEnumRuleCall();
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructuredSpec"


    // $ANTLR start "entryRuleConditional"
    // InternalResa.g:332:1: entryRuleConditional returns [EObject current=null] : iv_ruleConditional= ruleConditional EOF ;
    public final EObject entryRuleConditional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditional = null;


        try {
            // InternalResa.g:332:52: (iv_ruleConditional= ruleConditional EOF )
            // InternalResa.g:333:2: iv_ruleConditional= ruleConditional EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConditionalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleConditional=ruleConditional();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConditional; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditional"


    // $ANTLR start "ruleConditional"
    // InternalResa.g:339:1: ruleConditional returns [EObject current=null] : ( () otherlv_1= 'if' ( (lv_if_2_0= ruleCompoundClause ) ) otherlv_3= 'then' ( (lv_then_4_0= ruleElse ) ) (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )? otherlv_7= 'endif' ) ;
    public final EObject ruleConditional() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_if_2_0 = null;

        EObject lv_then_4_0 = null;

        EObject lv_else_6_0 = null;



        	enterRule();

        try {
            // InternalResa.g:345:2: ( ( () otherlv_1= 'if' ( (lv_if_2_0= ruleCompoundClause ) ) otherlv_3= 'then' ( (lv_then_4_0= ruleElse ) ) (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )? otherlv_7= 'endif' ) )
            // InternalResa.g:346:2: ( () otherlv_1= 'if' ( (lv_if_2_0= ruleCompoundClause ) ) otherlv_3= 'then' ( (lv_then_4_0= ruleElse ) ) (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )? otherlv_7= 'endif' )
            {
            // InternalResa.g:346:2: ( () otherlv_1= 'if' ( (lv_if_2_0= ruleCompoundClause ) ) otherlv_3= 'then' ( (lv_then_4_0= ruleElse ) ) (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )? otherlv_7= 'endif' )
            // InternalResa.g:347:3: () otherlv_1= 'if' ( (lv_if_2_0= ruleCompoundClause ) ) otherlv_3= 'then' ( (lv_then_4_0= ruleElse ) ) (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )? otherlv_7= 'endif'
            {
            // InternalResa.g:347:3: ()
            // InternalResa.g:348:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getConditionalAccess().getIFAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,14,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getConditionalAccess().getIfKeyword_1());
              		
            }
            // InternalResa.g:358:3: ( (lv_if_2_0= ruleCompoundClause ) )
            // InternalResa.g:359:4: (lv_if_2_0= ruleCompoundClause )
            {
            // InternalResa.g:359:4: (lv_if_2_0= ruleCompoundClause )
            // InternalResa.g:360:5: lv_if_2_0= ruleCompoundClause
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalAccess().getIfCompoundClauseParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_if_2_0=ruleCompoundClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalRule());
              					}
              					set(
              						current,
              						"if",
              						lv_if_2_0,
              						"org.verispec.resa.Resa.CompoundClause");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getConditionalAccess().getThenKeyword_3());
              		
            }
            // InternalResa.g:381:3: ( (lv_then_4_0= ruleElse ) )
            // InternalResa.g:382:4: (lv_then_4_0= ruleElse )
            {
            // InternalResa.g:382:4: (lv_then_4_0= ruleElse )
            // InternalResa.g:383:5: lv_then_4_0= ruleElse
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getConditionalAccess().getThenElseParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_12);
            lv_then_4_0=ruleElse();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getConditionalRule());
              					}
              					set(
              						current,
              						"then",
              						lv_then_4_0,
              						"org.verispec.resa.Resa.Else");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:400:3: (otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalResa.g:401:4: otherlv_5= 'else' ( (lv_else_6_0= ruleElse ) )
                    {
                    otherlv_5=(Token)match(input,16,FOLLOW_11); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getConditionalAccess().getElseKeyword_5_0());
                      			
                    }
                    // InternalResa.g:405:4: ( (lv_else_6_0= ruleElse ) )
                    // InternalResa.g:406:5: (lv_else_6_0= ruleElse )
                    {
                    // InternalResa.g:406:5: (lv_else_6_0= ruleElse )
                    // InternalResa.g:407:6: lv_else_6_0= ruleElse
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getConditionalAccess().getElseElseParserRuleCall_5_1_0());
                      					
                    }
                    pushFollow(FOLLOW_13);
                    lv_else_6_0=ruleElse();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getConditionalRule());
                      						}
                      						set(
                      							current,
                      							"else",
                      							lv_else_6_0,
                      							"org.verispec.resa.Resa.Else");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,17,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_7, grammarAccess.getConditionalAccess().getEndifKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditional"


    // $ANTLR start "entryRuleElse"
    // InternalResa.g:433:1: entryRuleElse returns [EObject current=null] : iv_ruleElse= ruleElse EOF ;
    public final EObject entryRuleElse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElse = null;


        try {
            // InternalResa.g:433:45: (iv_ruleElse= ruleElse EOF )
            // InternalResa.g:434:2: iv_ruleElse= ruleElse EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getElseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleElse=ruleElse();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleElse; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElse"


    // $ANTLR start "ruleElse"
    // InternalResa.g:440:1: ruleElse returns [EObject current=null] : (this_Conditional_0= ruleConditional | this_CompoundClause_1= ruleCompoundClause ) ;
    public final EObject ruleElse() throws RecognitionException {
        EObject current = null;

        EObject this_Conditional_0 = null;

        EObject this_CompoundClause_1 = null;



        	enterRule();

        try {
            // InternalResa.g:446:2: ( (this_Conditional_0= ruleConditional | this_CompoundClause_1= ruleCompoundClause ) )
            // InternalResa.g:447:2: (this_Conditional_0= ruleConditional | this_CompoundClause_1= ruleCompoundClause )
            {
            // InternalResa.g:447:2: (this_Conditional_0= ruleConditional | this_CompoundClause_1= ruleCompoundClause )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            else if ( (LA6_0==RULE_ID||(LA6_0>=RULE_STRING && LA6_0<=RULE_INT)||LA6_0==24||(LA6_0>=30 && LA6_0<=32)) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalResa.g:448:3: this_Conditional_0= ruleConditional
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElseAccess().getConditionalParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Conditional_0=ruleConditional();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Conditional_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:457:3: this_CompoundClause_1= ruleCompoundClause
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getElseAccess().getCompoundClauseParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CompoundClause_1=ruleCompoundClause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CompoundClause_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElse"


    // $ANTLR start "entryRuleComplexSpec"
    // InternalResa.g:469:1: entryRuleComplexSpec returns [EObject current=null] : iv_ruleComplexSpec= ruleComplexSpec EOF ;
    public final EObject entryRuleComplexSpec() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplexSpec = null;


        try {
            // InternalResa.g:469:52: (iv_ruleComplexSpec= ruleComplexSpec EOF )
            // InternalResa.g:470:2: iv_ruleComplexSpec= ruleComplexSpec EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComplexSpecRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComplexSpec=ruleComplexSpec();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComplexSpec; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplexSpec"


    // $ANTLR start "ruleComplexSpec"
    // InternalResa.g:476:1: ruleComplexSpec returns [EObject current=null] : (otherlv_0= '>>' ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) ) otherlv_24= '.' ) ;
    public final EObject ruleComplexSpec() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_13=null;
        Token otherlv_17=null;
        Token otherlv_21=null;
        Token otherlv_24=null;
        EObject lv_subconj_L2_1_0 = null;

        EObject lv_antecedent_L2_2_0 = null;

        EObject lv_consequent_L2_4_0 = null;

        EObject lv_subconj_L1_6_0 = null;

        EObject lv_antecedent_L1_7_0 = null;

        EObject lv_consequent_L1_9_0 = null;

        EObject lv_consequent_L1_10_0 = null;

        EObject lv_subconj_L1_11_0 = null;

        EObject lv_antecedent_L1_12_0 = null;

        EObject lv_consequent_L1_14_0 = null;

        EObject lv_subconj_L1_15_0 = null;

        EObject lv_antecedent_L1_16_0 = null;

        EObject lv_consequent_L1_18_0 = null;

        EObject lv_subconj_L1_19_0 = null;

        EObject lv_antecedent_L1_20_0 = null;

        EObject lv_subconj_L0_22_0 = null;

        EObject lv_antecedent_L0_23_0 = null;



        	enterRule();

        try {
            // InternalResa.g:482:2: ( (otherlv_0= '>>' ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) ) otherlv_24= '.' ) )
            // InternalResa.g:483:2: (otherlv_0= '>>' ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) ) otherlv_24= '.' )
            {
            // InternalResa.g:483:2: (otherlv_0= '>>' ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) ) otherlv_24= '.' )
            // InternalResa.g:484:3: otherlv_0= '>>' ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) ) otherlv_24= '.'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getComplexSpecAccess().getGreaterThanSignGreaterThanSignKeyword_0());
              		
            }
            // InternalResa.g:488:3: ( ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) ) | ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==14||(LA11_0>=51 && LA11_0<=52)||(LA11_0>=62 && LA11_0<=75)) ) {
                alt11=1;
            }
            else if ( (LA11_0==RULE_ID||(LA11_0>=RULE_STRING && LA11_0<=RULE_INT)||LA11_0==20||LA11_0==24||(LA11_0>=30 && LA11_0<=32)) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalResa.g:489:4: ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) )
                    {
                    // InternalResa.g:489:4: ( ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) ) )
                    // InternalResa.g:490:5: ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L2_2_0= ruleCompoundClause ) ) otherlv_3= ',' ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) )
                    {
                    // InternalResa.g:490:5: ( (lv_subconj_L2_1_0= ruleSubordinateConjunctive ) )
                    // InternalResa.g:491:6: (lv_subconj_L2_1_0= ruleSubordinateConjunctive )
                    {
                    // InternalResa.g:491:6: (lv_subconj_L2_1_0= ruleSubordinateConjunctive )
                    // InternalResa.g:492:7: lv_subconj_L2_1_0= ruleSubordinateConjunctive
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L2SubordinateConjunctiveParserRuleCall_1_0_0_0());
                      						
                    }
                    pushFollow(FOLLOW_9);
                    lv_subconj_L2_1_0=ruleSubordinateConjunctive();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                      							}
                      							set(
                      								current,
                      								"subconj_L2",
                      								lv_subconj_L2_1_0,
                      								"org.verispec.resa.Resa.SubordinateConjunctive");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalResa.g:509:5: ( (lv_antecedent_L2_2_0= ruleCompoundClause ) )
                    // InternalResa.g:510:6: (lv_antecedent_L2_2_0= ruleCompoundClause )
                    {
                    // InternalResa.g:510:6: (lv_antecedent_L2_2_0= ruleCompoundClause )
                    // InternalResa.g:511:7: lv_antecedent_L2_2_0= ruleCompoundClause
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L2CompoundClauseParserRuleCall_1_0_1_0());
                      						
                    }
                    pushFollow(FOLLOW_15);
                    lv_antecedent_L2_2_0=ruleCompoundClause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                      							}
                      							set(
                      								current,
                      								"antecedent_L2",
                      								lv_antecedent_L2_2_0,
                      								"org.verispec.resa.Resa.CompoundClause");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    otherlv_3=(Token)match(input,19,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_3, grammarAccess.getComplexSpecAccess().getCommaKeyword_1_0_2());
                      				
                    }
                    // InternalResa.g:532:5: ( ( (lv_consequent_L2_4_0= ruleCompoundClause ) ) | (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' ) )
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==RULE_ID||(LA8_0>=RULE_STRING && LA8_0<=RULE_INT)||LA8_0==24||(LA8_0>=30 && LA8_0<=32)) ) {
                        alt8=1;
                    }
                    else if ( (LA8_0==20) ) {
                        alt8=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 0, input);

                        throw nvae;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalResa.g:533:6: ( (lv_consequent_L2_4_0= ruleCompoundClause ) )
                            {
                            // InternalResa.g:533:6: ( (lv_consequent_L2_4_0= ruleCompoundClause ) )
                            // InternalResa.g:534:7: (lv_consequent_L2_4_0= ruleCompoundClause )
                            {
                            // InternalResa.g:534:7: (lv_consequent_L2_4_0= ruleCompoundClause )
                            // InternalResa.g:535:8: lv_consequent_L2_4_0= ruleCompoundClause
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getComplexSpecAccess().getConsequent_L2CompoundClauseParserRuleCall_1_0_3_0_0());
                              							
                            }
                            pushFollow(FOLLOW_8);
                            lv_consequent_L2_4_0=ruleCompoundClause();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              								}
                              								set(
                              									current,
                              									"consequent_L2",
                              									lv_consequent_L2_4_0,
                              									"org.verispec.resa.Resa.CompoundClause");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalResa.g:553:6: (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' )
                            {
                            // InternalResa.g:553:6: (otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']' )
                            // InternalResa.g:554:7: otherlv_5= '[' ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) ) otherlv_13= ']'
                            {
                            otherlv_5=(Token)match(input,20,FOLLOW_17); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_5, grammarAccess.getComplexSpecAccess().getLeftSquareBracketKeyword_1_0_3_1_0());
                              						
                            }
                            // InternalResa.g:558:7: ( ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) ) | ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) ) )
                            int alt7=2;
                            int LA7_0 = input.LA(1);

                            if ( (LA7_0==14||(LA7_0>=51 && LA7_0<=52)||(LA7_0>=62 && LA7_0<=75)) ) {
                                alt7=1;
                            }
                            else if ( (LA7_0==RULE_ID||(LA7_0>=RULE_STRING && LA7_0<=RULE_INT)||LA7_0==24||(LA7_0>=30 && LA7_0<=32)) ) {
                                alt7=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 7, 0, input);

                                throw nvae;
                            }
                            switch (alt7) {
                                case 1 :
                                    // InternalResa.g:559:8: ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) )
                                    {
                                    // InternalResa.g:559:8: ( ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) ) )
                                    // InternalResa.g:560:9: ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_7_0= ruleCompoundClause ) ) otherlv_8= ',' ( (lv_consequent_L1_9_0= ruleCompoundClause ) )
                                    {
                                    // InternalResa.g:560:9: ( (lv_subconj_L1_6_0= ruleSubordinateConjunctive ) )
                                    // InternalResa.g:561:10: (lv_subconj_L1_6_0= ruleSubordinateConjunctive )
                                    {
                                    // InternalResa.g:561:10: (lv_subconj_L1_6_0= ruleSubordinateConjunctive )
                                    // InternalResa.g:562:11: lv_subconj_L1_6_0= ruleSubordinateConjunctive
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L1SubordinateConjunctiveParserRuleCall_1_0_3_1_1_0_0_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_9);
                                    lv_subconj_L1_6_0=ruleSubordinateConjunctive();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"subconj_L1",
                                      												lv_subconj_L1_6_0,
                                      												"org.verispec.resa.Resa.SubordinateConjunctive");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }

                                    // InternalResa.g:579:9: ( (lv_antecedent_L1_7_0= ruleCompoundClause ) )
                                    // InternalResa.g:580:10: (lv_antecedent_L1_7_0= ruleCompoundClause )
                                    {
                                    // InternalResa.g:580:10: (lv_antecedent_L1_7_0= ruleCompoundClause )
                                    // InternalResa.g:581:11: lv_antecedent_L1_7_0= ruleCompoundClause
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L1CompoundClauseParserRuleCall_1_0_3_1_1_0_1_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_15);
                                    lv_antecedent_L1_7_0=ruleCompoundClause();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"antecedent_L1",
                                      												lv_antecedent_L1_7_0,
                                      												"org.verispec.resa.Resa.CompoundClause");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }

                                    otherlv_8=(Token)match(input,19,FOLLOW_9); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      									newLeafNode(otherlv_8, grammarAccess.getComplexSpecAccess().getCommaKeyword_1_0_3_1_1_0_2());
                                      								
                                    }
                                    // InternalResa.g:602:9: ( (lv_consequent_L1_9_0= ruleCompoundClause ) )
                                    // InternalResa.g:603:10: (lv_consequent_L1_9_0= ruleCompoundClause )
                                    {
                                    // InternalResa.g:603:10: (lv_consequent_L1_9_0= ruleCompoundClause )
                                    // InternalResa.g:604:11: lv_consequent_L1_9_0= ruleCompoundClause
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getConsequent_L1CompoundClauseParserRuleCall_1_0_3_1_1_0_3_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_18);
                                    lv_consequent_L1_9_0=ruleCompoundClause();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"consequent_L1",
                                      												lv_consequent_L1_9_0,
                                      												"org.verispec.resa.Resa.CompoundClause");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }


                                    }


                                    }
                                    break;
                                case 2 :
                                    // InternalResa.g:623:8: ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) )
                                    {
                                    // InternalResa.g:623:8: ( ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) ) )
                                    // InternalResa.g:624:9: ( (lv_consequent_L1_10_0= ruleCompoundClause ) ) ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_12_0= ruleCompoundClause ) )
                                    {
                                    // InternalResa.g:624:9: ( (lv_consequent_L1_10_0= ruleCompoundClause ) )
                                    // InternalResa.g:625:10: (lv_consequent_L1_10_0= ruleCompoundClause )
                                    {
                                    // InternalResa.g:625:10: (lv_consequent_L1_10_0= ruleCompoundClause )
                                    // InternalResa.g:626:11: lv_consequent_L1_10_0= ruleCompoundClause
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getConsequent_L1CompoundClauseParserRuleCall_1_0_3_1_1_1_0_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_19);
                                    lv_consequent_L1_10_0=ruleCompoundClause();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"consequent_L1",
                                      												lv_consequent_L1_10_0,
                                      												"org.verispec.resa.Resa.CompoundClause");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }

                                    // InternalResa.g:643:9: ( (lv_subconj_L1_11_0= ruleSubordinateConjunctive ) )
                                    // InternalResa.g:644:10: (lv_subconj_L1_11_0= ruleSubordinateConjunctive )
                                    {
                                    // InternalResa.g:644:10: (lv_subconj_L1_11_0= ruleSubordinateConjunctive )
                                    // InternalResa.g:645:11: lv_subconj_L1_11_0= ruleSubordinateConjunctive
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L1SubordinateConjunctiveParserRuleCall_1_0_3_1_1_1_1_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_9);
                                    lv_subconj_L1_11_0=ruleSubordinateConjunctive();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"subconj_L1",
                                      												lv_subconj_L1_11_0,
                                      												"org.verispec.resa.Resa.SubordinateConjunctive");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }

                                    // InternalResa.g:662:9: ( (lv_antecedent_L1_12_0= ruleCompoundClause ) )
                                    // InternalResa.g:663:10: (lv_antecedent_L1_12_0= ruleCompoundClause )
                                    {
                                    // InternalResa.g:663:10: (lv_antecedent_L1_12_0= ruleCompoundClause )
                                    // InternalResa.g:664:11: lv_antecedent_L1_12_0= ruleCompoundClause
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L1CompoundClauseParserRuleCall_1_0_3_1_1_1_2_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_18);
                                    lv_antecedent_L1_12_0=ruleCompoundClause();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      											}
                                      											set(
                                      												current,
                                      												"antecedent_L1",
                                      												lv_antecedent_L1_12_0,
                                      												"org.verispec.resa.Resa.CompoundClause");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }


                                    }


                                    }
                                    break;

                            }

                            otherlv_13=(Token)match(input,21,FOLLOW_8); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(otherlv_13, grammarAccess.getComplexSpecAccess().getRightSquareBracketKeyword_1_0_3_1_2());
                              						
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:691:4: ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) )
                    {
                    // InternalResa.g:691:4: ( ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? ) | (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) ) )
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==RULE_ID||(LA10_0>=RULE_STRING && LA10_0<=RULE_INT)||LA10_0==24||(LA10_0>=30 && LA10_0<=32)) ) {
                        alt10=1;
                    }
                    else if ( (LA10_0==20) ) {
                        alt10=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 0, input);

                        throw nvae;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalResa.g:692:5: ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? )
                            {
                            // InternalResa.g:692:5: ( ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )? )
                            // InternalResa.g:693:6: ( (lv_consequent_L1_14_0= ruleCompoundClause ) ) ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )?
                            {
                            // InternalResa.g:693:6: ( (lv_consequent_L1_14_0= ruleCompoundClause ) )
                            // InternalResa.g:694:7: (lv_consequent_L1_14_0= ruleCompoundClause )
                            {
                            // InternalResa.g:694:7: (lv_consequent_L1_14_0= ruleCompoundClause )
                            // InternalResa.g:695:8: lv_consequent_L1_14_0= ruleCompoundClause
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getComplexSpecAccess().getConsequent_L1CompoundClauseParserRuleCall_1_1_0_0_0());
                              							
                            }
                            pushFollow(FOLLOW_20);
                            lv_consequent_L1_14_0=ruleCompoundClause();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              								}
                              								set(
                              									current,
                              									"consequent_L1",
                              									lv_consequent_L1_14_0,
                              									"org.verispec.resa.Resa.CompoundClause");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }

                            // InternalResa.g:712:6: ( ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) ) )?
                            int alt9=2;
                            int LA9_0 = input.LA(1);

                            if ( (LA9_0==14||(LA9_0>=51 && LA9_0<=52)||(LA9_0>=62 && LA9_0<=75)) ) {
                                alt9=1;
                            }
                            switch (alt9) {
                                case 1 :
                                    // InternalResa.g:713:7: ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_16_0= ruleCompoundClause ) )
                                    {
                                    // InternalResa.g:713:7: ( (lv_subconj_L1_15_0= ruleSubordinateConjunctive ) )
                                    // InternalResa.g:714:8: (lv_subconj_L1_15_0= ruleSubordinateConjunctive )
                                    {
                                    // InternalResa.g:714:8: (lv_subconj_L1_15_0= ruleSubordinateConjunctive )
                                    // InternalResa.g:715:9: lv_subconj_L1_15_0= ruleSubordinateConjunctive
                                    {
                                    if ( state.backtracking==0 ) {

                                      									newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L1SubordinateConjunctiveParserRuleCall_1_1_0_1_0_0());
                                      								
                                    }
                                    pushFollow(FOLLOW_9);
                                    lv_subconj_L1_15_0=ruleSubordinateConjunctive();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      									if (current==null) {
                                      										current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      									}
                                      									set(
                                      										current,
                                      										"subconj_L1",
                                      										lv_subconj_L1_15_0,
                                      										"org.verispec.resa.Resa.SubordinateConjunctive");
                                      									afterParserOrEnumRuleCall();
                                      								
                                    }

                                    }


                                    }

                                    // InternalResa.g:732:7: ( (lv_antecedent_L1_16_0= ruleCompoundClause ) )
                                    // InternalResa.g:733:8: (lv_antecedent_L1_16_0= ruleCompoundClause )
                                    {
                                    // InternalResa.g:733:8: (lv_antecedent_L1_16_0= ruleCompoundClause )
                                    // InternalResa.g:734:9: lv_antecedent_L1_16_0= ruleCompoundClause
                                    {
                                    if ( state.backtracking==0 ) {

                                      									newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L1CompoundClauseParserRuleCall_1_1_0_1_1_0());
                                      								
                                    }
                                    pushFollow(FOLLOW_8);
                                    lv_antecedent_L1_16_0=ruleCompoundClause();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      									if (current==null) {
                                      										current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                                      									}
                                      									set(
                                      										current,
                                      										"antecedent_L1",
                                      										lv_antecedent_L1_16_0,
                                      										"org.verispec.resa.Resa.CompoundClause");
                                      									afterParserOrEnumRuleCall();
                                      								
                                    }

                                    }


                                    }


                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalResa.g:754:5: (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) )
                            {
                            // InternalResa.g:754:5: (otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) ) )
                            // InternalResa.g:755:6: otherlv_17= '[' ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) ) otherlv_21= ']' ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L0_23_0= ruleCompoundClause ) )
                            {
                            otherlv_17=(Token)match(input,20,FOLLOW_9); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_17, grammarAccess.getComplexSpecAccess().getLeftSquareBracketKeyword_1_1_1_0());
                              					
                            }
                            // InternalResa.g:759:6: ( ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) ) )
                            // InternalResa.g:760:7: ( (lv_consequent_L1_18_0= ruleCompoundClause ) ) ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) ) ( (lv_antecedent_L1_20_0= ruleCompoundClause ) )
                            {
                            // InternalResa.g:760:7: ( (lv_consequent_L1_18_0= ruleCompoundClause ) )
                            // InternalResa.g:761:8: (lv_consequent_L1_18_0= ruleCompoundClause )
                            {
                            // InternalResa.g:761:8: (lv_consequent_L1_18_0= ruleCompoundClause )
                            // InternalResa.g:762:9: lv_consequent_L1_18_0= ruleCompoundClause
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getComplexSpecAccess().getConsequent_L1CompoundClauseParserRuleCall_1_1_1_1_0_0());
                              								
                            }
                            pushFollow(FOLLOW_19);
                            lv_consequent_L1_18_0=ruleCompoundClause();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              									}
                              									set(
                              										current,
                              										"consequent_L1",
                              										lv_consequent_L1_18_0,
                              										"org.verispec.resa.Resa.CompoundClause");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }

                            // InternalResa.g:779:7: ( (lv_subconj_L1_19_0= ruleSubordinateConjunctive ) )
                            // InternalResa.g:780:8: (lv_subconj_L1_19_0= ruleSubordinateConjunctive )
                            {
                            // InternalResa.g:780:8: (lv_subconj_L1_19_0= ruleSubordinateConjunctive )
                            // InternalResa.g:781:9: lv_subconj_L1_19_0= ruleSubordinateConjunctive
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L1SubordinateConjunctiveParserRuleCall_1_1_1_1_1_0());
                              								
                            }
                            pushFollow(FOLLOW_9);
                            lv_subconj_L1_19_0=ruleSubordinateConjunctive();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              									}
                              									set(
                              										current,
                              										"subconj_L1",
                              										lv_subconj_L1_19_0,
                              										"org.verispec.resa.Resa.SubordinateConjunctive");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }

                            // InternalResa.g:798:7: ( (lv_antecedent_L1_20_0= ruleCompoundClause ) )
                            // InternalResa.g:799:8: (lv_antecedent_L1_20_0= ruleCompoundClause )
                            {
                            // InternalResa.g:799:8: (lv_antecedent_L1_20_0= ruleCompoundClause )
                            // InternalResa.g:800:9: lv_antecedent_L1_20_0= ruleCompoundClause
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L1CompoundClauseParserRuleCall_1_1_1_1_2_0());
                              								
                            }
                            pushFollow(FOLLOW_18);
                            lv_antecedent_L1_20_0=ruleCompoundClause();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              									}
                              									set(
                              										current,
                              										"antecedent_L1",
                              										lv_antecedent_L1_20_0,
                              										"org.verispec.resa.Resa.CompoundClause");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }


                            }

                            otherlv_21=(Token)match(input,21,FOLLOW_19); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_21, grammarAccess.getComplexSpecAccess().getRightSquareBracketKeyword_1_1_1_2());
                              					
                            }
                            // InternalResa.g:822:6: ( (lv_subconj_L0_22_0= ruleSubordinateConjunctive ) )
                            // InternalResa.g:823:7: (lv_subconj_L0_22_0= ruleSubordinateConjunctive )
                            {
                            // InternalResa.g:823:7: (lv_subconj_L0_22_0= ruleSubordinateConjunctive )
                            // InternalResa.g:824:8: lv_subconj_L0_22_0= ruleSubordinateConjunctive
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getComplexSpecAccess().getSubconj_L0SubordinateConjunctiveParserRuleCall_1_1_1_3_0());
                              							
                            }
                            pushFollow(FOLLOW_9);
                            lv_subconj_L0_22_0=ruleSubordinateConjunctive();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              								}
                              								set(
                              									current,
                              									"subconj_L0",
                              									lv_subconj_L0_22_0,
                              									"org.verispec.resa.Resa.SubordinateConjunctive");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }

                            // InternalResa.g:841:6: ( (lv_antecedent_L0_23_0= ruleCompoundClause ) )
                            // InternalResa.g:842:7: (lv_antecedent_L0_23_0= ruleCompoundClause )
                            {
                            // InternalResa.g:842:7: (lv_antecedent_L0_23_0= ruleCompoundClause )
                            // InternalResa.g:843:8: lv_antecedent_L0_23_0= ruleCompoundClause
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getComplexSpecAccess().getAntecedent_L0CompoundClauseParserRuleCall_1_1_1_4_0());
                              							
                            }
                            pushFollow(FOLLOW_8);
                            lv_antecedent_L0_23_0=ruleCompoundClause();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getComplexSpecRule());
                              								}
                              								set(
                              									current,
                              									"antecedent_L0",
                              									lv_antecedent_L0_23_0,
                              									"org.verispec.resa.Resa.CompoundClause");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            otherlv_24=(Token)match(input,11,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_24, grammarAccess.getComplexSpecAccess().getFullStopKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplexSpec"


    // $ANTLR start "entryRuleCompoundClause"
    // InternalResa.g:871:1: entryRuleCompoundClause returns [EObject current=null] : iv_ruleCompoundClause= ruleCompoundClause EOF ;
    public final EObject entryRuleCompoundClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundClause = null;


        try {
            // InternalResa.g:871:55: (iv_ruleCompoundClause= ruleCompoundClause EOF )
            // InternalResa.g:872:2: iv_ruleCompoundClause= ruleCompoundClause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCompoundClauseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCompoundClause=ruleCompoundClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCompoundClause; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundClause"


    // $ANTLR start "ruleCompoundClause"
    // InternalResa.g:878:1: ruleCompoundClause returns [EObject current=null] : (this_ClauseType_0= ruleClauseType ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )* ) ;
    public final EObject ruleCompoundClause() throws RecognitionException {
        EObject current = null;

        EObject this_ClauseType_0 = null;

        EObject lv_lop_2_0 = null;

        EObject lv_right_3_0 = null;



        	enterRule();

        try {
            // InternalResa.g:884:2: ( (this_ClauseType_0= ruleClauseType ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )* ) )
            // InternalResa.g:885:2: (this_ClauseType_0= ruleClauseType ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )* )
            {
            // InternalResa.g:885:2: (this_ClauseType_0= ruleClauseType ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )* )
            // InternalResa.g:886:3: this_ClauseType_0= ruleClauseType ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getCompoundClauseAccess().getClauseTypeParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_21);
            this_ClauseType_0=ruleClauseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current = this_ClauseType_0;
              			afterParserOrEnumRuleCall();
              		
            }
            // InternalResa.g:894:3: ( () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=22 && LA12_0<=23)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalResa.g:895:4: () ( (lv_lop_2_0= ruleLogicalOP ) ) ( (lv_right_3_0= ruleClauseType ) )
            	    {
            	    // InternalResa.g:895:4: ()
            	    // InternalResa.g:896:5: 
            	    {
            	    if ( state.backtracking==0 ) {

            	      					current = forceCreateModelElementAndSet(
            	      						grammarAccess.getCompoundClauseAccess().getLOPLeftAction_1_0(),
            	      						current);
            	      				
            	    }

            	    }

            	    // InternalResa.g:902:4: ( (lv_lop_2_0= ruleLogicalOP ) )
            	    // InternalResa.g:903:5: (lv_lop_2_0= ruleLogicalOP )
            	    {
            	    // InternalResa.g:903:5: (lv_lop_2_0= ruleLogicalOP )
            	    // InternalResa.g:904:6: lv_lop_2_0= ruleLogicalOP
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getCompoundClauseAccess().getLopLogicalOPParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_9);
            	    lv_lop_2_0=ruleLogicalOP();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getCompoundClauseRule());
            	      						}
            	      						set(
            	      							current,
            	      							"lop",
            	      							lv_lop_2_0,
            	      							"org.verispec.resa.Resa.LogicalOP");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }

            	    // InternalResa.g:921:4: ( (lv_right_3_0= ruleClauseType ) )
            	    // InternalResa.g:922:5: (lv_right_3_0= ruleClauseType )
            	    {
            	    // InternalResa.g:922:5: (lv_right_3_0= ruleClauseType )
            	    // InternalResa.g:923:6: lv_right_3_0= ruleClauseType
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getCompoundClauseAccess().getRightClauseTypeParserRuleCall_1_2_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_21);
            	    lv_right_3_0=ruleClauseType();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getCompoundClauseRule());
            	      						}
            	      						set(
            	      							current,
            	      							"right",
            	      							lv_right_3_0,
            	      							"org.verispec.resa.Resa.ClauseType");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundClause"


    // $ANTLR start "entryRuleLogicalOP"
    // InternalResa.g:945:1: entryRuleLogicalOP returns [EObject current=null] : iv_ruleLogicalOP= ruleLogicalOP EOF ;
    public final EObject entryRuleLogicalOP() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalOP = null;


        try {
            // InternalResa.g:945:50: (iv_ruleLogicalOP= ruleLogicalOP EOF )
            // InternalResa.g:946:2: iv_ruleLogicalOP= ruleLogicalOP EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLogicalOPRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLogicalOP=ruleLogicalOP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLogicalOP; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalOP"


    // $ANTLR start "ruleLogicalOP"
    // InternalResa.g:952:1: ruleLogicalOP returns [EObject current=null] : ( ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) ) ) ;
    public final EObject ruleLogicalOP() throws RecognitionException {
        EObject current = null;

        Token lv_lop_0_1=null;
        Token lv_lop_0_2=null;


        	enterRule();

        try {
            // InternalResa.g:958:2: ( ( ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) ) ) )
            // InternalResa.g:959:2: ( ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) ) )
            {
            // InternalResa.g:959:2: ( ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) ) )
            // InternalResa.g:960:3: ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) )
            {
            // InternalResa.g:960:3: ( (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' ) )
            // InternalResa.g:961:4: (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' )
            {
            // InternalResa.g:961:4: (lv_lop_0_1= 'and' | lv_lop_0_2= 'or' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==22) ) {
                alt13=1;
            }
            else if ( (LA13_0==23) ) {
                alt13=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalResa.g:962:5: lv_lop_0_1= 'and'
                    {
                    lv_lop_0_1=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_lop_0_1, grammarAccess.getLogicalOPAccess().getLopAndKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getLogicalOPRule());
                      					}
                      					setWithLastConsumed(current, "lop", lv_lop_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:973:5: lv_lop_0_2= 'or'
                    {
                    lv_lop_0_2=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_lop_0_2, grammarAccess.getLogicalOPAccess().getLopOrKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getLogicalOPRule());
                      					}
                      					setWithLastConsumed(current, "lop", lv_lop_0_2, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalOP"


    // $ANTLR start "entryRuleClauseType"
    // InternalResa.g:989:1: entryRuleClauseType returns [EObject current=null] : iv_ruleClauseType= ruleClauseType EOF ;
    public final EObject entryRuleClauseType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClauseType = null;


        try {
            // InternalResa.g:989:51: (iv_ruleClauseType= ruleClauseType EOF )
            // InternalResa.g:990:2: iv_ruleClauseType= ruleClauseType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClauseTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleClauseType=ruleClauseType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClauseType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClauseType"


    // $ANTLR start "ruleClauseType"
    // InternalResa.g:996:1: ruleClauseType returns [EObject current=null] : ( (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' ) | ( (lv_clause_3_0= ruleClause ) ) | ( (lv_complexclause_4_0= ruleComplexClause ) ) ) ;
    public final EObject ruleClauseType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_CompoundClause_1 = null;

        EObject lv_clause_3_0 = null;

        EObject lv_complexclause_4_0 = null;



        	enterRule();

        try {
            // InternalResa.g:1002:2: ( ( (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' ) | ( (lv_clause_3_0= ruleClause ) ) | ( (lv_complexclause_4_0= ruleComplexClause ) ) ) )
            // InternalResa.g:1003:2: ( (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' ) | ( (lv_clause_3_0= ruleClause ) ) | ( (lv_complexclause_4_0= ruleComplexClause ) ) )
            {
            // InternalResa.g:1003:2: ( (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' ) | ( (lv_clause_3_0= ruleClause ) ) | ( (lv_complexclause_4_0= ruleComplexClause ) ) )
            int alt14=3;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // InternalResa.g:1004:3: (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' )
                    {
                    // InternalResa.g:1004:3: (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' )
                    // InternalResa.g:1005:4: otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,24,FOLLOW_9); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getClauseTypeAccess().getLeftParenthesisKeyword_0_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getClauseTypeAccess().getCompoundClauseParserRuleCall_0_1());
                      			
                    }
                    pushFollow(FOLLOW_22);
                    this_CompoundClause_1=ruleCompoundClause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_CompoundClause_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    otherlv_2=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getClauseTypeAccess().getRightParenthesisKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:1023:3: ( (lv_clause_3_0= ruleClause ) )
                    {
                    // InternalResa.g:1023:3: ( (lv_clause_3_0= ruleClause ) )
                    // InternalResa.g:1024:4: (lv_clause_3_0= ruleClause )
                    {
                    // InternalResa.g:1024:4: (lv_clause_3_0= ruleClause )
                    // InternalResa.g:1025:5: lv_clause_3_0= ruleClause
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getClauseTypeAccess().getClauseClauseParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_clause_3_0=ruleClause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getClauseTypeRule());
                      					}
                      					set(
                      						current,
                      						"clause",
                      						lv_clause_3_0,
                      						"org.verispec.resa.Resa.Clause");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalResa.g:1043:3: ( (lv_complexclause_4_0= ruleComplexClause ) )
                    {
                    // InternalResa.g:1043:3: ( (lv_complexclause_4_0= ruleComplexClause ) )
                    // InternalResa.g:1044:4: (lv_complexclause_4_0= ruleComplexClause )
                    {
                    // InternalResa.g:1044:4: (lv_complexclause_4_0= ruleComplexClause )
                    // InternalResa.g:1045:5: lv_complexclause_4_0= ruleComplexClause
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getClauseTypeAccess().getComplexclauseComplexClauseParserRuleCall_2_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_complexclause_4_0=ruleComplexClause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getClauseTypeRule());
                      					}
                      					set(
                      						current,
                      						"complexclause",
                      						lv_complexclause_4_0,
                      						"org.verispec.resa.Resa.ComplexClause");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClauseType"


    // $ANTLR start "entryRuleComplexClause"
    // InternalResa.g:1066:1: entryRuleComplexClause returns [EObject current=null] : iv_ruleComplexClause= ruleComplexClause EOF ;
    public final EObject entryRuleComplexClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplexClause = null;


        try {
            // InternalResa.g:1066:54: (iv_ruleComplexClause= ruleComplexClause EOF )
            // InternalResa.g:1067:2: iv_ruleComplexClause= ruleComplexClause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComplexClauseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComplexClause=ruleComplexClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComplexClause; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplexClause"


    // $ANTLR start "ruleComplexClause"
    // InternalResa.g:1073:1: ruleComplexClause returns [EObject current=null] : (otherlv_0= '(' ( (lv_clause_1_0= ruleClause ) ) ( (lv_subclause_2_0= ruleSubordinateClause ) ) otherlv_3= ')' ) ;
    public final EObject ruleComplexClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject lv_clause_1_0 = null;

        EObject lv_subclause_2_0 = null;



        	enterRule();

        try {
            // InternalResa.g:1079:2: ( (otherlv_0= '(' ( (lv_clause_1_0= ruleClause ) ) ( (lv_subclause_2_0= ruleSubordinateClause ) ) otherlv_3= ')' ) )
            // InternalResa.g:1080:2: (otherlv_0= '(' ( (lv_clause_1_0= ruleClause ) ) ( (lv_subclause_2_0= ruleSubordinateClause ) ) otherlv_3= ')' )
            {
            // InternalResa.g:1080:2: (otherlv_0= '(' ( (lv_clause_1_0= ruleClause ) ) ( (lv_subclause_2_0= ruleSubordinateClause ) ) otherlv_3= ')' )
            // InternalResa.g:1081:3: otherlv_0= '(' ( (lv_clause_1_0= ruleClause ) ) ( (lv_subclause_2_0= ruleSubordinateClause ) ) otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getComplexClauseAccess().getLeftParenthesisKeyword_0());
              		
            }
            // InternalResa.g:1085:3: ( (lv_clause_1_0= ruleClause ) )
            // InternalResa.g:1086:4: (lv_clause_1_0= ruleClause )
            {
            // InternalResa.g:1086:4: (lv_clause_1_0= ruleClause )
            // InternalResa.g:1087:5: lv_clause_1_0= ruleClause
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getComplexClauseAccess().getClauseClauseParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_19);
            lv_clause_1_0=ruleClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getComplexClauseRule());
              					}
              					set(
              						current,
              						"clause",
              						lv_clause_1_0,
              						"org.verispec.resa.Resa.Clause");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:1104:3: ( (lv_subclause_2_0= ruleSubordinateClause ) )
            // InternalResa.g:1105:4: (lv_subclause_2_0= ruleSubordinateClause )
            {
            // InternalResa.g:1105:4: (lv_subclause_2_0= ruleSubordinateClause )
            // InternalResa.g:1106:5: lv_subclause_2_0= ruleSubordinateClause
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getComplexClauseAccess().getSubclauseSubordinateClauseParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_22);
            lv_subclause_2_0=ruleSubordinateClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getComplexClauseRule());
              					}
              					set(
              						current,
              						"subclause",
              						lv_subclause_2_0,
              						"org.verispec.resa.Resa.SubordinateClause");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getComplexClauseAccess().getRightParenthesisKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplexClause"


    // $ANTLR start "entryRuleComplement"
    // InternalResa.g:1131:1: entryRuleComplement returns [EObject current=null] : iv_ruleComplement= ruleComplement EOF ;
    public final EObject entryRuleComplement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplement = null;


        try {
            // InternalResa.g:1131:51: (iv_ruleComplement= ruleComplement EOF )
            // InternalResa.g:1132:2: iv_ruleComplement= ruleComplement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComplementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComplement=ruleComplement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComplement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplement"


    // $ANTLR start "ruleComplement"
    // InternalResa.g:1138:1: ruleComplement returns [EObject current=null] : (this_Timing_0= ruleTiming | this_Occurence_1= ruleOccurence ) ;
    public final EObject ruleComplement() throws RecognitionException {
        EObject current = null;

        EObject this_Timing_0 = null;

        EObject this_Occurence_1 = null;



        	enterRule();

        try {
            // InternalResa.g:1144:2: ( (this_Timing_0= ruleTiming | this_Occurence_1= ruleOccurence ) )
            // InternalResa.g:1145:2: (this_Timing_0= ruleTiming | this_Occurence_1= ruleOccurence )
            {
            // InternalResa.g:1145:2: (this_Timing_0= ruleTiming | this_Occurence_1= ruleOccurence )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==20||LA15_0==38||(LA15_0>=49 && LA15_0<=54)) ) {
                alt15=1;
            }
            else if ( ((LA15_0>=59 && LA15_0<=60)) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalResa.g:1146:3: this_Timing_0= ruleTiming
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getComplementAccess().getTimingParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Timing_0=ruleTiming();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Timing_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:1155:3: this_Occurence_1= ruleOccurence
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getComplementAccess().getOccurenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Occurence_1=ruleOccurence();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Occurence_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplement"


    // $ANTLR start "entryRuleClause"
    // InternalResa.g:1167:1: entryRuleClause returns [EObject current=null] : iv_ruleClause= ruleClause EOF ;
    public final EObject entryRuleClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClause = null;


        try {
            // InternalResa.g:1167:47: (iv_ruleClause= ruleClause EOF )
            // InternalResa.g:1168:2: iv_ruleClause= ruleClause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getClauseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleClause=ruleClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleClause; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClause"


    // $ANTLR start "ruleClause"
    // InternalResa.g:1174:1: ruleClause returns [EObject current=null] : ( ( (lv_possession_0_0= rulePossession ) )? ( (lv_sub_1_0= ruleNP ) ) ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) ) ( (lv_comp_21_0= ruleComplement ) )? (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )? ) ;
    public final EObject ruleClause() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token lv_be_6_0=null;
        Token lv_setto_9_0=null;
        Token otherlv_15=null;
        Token lv_setto_18_0=null;
        Token otherlv_22=null;
        EObject lv_possession_0_0 = null;

        EObject lv_sub_1_0 = null;

        EObject lv_modal_2_0 = null;

        EObject lv_verb_4_0 = null;

        EObject lv_apredicate_5_0 = null;

        EObject lv_verb_7_0 = null;

        EObject lv_ppredicate_8_0 = null;

        EObject lv_right_10_0 = null;

        AntlrDatatypeRuleToken lv_verbnegation_11_0 = null;

        EObject lv_verb_12_0 = null;

        EObject lv_apredicate_13_0 = null;

        EObject lv_tobe_14_0 = null;

        EObject lv_verb_16_0 = null;

        EObject lv_apredicate_17_0 = null;

        EObject lv_right_19_0 = null;

        EObject lv_adj_20_0 = null;

        EObject lv_comp_21_0 = null;

        EObject lv_rational_23_0 = null;



        	enterRule();

        try {
            // InternalResa.g:1180:2: ( ( ( (lv_possession_0_0= rulePossession ) )? ( (lv_sub_1_0= ruleNP ) ) ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) ) ( (lv_comp_21_0= ruleComplement ) )? (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )? ) )
            // InternalResa.g:1181:2: ( ( (lv_possession_0_0= rulePossession ) )? ( (lv_sub_1_0= ruleNP ) ) ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) ) ( (lv_comp_21_0= ruleComplement ) )? (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )? )
            {
            // InternalResa.g:1181:2: ( ( (lv_possession_0_0= rulePossession ) )? ( (lv_sub_1_0= ruleNP ) ) ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) ) ( (lv_comp_21_0= ruleComplement ) )? (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )? )
            // InternalResa.g:1182:3: ( (lv_possession_0_0= rulePossession ) )? ( (lv_sub_1_0= ruleNP ) ) ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) ) ( (lv_comp_21_0= ruleComplement ) )? (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )?
            {
            // InternalResa.g:1182:3: ( (lv_possession_0_0= rulePossession ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=30 && LA16_0<=32)) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalResa.g:1183:4: (lv_possession_0_0= rulePossession )
                    {
                    // InternalResa.g:1183:4: (lv_possession_0_0= rulePossession )
                    // InternalResa.g:1184:5: lv_possession_0_0= rulePossession
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getClauseAccess().getPossessionPossessionParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_7);
                    lv_possession_0_0=rulePossession();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getClauseRule());
                      					}
                      					set(
                      						current,
                      						"possession",
                      						lv_possession_0_0,
                      						"org.verispec.resa.Resa.Possession");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalResa.g:1201:3: ( (lv_sub_1_0= ruleNP ) )
            // InternalResa.g:1202:4: (lv_sub_1_0= ruleNP )
            {
            // InternalResa.g:1202:4: (lv_sub_1_0= ruleNP )
            // InternalResa.g:1203:5: lv_sub_1_0= ruleNP
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getClauseAccess().getSubNPParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_23);
            lv_sub_1_0=ruleNP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getClauseRule());
              					}
              					set(
              						current,
              						"sub",
              						lv_sub_1_0,
              						"org.verispec.resa.Resa.NP");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:1220:3: ( ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) ) | ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? ) | ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) ) )
            int alt27=3;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt27=1;
                }
                break;
            case RULE_ID:
            case 33:
            case 34:
            case 35:
            case 36:
                {
                alt27=2;
                }
                break;
            case 41:
                {
                alt27=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalResa.g:1221:4: ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) )
                    {
                    // InternalResa.g:1221:4: ( ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) ) )
                    // InternalResa.g:1222:5: ( (lv_modal_2_0= ruleMOD ) ) (otherlv_3= 'not' )? ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) )
                    {
                    // InternalResa.g:1222:5: ( (lv_modal_2_0= ruleMOD ) )
                    // InternalResa.g:1223:6: (lv_modal_2_0= ruleMOD )
                    {
                    // InternalResa.g:1223:6: (lv_modal_2_0= ruleMOD )
                    // InternalResa.g:1224:7: lv_modal_2_0= ruleMOD
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getClauseAccess().getModalMODParserRuleCall_2_0_0_0());
                      						
                    }
                    pushFollow(FOLLOW_24);
                    lv_modal_2_0=ruleMOD();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getClauseRule());
                      							}
                      							set(
                      								current,
                      								"modal",
                      								lv_modal_2_0,
                      								"org.verispec.resa.Resa.MOD");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalResa.g:1241:5: (otherlv_3= 'not' )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==26) ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalResa.g:1242:6: otherlv_3= 'not'
                            {
                            otherlv_3=(Token)match(input,26,FOLLOW_25); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_3, grammarAccess.getClauseAccess().getNotKeyword_2_0_1());
                              					
                            }

                            }
                            break;

                    }

                    // InternalResa.g:1247:5: ( ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? ) | ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) ) )
                    int alt21=2;
                    int LA21_0 = input.LA(1);

                    if ( (LA21_0==RULE_ID) ) {
                        alt21=1;
                    }
                    else if ( (LA21_0==27) ) {
                        alt21=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 21, 0, input);

                        throw nvae;
                    }
                    switch (alt21) {
                        case 1 :
                            // InternalResa.g:1248:6: ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? )
                            {
                            // InternalResa.g:1248:6: ( ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )? )
                            // InternalResa.g:1249:7: ( (lv_verb_4_0= ruleVB ) ) ( (lv_apredicate_5_0= ruleActivePredicate ) )?
                            {
                            // InternalResa.g:1249:7: ( (lv_verb_4_0= ruleVB ) )
                            // InternalResa.g:1250:8: (lv_verb_4_0= ruleVB )
                            {
                            // InternalResa.g:1250:8: (lv_verb_4_0= ruleVB )
                            // InternalResa.g:1251:9: lv_verb_4_0= ruleVB
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getClauseAccess().getVerbVBParserRuleCall_2_0_2_0_0_0());
                              								
                            }
                            pushFollow(FOLLOW_26);
                            lv_verb_4_0=ruleVB();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getClauseRule());
                              									}
                              									set(
                              										current,
                              										"verb",
                              										lv_verb_4_0,
                              										"org.verispec.resa.Resa.VB");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }

                            // InternalResa.g:1268:7: ( (lv_apredicate_5_0= ruleActivePredicate ) )?
                            int alt18=2;
                            int LA18_0 = input.LA(1);

                            if ( (LA18_0==RULE_ID||(LA18_0>=RULE_STRING && LA18_0<=RULE_INT)) ) {
                                alt18=1;
                            }
                            switch (alt18) {
                                case 1 :
                                    // InternalResa.g:1269:8: (lv_apredicate_5_0= ruleActivePredicate )
                                    {
                                    // InternalResa.g:1269:8: (lv_apredicate_5_0= ruleActivePredicate )
                                    // InternalResa.g:1270:9: lv_apredicate_5_0= ruleActivePredicate
                                    {
                                    if ( state.backtracking==0 ) {

                                      									newCompositeNode(grammarAccess.getClauseAccess().getApredicateActivePredicateParserRuleCall_2_0_2_0_1_0());
                                      								
                                    }
                                    pushFollow(FOLLOW_27);
                                    lv_apredicate_5_0=ruleActivePredicate();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      									if (current==null) {
                                      										current = createModelElementForParent(grammarAccess.getClauseRule());
                                      									}
                                      									set(
                                      										current,
                                      										"apredicate",
                                      										lv_apredicate_5_0,
                                      										"org.verispec.resa.Resa.ActivePredicate");
                                      									afterParserOrEnumRuleCall();
                                      								
                                    }

                                    }


                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalResa.g:1289:6: ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) )
                            {
                            // InternalResa.g:1289:6: ( ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) ) )
                            // InternalResa.g:1290:7: ( (lv_be_6_0= 'be' ) ) ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) )
                            {
                            // InternalResa.g:1290:7: ( (lv_be_6_0= 'be' ) )
                            // InternalResa.g:1291:8: (lv_be_6_0= 'be' )
                            {
                            // InternalResa.g:1291:8: (lv_be_6_0= 'be' )
                            // InternalResa.g:1292:9: lv_be_6_0= 'be'
                            {
                            lv_be_6_0=(Token)match(input,27,FOLLOW_28); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									newLeafNode(lv_be_6_0, grammarAccess.getClauseAccess().getBeBeKeyword_2_0_2_1_0_0());
                              								
                            }
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElement(grammarAccess.getClauseRule());
                              									}
                              									setWithLastConsumed(current, "be", lv_be_6_0, "be");
                              								
                            }

                            }


                            }

                            // InternalResa.g:1304:7: ( ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) ) )
                            int alt20=2;
                            int LA20_0 = input.LA(1);

                            if ( (LA20_0==RULE_ID) ) {
                                alt20=1;
                            }
                            else if ( (LA20_0==28) ) {
                                alt20=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 20, 0, input);

                                throw nvae;
                            }
                            switch (alt20) {
                                case 1 :
                                    // InternalResa.g:1305:8: ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? )
                                    {
                                    // InternalResa.g:1305:8: ( ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )? )
                                    // InternalResa.g:1306:9: ( (lv_verb_7_0= ruleVB ) ) ( (lv_ppredicate_8_0= rulePassivePredicate ) )?
                                    {
                                    // InternalResa.g:1306:9: ( (lv_verb_7_0= ruleVB ) )
                                    // InternalResa.g:1307:10: (lv_verb_7_0= ruleVB )
                                    {
                                    // InternalResa.g:1307:10: (lv_verb_7_0= ruleVB )
                                    // InternalResa.g:1308:11: lv_verb_7_0= ruleVB
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getClauseAccess().getVerbVBParserRuleCall_2_0_2_1_1_0_0_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_29);
                                    lv_verb_7_0=ruleVB();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getClauseRule());
                                      											}
                                      											set(
                                      												current,
                                      												"verb",
                                      												lv_verb_7_0,
                                      												"org.verispec.resa.Resa.VB");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }

                                    // InternalResa.g:1325:9: ( (lv_ppredicate_8_0= rulePassivePredicate ) )?
                                    int alt19=2;
                                    int LA19_0 = input.LA(1);

                                    if ( (LA19_0==RULE_ID||(LA19_0>=RULE_STRING && LA19_0<=RULE_INT)||LA19_0==37||(LA19_0>=39 && LA19_0<=40)) ) {
                                        alt19=1;
                                    }
                                    else if ( (LA19_0==38) ) {
                                        int LA19_2 = input.LA(2);

                                        if ( (LA19_2==RULE_INT) ) {
                                            int LA19_4 = input.LA(3);

                                            if ( (LA19_4==11) ) {
                                                int LA19_5 = input.LA(4);

                                                if ( (LA19_5==RULE_INT) ) {
                                                    int LA19_6 = input.LA(5);

                                                    if ( (LA19_6==EOF||LA19_6==11||(LA19_6>=14 && LA19_6<=17)||(LA19_6>=19 && LA19_6<=23)||LA19_6==25||LA19_6==29||(LA19_6>=38 && LA19_6<=40)||(LA19_6>=49 && LA19_6<=54)||(LA19_6>=59 && LA19_6<=60)||(LA19_6>=62 && LA19_6<=75)) ) {
                                                        alt19=1;
                                                    }
                                                }
                                                else if ( (LA19_5==EOF||(LA19_5>=13 && LA19_5<=14)||LA19_5==18) ) {
                                                    alt19=1;
                                                }
                                            }
                                            else if ( (LA19_4==EOF||(LA19_4>=14 && LA19_4<=17)||(LA19_4>=19 && LA19_4<=23)||LA19_4==25||LA19_4==29||(LA19_4>=38 && LA19_4<=40)||(LA19_4>=49 && LA19_4<=54)||(LA19_4>=59 && LA19_4<=60)||(LA19_4>=62 && LA19_4<=75)) ) {
                                                alt19=1;
                                            }
                                        }
                                        else if ( (LA19_2==RULE_ID||LA19_2==RULE_STRING) ) {
                                            alt19=1;
                                        }
                                    }
                                    switch (alt19) {
                                        case 1 :
                                            // InternalResa.g:1326:10: (lv_ppredicate_8_0= rulePassivePredicate )
                                            {
                                            // InternalResa.g:1326:10: (lv_ppredicate_8_0= rulePassivePredicate )
                                            // InternalResa.g:1327:11: lv_ppredicate_8_0= rulePassivePredicate
                                            {
                                            if ( state.backtracking==0 ) {

                                              											newCompositeNode(grammarAccess.getClauseAccess().getPpredicatePassivePredicateParserRuleCall_2_0_2_1_1_0_1_0());
                                              										
                                            }
                                            pushFollow(FOLLOW_27);
                                            lv_ppredicate_8_0=rulePassivePredicate();

                                            state._fsp--;
                                            if (state.failed) return current;
                                            if ( state.backtracking==0 ) {

                                              											if (current==null) {
                                              												current = createModelElementForParent(grammarAccess.getClauseRule());
                                              											}
                                              											set(
                                              												current,
                                              												"ppredicate",
                                              												lv_ppredicate_8_0,
                                              												"org.verispec.resa.Resa.PassivePredicate");
                                              											afterParserOrEnumRuleCall();
                                              										
                                            }

                                            }


                                            }
                                            break;

                                    }


                                    }


                                    }
                                    break;
                                case 2 :
                                    // InternalResa.g:1346:8: ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) )
                                    {
                                    // InternalResa.g:1346:8: ( ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) ) )
                                    // InternalResa.g:1347:9: ( (lv_setto_9_0= 'set_to' ) ) ( (lv_right_10_0= ruleVariable ) )
                                    {
                                    // InternalResa.g:1347:9: ( (lv_setto_9_0= 'set_to' ) )
                                    // InternalResa.g:1348:10: (lv_setto_9_0= 'set_to' )
                                    {
                                    // InternalResa.g:1348:10: (lv_setto_9_0= 'set_to' )
                                    // InternalResa.g:1349:11: lv_setto_9_0= 'set_to'
                                    {
                                    lv_setto_9_0=(Token)match(input,28,FOLLOW_5); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											newLeafNode(lv_setto_9_0, grammarAccess.getClauseAccess().getSettoSet_toKeyword_2_0_2_1_1_1_0_0());
                                      										
                                    }
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElement(grammarAccess.getClauseRule());
                                      											}
                                      											setWithLastConsumed(current, "setto", lv_setto_9_0, "set_to");
                                      										
                                    }

                                    }


                                    }

                                    // InternalResa.g:1361:9: ( (lv_right_10_0= ruleVariable ) )
                                    // InternalResa.g:1362:10: (lv_right_10_0= ruleVariable )
                                    {
                                    // InternalResa.g:1362:10: (lv_right_10_0= ruleVariable )
                                    // InternalResa.g:1363:11: lv_right_10_0= ruleVariable
                                    {
                                    if ( state.backtracking==0 ) {

                                      											newCompositeNode(grammarAccess.getClauseAccess().getRightVariableParserRuleCall_2_0_2_1_1_1_1_0());
                                      										
                                    }
                                    pushFollow(FOLLOW_27);
                                    lv_right_10_0=ruleVariable();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      											if (current==null) {
                                      												current = createModelElementForParent(grammarAccess.getClauseRule());
                                      											}
                                      											set(
                                      												current,
                                      												"right",
                                      												lv_right_10_0,
                                      												"org.verispec.resa.Resa.Variable");
                                      											afterParserOrEnumRuleCall();
                                      										
                                    }

                                    }


                                    }


                                    }


                                    }
                                    break;

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:1386:4: ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? )
                    {
                    // InternalResa.g:1386:4: ( ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )? )
                    // InternalResa.g:1387:5: ( (lv_verbnegation_11_0= ruleVerbNegation ) )? ( (lv_verb_12_0= ruleVB ) ) ( (lv_apredicate_13_0= ruleActivePredicate ) )?
                    {
                    // InternalResa.g:1387:5: ( (lv_verbnegation_11_0= ruleVerbNegation ) )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( ((LA22_0>=33 && LA22_0<=36)) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // InternalResa.g:1388:6: (lv_verbnegation_11_0= ruleVerbNegation )
                            {
                            // InternalResa.g:1388:6: (lv_verbnegation_11_0= ruleVerbNegation )
                            // InternalResa.g:1389:7: lv_verbnegation_11_0= ruleVerbNegation
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getClauseAccess().getVerbnegationVerbNegationParserRuleCall_2_1_0_0());
                              						
                            }
                            pushFollow(FOLLOW_30);
                            lv_verbnegation_11_0=ruleVerbNegation();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getClauseRule());
                              							}
                              							set(
                              								current,
                              								"verbnegation",
                              								lv_verbnegation_11_0,
                              								"org.verispec.resa.Resa.VerbNegation");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }
                            break;

                    }

                    // InternalResa.g:1406:5: ( (lv_verb_12_0= ruleVB ) )
                    // InternalResa.g:1407:6: (lv_verb_12_0= ruleVB )
                    {
                    // InternalResa.g:1407:6: (lv_verb_12_0= ruleVB )
                    // InternalResa.g:1408:7: lv_verb_12_0= ruleVB
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getClauseAccess().getVerbVBParserRuleCall_2_1_1_0());
                      						
                    }
                    pushFollow(FOLLOW_26);
                    lv_verb_12_0=ruleVB();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getClauseRule());
                      							}
                      							set(
                      								current,
                      								"verb",
                      								lv_verb_12_0,
                      								"org.verispec.resa.Resa.VB");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalResa.g:1425:5: ( (lv_apredicate_13_0= ruleActivePredicate ) )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==RULE_ID||(LA23_0>=RULE_STRING && LA23_0<=RULE_INT)) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // InternalResa.g:1426:6: (lv_apredicate_13_0= ruleActivePredicate )
                            {
                            // InternalResa.g:1426:6: (lv_apredicate_13_0= ruleActivePredicate )
                            // InternalResa.g:1427:7: lv_apredicate_13_0= ruleActivePredicate
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getClauseAccess().getApredicateActivePredicateParserRuleCall_2_1_2_0());
                              						
                            }
                            pushFollow(FOLLOW_27);
                            lv_apredicate_13_0=ruleActivePredicate();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getClauseRule());
                              							}
                              							set(
                              								current,
                              								"apredicate",
                              								lv_apredicate_13_0,
                              								"org.verispec.resa.Resa.ActivePredicate");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalResa.g:1446:4: ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) )
                    {
                    // InternalResa.g:1446:4: ( ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) ) )
                    // InternalResa.g:1447:5: ( (lv_tobe_14_0= ruleTOBE ) ) (otherlv_15= 'not' )? ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) )
                    {
                    // InternalResa.g:1447:5: ( (lv_tobe_14_0= ruleTOBE ) )
                    // InternalResa.g:1448:6: (lv_tobe_14_0= ruleTOBE )
                    {
                    // InternalResa.g:1448:6: (lv_tobe_14_0= ruleTOBE )
                    // InternalResa.g:1449:7: lv_tobe_14_0= ruleTOBE
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getClauseAccess().getTobeTOBEParserRuleCall_2_2_0_0());
                      						
                    }
                    pushFollow(FOLLOW_31);
                    lv_tobe_14_0=ruleTOBE();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getClauseRule());
                      							}
                      							set(
                      								current,
                      								"tobe",
                      								lv_tobe_14_0,
                      								"org.verispec.resa.Resa.TOBE");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalResa.g:1466:5: (otherlv_15= 'not' )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==26) ) {
                        alt24=1;
                    }
                    switch (alt24) {
                        case 1 :
                            // InternalResa.g:1467:6: otherlv_15= 'not'
                            {
                            otherlv_15=(Token)match(input,26,FOLLOW_31); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_15, grammarAccess.getClauseAccess().getNotKeyword_2_2_1());
                              					
                            }

                            }
                            break;

                    }

                    // InternalResa.g:1472:5: ( ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? ) | ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) ) | ( (lv_adj_20_0= ruleADJ ) ) )
                    int alt26=3;
                    switch ( input.LA(1) ) {
                    case RULE_ID:
                        {
                        alt26=1;
                        }
                        break;
                    case 28:
                        {
                        alt26=2;
                        }
                        break;
                    case RULE_STRING:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                        {
                        alt26=3;
                        }
                        break;
                    default:
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 26, 0, input);

                        throw nvae;
                    }

                    switch (alt26) {
                        case 1 :
                            // InternalResa.g:1473:6: ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? )
                            {
                            // InternalResa.g:1473:6: ( ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )? )
                            // InternalResa.g:1474:7: ( (lv_verb_16_0= ruleVB ) ) ( (lv_apredicate_17_0= rulePassivePredicate ) )?
                            {
                            // InternalResa.g:1474:7: ( (lv_verb_16_0= ruleVB ) )
                            // InternalResa.g:1475:8: (lv_verb_16_0= ruleVB )
                            {
                            // InternalResa.g:1475:8: (lv_verb_16_0= ruleVB )
                            // InternalResa.g:1476:9: lv_verb_16_0= ruleVB
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getClauseAccess().getVerbVBParserRuleCall_2_2_2_0_0_0());
                              								
                            }
                            pushFollow(FOLLOW_29);
                            lv_verb_16_0=ruleVB();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getClauseRule());
                              									}
                              									set(
                              										current,
                              										"verb",
                              										lv_verb_16_0,
                              										"org.verispec.resa.Resa.VB");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }

                            // InternalResa.g:1493:7: ( (lv_apredicate_17_0= rulePassivePredicate ) )?
                            int alt25=2;
                            int LA25_0 = input.LA(1);

                            if ( (LA25_0==RULE_ID||(LA25_0>=RULE_STRING && LA25_0<=RULE_INT)||LA25_0==37||(LA25_0>=39 && LA25_0<=40)) ) {
                                alt25=1;
                            }
                            else if ( (LA25_0==38) ) {
                                int LA25_2 = input.LA(2);

                                if ( (LA25_2==RULE_INT) ) {
                                    int LA25_4 = input.LA(3);

                                    if ( (LA25_4==11) ) {
                                        int LA25_5 = input.LA(4);

                                        if ( (LA25_5==EOF||(LA25_5>=13 && LA25_5<=14)||LA25_5==18) ) {
                                            alt25=1;
                                        }
                                        else if ( (LA25_5==RULE_INT) ) {
                                            int LA25_6 = input.LA(5);

                                            if ( (LA25_6==EOF||LA25_6==11||(LA25_6>=14 && LA25_6<=17)||(LA25_6>=19 && LA25_6<=23)||LA25_6==25||LA25_6==29||(LA25_6>=38 && LA25_6<=40)||(LA25_6>=49 && LA25_6<=54)||(LA25_6>=59 && LA25_6<=60)||(LA25_6>=62 && LA25_6<=75)) ) {
                                                alt25=1;
                                            }
                                        }
                                    }
                                    else if ( (LA25_4==EOF||(LA25_4>=14 && LA25_4<=17)||(LA25_4>=19 && LA25_4<=23)||LA25_4==25||LA25_4==29||(LA25_4>=38 && LA25_4<=40)||(LA25_4>=49 && LA25_4<=54)||(LA25_4>=59 && LA25_4<=60)||(LA25_4>=62 && LA25_4<=75)) ) {
                                        alt25=1;
                                    }
                                }
                                else if ( (LA25_2==RULE_ID||LA25_2==RULE_STRING) ) {
                                    alt25=1;
                                }
                            }
                            switch (alt25) {
                                case 1 :
                                    // InternalResa.g:1494:8: (lv_apredicate_17_0= rulePassivePredicate )
                                    {
                                    // InternalResa.g:1494:8: (lv_apredicate_17_0= rulePassivePredicate )
                                    // InternalResa.g:1495:9: lv_apredicate_17_0= rulePassivePredicate
                                    {
                                    if ( state.backtracking==0 ) {

                                      									newCompositeNode(grammarAccess.getClauseAccess().getApredicatePassivePredicateParserRuleCall_2_2_2_0_1_0());
                                      								
                                    }
                                    pushFollow(FOLLOW_27);
                                    lv_apredicate_17_0=rulePassivePredicate();

                                    state._fsp--;
                                    if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      									if (current==null) {
                                      										current = createModelElementForParent(grammarAccess.getClauseRule());
                                      									}
                                      									set(
                                      										current,
                                      										"apredicate",
                                      										lv_apredicate_17_0,
                                      										"org.verispec.resa.Resa.PassivePredicate");
                                      									afterParserOrEnumRuleCall();
                                      								
                                    }

                                    }


                                    }
                                    break;

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalResa.g:1514:6: ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) )
                            {
                            // InternalResa.g:1514:6: ( ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) ) )
                            // InternalResa.g:1515:7: ( (lv_setto_18_0= 'set_to' ) ) ( (lv_right_19_0= ruleVariable ) )
                            {
                            // InternalResa.g:1515:7: ( (lv_setto_18_0= 'set_to' ) )
                            // InternalResa.g:1516:8: (lv_setto_18_0= 'set_to' )
                            {
                            // InternalResa.g:1516:8: (lv_setto_18_0= 'set_to' )
                            // InternalResa.g:1517:9: lv_setto_18_0= 'set_to'
                            {
                            lv_setto_18_0=(Token)match(input,28,FOLLOW_5); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									newLeafNode(lv_setto_18_0, grammarAccess.getClauseAccess().getSettoSet_toKeyword_2_2_2_1_0_0());
                              								
                            }
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElement(grammarAccess.getClauseRule());
                              									}
                              									setWithLastConsumed(current, "setto", lv_setto_18_0, "set_to");
                              								
                            }

                            }


                            }

                            // InternalResa.g:1529:7: ( (lv_right_19_0= ruleVariable ) )
                            // InternalResa.g:1530:8: (lv_right_19_0= ruleVariable )
                            {
                            // InternalResa.g:1530:8: (lv_right_19_0= ruleVariable )
                            // InternalResa.g:1531:9: lv_right_19_0= ruleVariable
                            {
                            if ( state.backtracking==0 ) {

                              									newCompositeNode(grammarAccess.getClauseAccess().getRightVariableParserRuleCall_2_2_2_1_1_0());
                              								
                            }
                            pushFollow(FOLLOW_27);
                            lv_right_19_0=ruleVariable();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              									if (current==null) {
                              										current = createModelElementForParent(grammarAccess.getClauseRule());
                              									}
                              									set(
                              										current,
                              										"right",
                              										lv_right_19_0,
                              										"org.verispec.resa.Resa.Variable");
                              									afterParserOrEnumRuleCall();
                              								
                            }

                            }


                            }


                            }


                            }
                            break;
                        case 3 :
                            // InternalResa.g:1550:6: ( (lv_adj_20_0= ruleADJ ) )
                            {
                            // InternalResa.g:1550:6: ( (lv_adj_20_0= ruleADJ ) )
                            // InternalResa.g:1551:7: (lv_adj_20_0= ruleADJ )
                            {
                            // InternalResa.g:1551:7: (lv_adj_20_0= ruleADJ )
                            // InternalResa.g:1552:8: lv_adj_20_0= ruleADJ
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getClauseAccess().getAdjADJParserRuleCall_2_2_2_2_0());
                              							
                            }
                            pushFollow(FOLLOW_27);
                            lv_adj_20_0=ruleADJ();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getClauseRule());
                              								}
                              								set(
                              									current,
                              									"adj",
                              									lv_adj_20_0,
                              									"org.verispec.resa.Resa.ADJ");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // InternalResa.g:1572:3: ( (lv_comp_21_0= ruleComplement ) )?
            int alt28=2;
            switch ( input.LA(1) ) {
                case 20:
                case 38:
                case 49:
                case 50:
                case 53:
                case 54:
                case 59:
                case 60:
                    {
                    alt28=1;
                    }
                    break;
                case 51:
                    {
                    int LA28_2 = input.LA(2);

                    if ( (LA28_2==RULE_INT) ) {
                        int LA28_5 = input.LA(3);

                        if ( (LA28_5==11) ) {
                            int LA28_6 = input.LA(4);

                            if ( (LA28_6==RULE_INT) ) {
                                int LA28_7 = input.LA(5);

                                if ( ((LA28_7>=55 && LA28_7<=58)) ) {
                                    alt28=1;
                                }
                            }
                        }
                        else if ( ((LA28_5>=55 && LA28_5<=58)) ) {
                            alt28=1;
                        }
                    }
                    }
                    break;
                case 52:
                    {
                    int LA28_3 = input.LA(2);

                    if ( (LA28_3==RULE_INT) ) {
                        int LA28_5 = input.LA(3);

                        if ( (LA28_5==11) ) {
                            int LA28_6 = input.LA(4);

                            if ( (LA28_6==RULE_INT) ) {
                                int LA28_7 = input.LA(5);

                                if ( ((LA28_7>=55 && LA28_7<=58)) ) {
                                    alt28=1;
                                }
                            }
                        }
                        else if ( ((LA28_5>=55 && LA28_5<=58)) ) {
                            alt28=1;
                        }
                    }
                    }
                    break;
            }

            switch (alt28) {
                case 1 :
                    // InternalResa.g:1573:4: (lv_comp_21_0= ruleComplement )
                    {
                    // InternalResa.g:1573:4: (lv_comp_21_0= ruleComplement )
                    // InternalResa.g:1574:5: lv_comp_21_0= ruleComplement
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getClauseAccess().getCompComplementParserRuleCall_3_0());
                      				
                    }
                    pushFollow(FOLLOW_32);
                    lv_comp_21_0=ruleComplement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getClauseRule());
                      					}
                      					set(
                      						current,
                      						"comp",
                      						lv_comp_21_0,
                      						"org.verispec.resa.Resa.Complement");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            // InternalResa.g:1591:3: (otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==29) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalResa.g:1592:4: otherlv_22= 'in_order_to' ( (lv_rational_23_0= ruleText ) )
                    {
                    otherlv_22=(Token)match(input,29,FOLLOW_33); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_22, grammarAccess.getClauseAccess().getIn_order_toKeyword_4_0());
                      			
                    }
                    // InternalResa.g:1596:4: ( (lv_rational_23_0= ruleText ) )
                    // InternalResa.g:1597:5: (lv_rational_23_0= ruleText )
                    {
                    // InternalResa.g:1597:5: (lv_rational_23_0= ruleText )
                    // InternalResa.g:1598:6: lv_rational_23_0= ruleText
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getClauseAccess().getRationalTextParserRuleCall_4_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_rational_23_0=ruleText();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getClauseRule());
                      						}
                      						set(
                      							current,
                      							"rational",
                      							lv_rational_23_0,
                      							"org.verispec.resa.Resa.Text");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClause"


    // $ANTLR start "entryRulePossession"
    // InternalResa.g:1620:1: entryRulePossession returns [EObject current=null] : iv_rulePossession= rulePossession EOF ;
    public final EObject entryRulePossession() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePossession = null;


        try {
            // InternalResa.g:1620:51: (iv_rulePossession= rulePossession EOF )
            // InternalResa.g:1621:2: iv_rulePossession= rulePossession EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPossessionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePossession=rulePossession();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePossession; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePossession"


    // $ANTLR start "rulePossession"
    // InternalResa.g:1627:1: rulePossession returns [EObject current=null] : ( ( (lv_status_0_0= 'The_status_of' ) ) | ( (lv_state_1_0= 'The_state_of' ) ) | ( (lv_mode_2_0= 'The_mode_of' ) ) ) ;
    public final EObject rulePossession() throws RecognitionException {
        EObject current = null;

        Token lv_status_0_0=null;
        Token lv_state_1_0=null;
        Token lv_mode_2_0=null;


        	enterRule();

        try {
            // InternalResa.g:1633:2: ( ( ( (lv_status_0_0= 'The_status_of' ) ) | ( (lv_state_1_0= 'The_state_of' ) ) | ( (lv_mode_2_0= 'The_mode_of' ) ) ) )
            // InternalResa.g:1634:2: ( ( (lv_status_0_0= 'The_status_of' ) ) | ( (lv_state_1_0= 'The_state_of' ) ) | ( (lv_mode_2_0= 'The_mode_of' ) ) )
            {
            // InternalResa.g:1634:2: ( ( (lv_status_0_0= 'The_status_of' ) ) | ( (lv_state_1_0= 'The_state_of' ) ) | ( (lv_mode_2_0= 'The_mode_of' ) ) )
            int alt30=3;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt30=1;
                }
                break;
            case 31:
                {
                alt30=2;
                }
                break;
            case 32:
                {
                alt30=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // InternalResa.g:1635:3: ( (lv_status_0_0= 'The_status_of' ) )
                    {
                    // InternalResa.g:1635:3: ( (lv_status_0_0= 'The_status_of' ) )
                    // InternalResa.g:1636:4: (lv_status_0_0= 'The_status_of' )
                    {
                    // InternalResa.g:1636:4: (lv_status_0_0= 'The_status_of' )
                    // InternalResa.g:1637:5: lv_status_0_0= 'The_status_of'
                    {
                    lv_status_0_0=(Token)match(input,30,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_status_0_0, grammarAccess.getPossessionAccess().getStatusThe_status_ofKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getPossessionRule());
                      					}
                      					setWithLastConsumed(current, "status", lv_status_0_0, "The_status_of");
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:1650:3: ( (lv_state_1_0= 'The_state_of' ) )
                    {
                    // InternalResa.g:1650:3: ( (lv_state_1_0= 'The_state_of' ) )
                    // InternalResa.g:1651:4: (lv_state_1_0= 'The_state_of' )
                    {
                    // InternalResa.g:1651:4: (lv_state_1_0= 'The_state_of' )
                    // InternalResa.g:1652:5: lv_state_1_0= 'The_state_of'
                    {
                    lv_state_1_0=(Token)match(input,31,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_state_1_0, grammarAccess.getPossessionAccess().getStateThe_state_ofKeyword_1_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getPossessionRule());
                      					}
                      					setWithLastConsumed(current, "state", lv_state_1_0, "The_state_of");
                      				
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalResa.g:1665:3: ( (lv_mode_2_0= 'The_mode_of' ) )
                    {
                    // InternalResa.g:1665:3: ( (lv_mode_2_0= 'The_mode_of' ) )
                    // InternalResa.g:1666:4: (lv_mode_2_0= 'The_mode_of' )
                    {
                    // InternalResa.g:1666:4: (lv_mode_2_0= 'The_mode_of' )
                    // InternalResa.g:1667:5: lv_mode_2_0= 'The_mode_of'
                    {
                    lv_mode_2_0=(Token)match(input,32,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_mode_2_0, grammarAccess.getPossessionAccess().getModeThe_mode_ofKeyword_2_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getPossessionRule());
                      					}
                      					setWithLastConsumed(current, "mode", lv_mode_2_0, "The_mode_of");
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePossession"


    // $ANTLR start "entryRuleVerbNegation"
    // InternalResa.g:1683:1: entryRuleVerbNegation returns [String current=null] : iv_ruleVerbNegation= ruleVerbNegation EOF ;
    public final String entryRuleVerbNegation() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVerbNegation = null;


        try {
            // InternalResa.g:1683:52: (iv_ruleVerbNegation= ruleVerbNegation EOF )
            // InternalResa.g:1684:2: iv_ruleVerbNegation= ruleVerbNegation EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVerbNegationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVerbNegation=ruleVerbNegation();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVerbNegation.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVerbNegation"


    // $ANTLR start "ruleVerbNegation"
    // InternalResa.g:1690:1: ruleVerbNegation returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'does_not' | kw= 'do_not' | kw= 'doesn\\'t' | kw= 'don\\'t' ) ;
    public final AntlrDatatypeRuleToken ruleVerbNegation() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalResa.g:1696:2: ( (kw= 'does_not' | kw= 'do_not' | kw= 'doesn\\'t' | kw= 'don\\'t' ) )
            // InternalResa.g:1697:2: (kw= 'does_not' | kw= 'do_not' | kw= 'doesn\\'t' | kw= 'don\\'t' )
            {
            // InternalResa.g:1697:2: (kw= 'does_not' | kw= 'do_not' | kw= 'doesn\\'t' | kw= 'don\\'t' )
            int alt31=4;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt31=1;
                }
                break;
            case 34:
                {
                alt31=2;
                }
                break;
            case 35:
                {
                alt31=3;
                }
                break;
            case 36:
                {
                alt31=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalResa.g:1698:3: kw= 'does_not'
                    {
                    kw=(Token)match(input,33,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getVerbNegationAccess().getDoes_notKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:1704:3: kw= 'do_not'
                    {
                    kw=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getVerbNegationAccess().getDo_notKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:1710:3: kw= 'doesn\\'t'
                    {
                    kw=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getVerbNegationAccess().getDoesnTKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:1716:3: kw= 'don\\'t'
                    {
                    kw=(Token)match(input,36,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getVerbNegationAccess().getDonTKeyword_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVerbNegation"


    // $ANTLR start "entryRuleText"
    // InternalResa.g:1725:1: entryRuleText returns [EObject current=null] : iv_ruleText= ruleText EOF ;
    public final EObject entryRuleText() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleText = null;


        try {
            // InternalResa.g:1725:45: (iv_ruleText= ruleText EOF )
            // InternalResa.g:1726:2: iv_ruleText= ruleText EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTextRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleText=ruleText();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleText; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleText"


    // $ANTLR start "ruleText"
    // InternalResa.g:1732:1: ruleText returns [EObject current=null] : ( ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )=> ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) ) )+ ;
    public final EObject ruleText() throws RecognitionException {
        EObject current = null;

        Token lv_text_0_1=null;
        Token lv_text_0_2=null;


        	enterRule();

        try {
            // InternalResa.g:1738:2: ( ( ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )=> ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) ) )+ )
            // InternalResa.g:1739:2: ( ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )=> ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) ) )+
            {
            // InternalResa.g:1739:2: ( ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )=> ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) ) )+
            int cnt33=0;
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==RULE_ID) ) {
                    int LA33_2 = input.LA(2);

                    if ( (synpred1_InternalResa()) ) {
                        alt33=1;
                    }


                }
                else if ( (LA33_0==RULE_ANY_OTHER) && (synpred1_InternalResa())) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalResa.g:1740:3: ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )=> ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) )
            	    {
            	    // InternalResa.g:1747:3: ( (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER ) )
            	    // InternalResa.g:1748:4: (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER )
            	    {
            	    // InternalResa.g:1748:4: (lv_text_0_1= RULE_ID | lv_text_0_2= RULE_ANY_OTHER )
            	    int alt32=2;
            	    int LA32_0 = input.LA(1);

            	    if ( (LA32_0==RULE_ID) ) {
            	        alt32=1;
            	    }
            	    else if ( (LA32_0==RULE_ANY_OTHER) ) {
            	        alt32=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 32, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt32) {
            	        case 1 :
            	            // InternalResa.g:1749:5: lv_text_0_1= RULE_ID
            	            {
            	            lv_text_0_1=(Token)match(input,RULE_ID,FOLLOW_34); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(lv_text_0_1, grammarAccess.getTextAccess().getTextIDTerminalRuleCall_0_0());
            	              				
            	            }
            	            if ( state.backtracking==0 ) {

            	              					if (current==null) {
            	              						current = createModelElement(grammarAccess.getTextRule());
            	              					}
            	              					addWithLastConsumed(
            	              						current,
            	              						"text",
            	              						lv_text_0_1,
            	              						"org.eclipse.xtext.common.Terminals.ID");
            	              				
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // InternalResa.g:1764:5: lv_text_0_2= RULE_ANY_OTHER
            	            {
            	            lv_text_0_2=(Token)match(input,RULE_ANY_OTHER,FOLLOW_34); if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              					newLeafNode(lv_text_0_2, grammarAccess.getTextAccess().getTextANY_OTHERTerminalRuleCall_0_1());
            	              				
            	            }
            	            if ( state.backtracking==0 ) {

            	              					if (current==null) {
            	              						current = createModelElement(grammarAccess.getTextRule());
            	              					}
            	              					addWithLastConsumed(
            	              						current,
            	              						"text",
            	              						lv_text_0_2,
            	              						"org.eclipse.xtext.common.Terminals.ANY_OTHER");
            	              				
            	            }

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt33 >= 1 ) break loop33;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(33, input);
                        throw eee;
                }
                cnt33++;
            } while (true);


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleText"


    // $ANTLR start "entryRuleActivePredicate"
    // InternalResa.g:1784:1: entryRuleActivePredicate returns [EObject current=null] : iv_ruleActivePredicate= ruleActivePredicate EOF ;
    public final EObject entryRuleActivePredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActivePredicate = null;


        try {
            // InternalResa.g:1784:56: (iv_ruleActivePredicate= ruleActivePredicate EOF )
            // InternalResa.g:1785:2: iv_ruleActivePredicate= ruleActivePredicate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getActivePredicateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleActivePredicate=ruleActivePredicate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleActivePredicate; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActivePredicate"


    // $ANTLR start "ruleActivePredicate"
    // InternalResa.g:1791:1: ruleActivePredicate returns [EObject current=null] : ( ( (lv_obj_0_0= ruleNP ) ) ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )? ) ;
    public final EObject ruleActivePredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_obj_0_0 = null;

        EObject lv_iobj_1_0 = null;

        EObject lv_dobj_4_0 = null;



        	enterRule();

        try {
            // InternalResa.g:1797:2: ( ( ( (lv_obj_0_0= ruleNP ) ) ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )? ) )
            // InternalResa.g:1798:2: ( ( (lv_obj_0_0= ruleNP ) ) ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )? )
            {
            // InternalResa.g:1798:2: ( ( (lv_obj_0_0= ruleNP ) ) ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )? )
            // InternalResa.g:1799:3: ( (lv_obj_0_0= ruleNP ) ) ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )?
            {
            // InternalResa.g:1799:3: ( (lv_obj_0_0= ruleNP ) )
            // InternalResa.g:1800:4: (lv_obj_0_0= ruleNP )
            {
            // InternalResa.g:1800:4: (lv_obj_0_0= ruleNP )
            // InternalResa.g:1801:5: lv_obj_0_0= ruleNP
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getActivePredicateAccess().getObjNPParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_35);
            lv_obj_0_0=ruleNP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getActivePredicateRule());
              					}
              					set(
              						current,
              						"obj",
              						lv_obj_0_0,
              						"org.verispec.resa.Resa.NP");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:1818:3: ( ( (lv_iobj_1_0= ruleNP ) ) | ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) ) )?
            int alt35=3;
            switch ( input.LA(1) ) {
                case RULE_ID:
                case RULE_STRING:
                case RULE_INT:
                    {
                    alt35=1;
                    }
                    break;
                case 37:
                    {
                    alt35=2;
                    }
                    break;
                case 38:
                    {
                    int LA35_3 = input.LA(2);

                    if ( (LA35_3==RULE_INT) ) {
                        int LA35_5 = input.LA(3);

                        if ( (LA35_5==11) ) {
                            int LA35_6 = input.LA(4);

                            if ( (LA35_6==RULE_INT) ) {
                                int LA35_7 = input.LA(5);

                                if ( (LA35_7==EOF||LA35_7==11||(LA35_7>=14 && LA35_7<=17)||(LA35_7>=19 && LA35_7<=23)||LA35_7==25||LA35_7==29||LA35_7==38||(LA35_7>=49 && LA35_7<=54)||(LA35_7>=59 && LA35_7<=60)||(LA35_7>=62 && LA35_7<=75)) ) {
                                    alt35=2;
                                }
                            }
                            else if ( (LA35_6==EOF||(LA35_6>=13 && LA35_6<=14)||LA35_6==18) ) {
                                alt35=2;
                            }
                        }
                        else if ( (LA35_5==EOF||(LA35_5>=14 && LA35_5<=17)||(LA35_5>=19 && LA35_5<=23)||LA35_5==25||LA35_5==29||LA35_5==38||(LA35_5>=49 && LA35_5<=54)||(LA35_5>=59 && LA35_5<=60)||(LA35_5>=62 && LA35_5<=75)) ) {
                            alt35=2;
                        }
                    }
                    else if ( (LA35_3==RULE_ID||LA35_3==RULE_STRING) ) {
                        alt35=2;
                    }
                    }
                    break;
            }

            switch (alt35) {
                case 1 :
                    // InternalResa.g:1819:4: ( (lv_iobj_1_0= ruleNP ) )
                    {
                    // InternalResa.g:1819:4: ( (lv_iobj_1_0= ruleNP ) )
                    // InternalResa.g:1820:5: (lv_iobj_1_0= ruleNP )
                    {
                    // InternalResa.g:1820:5: (lv_iobj_1_0= ruleNP )
                    // InternalResa.g:1821:6: lv_iobj_1_0= ruleNP
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getActivePredicateAccess().getIobjNPParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_iobj_1_0=ruleNP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getActivePredicateRule());
                      						}
                      						set(
                      							current,
                      							"iobj",
                      							lv_iobj_1_0,
                      							"org.verispec.resa.Resa.NP");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:1839:4: ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) )
                    {
                    // InternalResa.g:1839:4: ( (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) ) )
                    // InternalResa.g:1840:5: (otherlv_2= 'to' | otherlv_3= 'for' ) ( (lv_dobj_4_0= ruleNP ) )
                    {
                    // InternalResa.g:1840:5: (otherlv_2= 'to' | otherlv_3= 'for' )
                    int alt34=2;
                    int LA34_0 = input.LA(1);

                    if ( (LA34_0==37) ) {
                        alt34=1;
                    }
                    else if ( (LA34_0==38) ) {
                        alt34=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 34, 0, input);

                        throw nvae;
                    }
                    switch (alt34) {
                        case 1 :
                            // InternalResa.g:1841:6: otherlv_2= 'to'
                            {
                            otherlv_2=(Token)match(input,37,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_2, grammarAccess.getActivePredicateAccess().getToKeyword_1_1_0_0());
                              					
                            }

                            }
                            break;
                        case 2 :
                            // InternalResa.g:1846:6: otherlv_3= 'for'
                            {
                            otherlv_3=(Token)match(input,38,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              						newLeafNode(otherlv_3, grammarAccess.getActivePredicateAccess().getForKeyword_1_1_0_1());
                              					
                            }

                            }
                            break;

                    }

                    // InternalResa.g:1851:5: ( (lv_dobj_4_0= ruleNP ) )
                    // InternalResa.g:1852:6: (lv_dobj_4_0= ruleNP )
                    {
                    // InternalResa.g:1852:6: (lv_dobj_4_0= ruleNP )
                    // InternalResa.g:1853:7: lv_dobj_4_0= ruleNP
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getActivePredicateAccess().getDobjNPParserRuleCall_1_1_1_0());
                      						
                    }
                    pushFollow(FOLLOW_2);
                    lv_dobj_4_0=ruleNP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getActivePredicateRule());
                      							}
                      							set(
                      								current,
                      								"dobj",
                      								lv_dobj_4_0,
                      								"org.verispec.resa.Resa.NP");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActivePredicate"


    // $ANTLR start "entryRulePassivePredicate"
    // InternalResa.g:1876:1: entryRulePassivePredicate returns [EObject current=null] : iv_rulePassivePredicate= rulePassivePredicate EOF ;
    public final EObject entryRulePassivePredicate() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePassivePredicate = null;


        try {
            // InternalResa.g:1876:57: (iv_rulePassivePredicate= rulePassivePredicate EOF )
            // InternalResa.g:1877:2: iv_rulePassivePredicate= rulePassivePredicate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPassivePredicateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePassivePredicate=rulePassivePredicate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePassivePredicate; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePassivePredicate"


    // $ANTLR start "rulePassivePredicate"
    // InternalResa.g:1883:1: rulePassivePredicate returns [EObject current=null] : ( ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) ) | ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? ) ) ;
    public final EObject rulePassivePredicate() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_sub_3_0 = null;

        EObject lv_iobj_7_0 = null;

        EObject lv_sub_10_0 = null;



        	enterRule();

        try {
            // InternalResa.g:1889:2: ( ( ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) ) | ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? ) ) )
            // InternalResa.g:1890:2: ( ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) ) | ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? ) )
            {
            // InternalResa.g:1890:2: ( ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) ) | ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? ) )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( ((LA40_0>=39 && LA40_0<=40)) ) {
                alt40=1;
            }
            else if ( (LA40_0==RULE_ID||(LA40_0>=RULE_STRING && LA40_0<=RULE_INT)||(LA40_0>=37 && LA40_0<=38)) ) {
                alt40=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }
            switch (alt40) {
                case 1 :
                    // InternalResa.g:1891:3: ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) )
                    {
                    // InternalResa.g:1891:3: ( () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) ) )
                    // InternalResa.g:1892:4: () (otherlv_1= 'by' | otherlv_2= 'with' ) ( (lv_sub_3_0= ruleNP ) )
                    {
                    // InternalResa.g:1892:4: ()
                    // InternalResa.g:1893:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getPassivePredicateAccess().getTransitiveAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalResa.g:1899:4: (otherlv_1= 'by' | otherlv_2= 'with' )
                    int alt36=2;
                    int LA36_0 = input.LA(1);

                    if ( (LA36_0==39) ) {
                        alt36=1;
                    }
                    else if ( (LA36_0==40) ) {
                        alt36=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 36, 0, input);

                        throw nvae;
                    }
                    switch (alt36) {
                        case 1 :
                            // InternalResa.g:1900:5: otherlv_1= 'by'
                            {
                            otherlv_1=(Token)match(input,39,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_1, grammarAccess.getPassivePredicateAccess().getByKeyword_0_1_0());
                              				
                            }

                            }
                            break;
                        case 2 :
                            // InternalResa.g:1905:5: otherlv_2= 'with'
                            {
                            otherlv_2=(Token)match(input,40,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_2, grammarAccess.getPassivePredicateAccess().getWithKeyword_0_1_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalResa.g:1910:4: ( (lv_sub_3_0= ruleNP ) )
                    // InternalResa.g:1911:5: (lv_sub_3_0= ruleNP )
                    {
                    // InternalResa.g:1911:5: (lv_sub_3_0= ruleNP )
                    // InternalResa.g:1912:6: lv_sub_3_0= ruleNP
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getPassivePredicateAccess().getSubNPParserRuleCall_0_2_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_sub_3_0=ruleNP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getPassivePredicateRule());
                      						}
                      						set(
                      							current,
                      							"sub",
                      							lv_sub_3_0,
                      							"org.verispec.resa.Resa.NP");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:1931:3: ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? )
                    {
                    // InternalResa.g:1931:3: ( () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )? )
                    // InternalResa.g:1932:4: () (otherlv_5= 'to' | otherlv_6= 'for' )? ( (lv_iobj_7_0= ruleNP ) ) ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )?
                    {
                    // InternalResa.g:1932:4: ()
                    // InternalResa.g:1933:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getPassivePredicateAccess().getDitransitiveAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalResa.g:1939:4: (otherlv_5= 'to' | otherlv_6= 'for' )?
                    int alt37=3;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0==37) ) {
                        alt37=1;
                    }
                    else if ( (LA37_0==38) ) {
                        alt37=2;
                    }
                    switch (alt37) {
                        case 1 :
                            // InternalResa.g:1940:5: otherlv_5= 'to'
                            {
                            otherlv_5=(Token)match(input,37,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_5, grammarAccess.getPassivePredicateAccess().getToKeyword_1_1_0());
                              				
                            }

                            }
                            break;
                        case 2 :
                            // InternalResa.g:1945:5: otherlv_6= 'for'
                            {
                            otherlv_6=(Token)match(input,38,FOLLOW_7); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              					newLeafNode(otherlv_6, grammarAccess.getPassivePredicateAccess().getForKeyword_1_1_1());
                              				
                            }

                            }
                            break;

                    }

                    // InternalResa.g:1950:4: ( (lv_iobj_7_0= ruleNP ) )
                    // InternalResa.g:1951:5: (lv_iobj_7_0= ruleNP )
                    {
                    // InternalResa.g:1951:5: (lv_iobj_7_0= ruleNP )
                    // InternalResa.g:1952:6: lv_iobj_7_0= ruleNP
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getPassivePredicateAccess().getIobjNPParserRuleCall_1_2_0());
                      					
                    }
                    pushFollow(FOLLOW_36);
                    lv_iobj_7_0=ruleNP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getPassivePredicateRule());
                      						}
                      						set(
                      							current,
                      							"iobj",
                      							lv_iobj_7_0,
                      							"org.verispec.resa.Resa.NP");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalResa.g:1969:4: ( (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) ) )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( ((LA39_0>=39 && LA39_0<=40)) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // InternalResa.g:1970:5: (otherlv_8= 'by' | otherlv_9= 'with' ) ( (lv_sub_10_0= ruleNP ) )
                            {
                            // InternalResa.g:1970:5: (otherlv_8= 'by' | otherlv_9= 'with' )
                            int alt38=2;
                            int LA38_0 = input.LA(1);

                            if ( (LA38_0==39) ) {
                                alt38=1;
                            }
                            else if ( (LA38_0==40) ) {
                                alt38=2;
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return current;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 38, 0, input);

                                throw nvae;
                            }
                            switch (alt38) {
                                case 1 :
                                    // InternalResa.g:1971:6: otherlv_8= 'by'
                                    {
                                    otherlv_8=(Token)match(input,39,FOLLOW_7); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_8, grammarAccess.getPassivePredicateAccess().getByKeyword_1_3_0_0());
                                      					
                                    }

                                    }
                                    break;
                                case 2 :
                                    // InternalResa.g:1976:6: otherlv_9= 'with'
                                    {
                                    otherlv_9=(Token)match(input,40,FOLLOW_7); if (state.failed) return current;
                                    if ( state.backtracking==0 ) {

                                      						newLeafNode(otherlv_9, grammarAccess.getPassivePredicateAccess().getWithKeyword_1_3_0_1());
                                      					
                                    }

                                    }
                                    break;

                            }

                            // InternalResa.g:1981:5: ( (lv_sub_10_0= ruleNP ) )
                            // InternalResa.g:1982:6: (lv_sub_10_0= ruleNP )
                            {
                            // InternalResa.g:1982:6: (lv_sub_10_0= ruleNP )
                            // InternalResa.g:1983:7: lv_sub_10_0= ruleNP
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getPassivePredicateAccess().getSubNPParserRuleCall_1_3_1_0());
                              						
                            }
                            pushFollow(FOLLOW_2);
                            lv_sub_10_0=ruleNP();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getPassivePredicateRule());
                              							}
                              							set(
                              								current,
                              								"sub",
                              								lv_sub_10_0,
                              								"org.verispec.resa.Resa.NP");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePassivePredicate"


    // $ANTLR start "entryRuleTOBE"
    // InternalResa.g:2006:1: entryRuleTOBE returns [EObject current=null] : iv_ruleTOBE= ruleTOBE EOF ;
    public final EObject entryRuleTOBE() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTOBE = null;


        try {
            // InternalResa.g:2006:45: (iv_ruleTOBE= ruleTOBE EOF )
            // InternalResa.g:2007:2: iv_ruleTOBE= ruleTOBE EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTOBERule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTOBE=ruleTOBE();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTOBE; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTOBE"


    // $ANTLR start "ruleTOBE"
    // InternalResa.g:2013:1: ruleTOBE returns [EObject current=null] : ( (lv_is_0_0= 'is' ) ) ;
    public final EObject ruleTOBE() throws RecognitionException {
        EObject current = null;

        Token lv_is_0_0=null;


        	enterRule();

        try {
            // InternalResa.g:2019:2: ( ( (lv_is_0_0= 'is' ) ) )
            // InternalResa.g:2020:2: ( (lv_is_0_0= 'is' ) )
            {
            // InternalResa.g:2020:2: ( (lv_is_0_0= 'is' ) )
            // InternalResa.g:2021:3: (lv_is_0_0= 'is' )
            {
            // InternalResa.g:2021:3: (lv_is_0_0= 'is' )
            // InternalResa.g:2022:4: lv_is_0_0= 'is'
            {
            lv_is_0_0=(Token)match(input,41,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_is_0_0, grammarAccess.getTOBEAccess().getIsIsKeyword_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getTOBERule());
              				}
              				setWithLastConsumed(current, "is", lv_is_0_0, "is");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTOBE"


    // $ANTLR start "entryRuleVB"
    // InternalResa.g:2037:1: entryRuleVB returns [EObject current=null] : iv_ruleVB= ruleVB EOF ;
    public final EObject entryRuleVB() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVB = null;


        try {
            // InternalResa.g:2037:43: (iv_ruleVB= ruleVB EOF )
            // InternalResa.g:2038:2: iv_ruleVB= ruleVB EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVBRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVB=ruleVB();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVB; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVB"


    // $ANTLR start "ruleVB"
    // InternalResa.g:2044:1: ruleVB returns [EObject current=null] : ( ( (lv_verb_0_0= RULE_ID ) ) ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )? ) ;
    public final EObject ruleVB() throws RecognitionException {
        EObject current = null;

        Token lv_verb_0_0=null;
        EObject lv_adverb_1_0 = null;



        	enterRule();

        try {
            // InternalResa.g:2050:2: ( ( ( (lv_verb_0_0= RULE_ID ) ) ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )? ) )
            // InternalResa.g:2051:2: ( ( (lv_verb_0_0= RULE_ID ) ) ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )? )
            {
            // InternalResa.g:2051:2: ( ( (lv_verb_0_0= RULE_ID ) ) ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )? )
            // InternalResa.g:2052:3: ( (lv_verb_0_0= RULE_ID ) ) ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )?
            {
            // InternalResa.g:2052:3: ( (lv_verb_0_0= RULE_ID ) )
            // InternalResa.g:2053:4: (lv_verb_0_0= RULE_ID )
            {
            // InternalResa.g:2053:4: (lv_verb_0_0= RULE_ID )
            // InternalResa.g:2054:5: lv_verb_0_0= RULE_ID
            {
            lv_verb_0_0=(Token)match(input,RULE_ID,FOLLOW_34); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_verb_0_0, grammarAccess.getVBAccess().getVerbIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVBRule());
              					}
              					setWithLastConsumed(
              						current,
              						"verb",
              						lv_verb_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            // InternalResa.g:2070:3: ( ( ( ruleText ) )=> (lv_adverb_1_0= ruleText ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==RULE_ID) ) {
                int LA41_1 = input.LA(2);

                if ( (synpred2_InternalResa()) ) {
                    alt41=1;
                }
            }
            else if ( (LA41_0==RULE_ANY_OTHER) && (synpred2_InternalResa())) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalResa.g:2071:4: ( ( ruleText ) )=> (lv_adverb_1_0= ruleText )
                    {
                    // InternalResa.g:2075:4: (lv_adverb_1_0= ruleText )
                    // InternalResa.g:2076:5: lv_adverb_1_0= ruleText
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getVBAccess().getAdverbTextParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_adverb_1_0=ruleText();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getVBRule());
                      					}
                      					set(
                      						current,
                      						"adverb",
                      						lv_adverb_1_0,
                      						"org.verispec.resa.Resa.Text");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVB"


    // $ANTLR start "entryRuleMOD"
    // InternalResa.g:2097:1: entryRuleMOD returns [EObject current=null] : iv_ruleMOD= ruleMOD EOF ;
    public final EObject entryRuleMOD() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMOD = null;


        try {
            // InternalResa.g:2097:44: (iv_ruleMOD= ruleMOD EOF )
            // InternalResa.g:2098:2: iv_ruleMOD= ruleMOD EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMODRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMOD=ruleMOD();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMOD; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMOD"


    // $ANTLR start "ruleMOD"
    // InternalResa.g:2104:1: ruleMOD returns [EObject current=null] : ( (lv_mode_0_0= 'shall' ) ) ;
    public final EObject ruleMOD() throws RecognitionException {
        EObject current = null;

        Token lv_mode_0_0=null;


        	enterRule();

        try {
            // InternalResa.g:2110:2: ( ( (lv_mode_0_0= 'shall' ) ) )
            // InternalResa.g:2111:2: ( (lv_mode_0_0= 'shall' ) )
            {
            // InternalResa.g:2111:2: ( (lv_mode_0_0= 'shall' ) )
            // InternalResa.g:2112:3: (lv_mode_0_0= 'shall' )
            {
            // InternalResa.g:2112:3: (lv_mode_0_0= 'shall' )
            // InternalResa.g:2113:4: lv_mode_0_0= 'shall'
            {
            lv_mode_0_0=(Token)match(input,42,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_mode_0_0, grammarAccess.getMODAccess().getModeShallKeyword_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getMODRule());
              				}
              				setWithLastConsumed(current, "mode", lv_mode_0_0, "shall");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMOD"


    // $ANTLR start "entryRuleNP"
    // InternalResa.g:2128:1: entryRuleNP returns [EObject current=null] : iv_ruleNP= ruleNP EOF ;
    public final EObject entryRuleNP() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNP = null;


        try {
            // InternalResa.g:2128:43: (iv_ruleNP= ruleNP EOF )
            // InternalResa.g:2129:2: iv_ruleNP= ruleNP EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNPRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNP=ruleNP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNP; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNP"


    // $ANTLR start "ruleNP"
    // InternalResa.g:2135:1: ruleNP returns [EObject current=null] : (this_System_0= ruleSystem | this_Device_1= ruleDevice | this_Parameter_2= ruleParameter | this_User_3= ruleUser | this_Event_4= ruleEvent | this_Status_5= ruleStatus | this_Mode_6= ruleMode | this_State_7= ruleState | this_Entity_8= ruleEntity | this_Attribute_9= ruleAttribute | ( ( ruleVariable )=>this_Variable_10= ruleVariable ) | ( ( ruleLiteral )=>this_Literal_11= ruleLiteral ) | this_Numeric_12= ruleNumeric | ( (otherlv_13= RULE_ID ) ) ) ;
    public final EObject ruleNP() throws RecognitionException {
        EObject current = null;

        Token otherlv_13=null;
        EObject this_System_0 = null;

        EObject this_Device_1 = null;

        EObject this_Parameter_2 = null;

        EObject this_User_3 = null;

        EObject this_Event_4 = null;

        EObject this_Status_5 = null;

        EObject this_Mode_6 = null;

        EObject this_State_7 = null;

        EObject this_Entity_8 = null;

        EObject this_Attribute_9 = null;

        EObject this_Variable_10 = null;

        EObject this_Literal_11 = null;

        EObject this_Numeric_12 = null;



        	enterRule();

        try {
            // InternalResa.g:2141:2: ( (this_System_0= ruleSystem | this_Device_1= ruleDevice | this_Parameter_2= ruleParameter | this_User_3= ruleUser | this_Event_4= ruleEvent | this_Status_5= ruleStatus | this_Mode_6= ruleMode | this_State_7= ruleState | this_Entity_8= ruleEntity | this_Attribute_9= ruleAttribute | ( ( ruleVariable )=>this_Variable_10= ruleVariable ) | ( ( ruleLiteral )=>this_Literal_11= ruleLiteral ) | this_Numeric_12= ruleNumeric | ( (otherlv_13= RULE_ID ) ) ) )
            // InternalResa.g:2142:2: (this_System_0= ruleSystem | this_Device_1= ruleDevice | this_Parameter_2= ruleParameter | this_User_3= ruleUser | this_Event_4= ruleEvent | this_Status_5= ruleStatus | this_Mode_6= ruleMode | this_State_7= ruleState | this_Entity_8= ruleEntity | this_Attribute_9= ruleAttribute | ( ( ruleVariable )=>this_Variable_10= ruleVariable ) | ( ( ruleLiteral )=>this_Literal_11= ruleLiteral ) | this_Numeric_12= ruleNumeric | ( (otherlv_13= RULE_ID ) ) )
            {
            // InternalResa.g:2142:2: (this_System_0= ruleSystem | this_Device_1= ruleDevice | this_Parameter_2= ruleParameter | this_User_3= ruleUser | this_Event_4= ruleEvent | this_Status_5= ruleStatus | this_Mode_6= ruleMode | this_State_7= ruleState | this_Entity_8= ruleEntity | this_Attribute_9= ruleAttribute | ( ( ruleVariable )=>this_Variable_10= ruleVariable ) | ( ( ruleLiteral )=>this_Literal_11= ruleLiteral ) | this_Numeric_12= ruleNumeric | ( (otherlv_13= RULE_ID ) ) )
            int alt42=14;
            alt42 = dfa42.predict(input);
            switch (alt42) {
                case 1 :
                    // InternalResa.g:2143:3: this_System_0= ruleSystem
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getSystemParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_System_0=ruleSystem();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_System_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:2152:3: this_Device_1= ruleDevice
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getDeviceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Device_1=ruleDevice();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Device_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:2161:3: this_Parameter_2= ruleParameter
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getParameterParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Parameter_2=ruleParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Parameter_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:2170:3: this_User_3= ruleUser
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getUserParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_User_3=ruleUser();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_User_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalResa.g:2179:3: this_Event_4= ruleEvent
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getEventParserRuleCall_4());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Event_4=ruleEvent();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Event_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 6 :
                    // InternalResa.g:2188:3: this_Status_5= ruleStatus
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getStatusParserRuleCall_5());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Status_5=ruleStatus();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Status_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 7 :
                    // InternalResa.g:2197:3: this_Mode_6= ruleMode
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getModeParserRuleCall_6());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Mode_6=ruleMode();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Mode_6;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 8 :
                    // InternalResa.g:2206:3: this_State_7= ruleState
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getStateParserRuleCall_7());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_State_7=ruleState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_State_7;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 9 :
                    // InternalResa.g:2215:3: this_Entity_8= ruleEntity
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getEntityParserRuleCall_8());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Entity_8=ruleEntity();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Entity_8;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 10 :
                    // InternalResa.g:2224:3: this_Attribute_9= ruleAttribute
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getAttributeParserRuleCall_9());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Attribute_9=ruleAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Attribute_9;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 11 :
                    // InternalResa.g:2233:3: ( ( ruleVariable )=>this_Variable_10= ruleVariable )
                    {
                    // InternalResa.g:2233:3: ( ( ruleVariable )=>this_Variable_10= ruleVariable )
                    // InternalResa.g:2234:4: ( ruleVariable )=>this_Variable_10= ruleVariable
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getNPAccess().getVariableParserRuleCall_10());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_Variable_10=ruleVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_Variable_10;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 12 :
                    // InternalResa.g:2245:3: ( ( ruleLiteral )=>this_Literal_11= ruleLiteral )
                    {
                    // InternalResa.g:2245:3: ( ( ruleLiteral )=>this_Literal_11= ruleLiteral )
                    // InternalResa.g:2246:4: ( ruleLiteral )=>this_Literal_11= ruleLiteral
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getNPAccess().getLiteralParserRuleCall_11());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_Literal_11=ruleLiteral();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_Literal_11;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 13 :
                    // InternalResa.g:2257:3: this_Numeric_12= ruleNumeric
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getNPAccess().getNumericParserRuleCall_12());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Numeric_12=ruleNumeric();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Numeric_12;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 14 :
                    // InternalResa.g:2266:3: ( (otherlv_13= RULE_ID ) )
                    {
                    // InternalResa.g:2266:3: ( (otherlv_13= RULE_ID ) )
                    // InternalResa.g:2267:4: (otherlv_13= RULE_ID )
                    {
                    // InternalResa.g:2267:4: (otherlv_13= RULE_ID )
                    // InternalResa.g:2268:5: otherlv_13= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getNPRule());
                      					}
                      				
                    }
                    otherlv_13=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_13, grammarAccess.getNPAccess().getTypedResaDataTypeCrossReference_13_0());
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNP"


    // $ANTLR start "entryRuleLiteral"
    // InternalResa.g:2283:1: entryRuleLiteral returns [EObject current=null] : iv_ruleLiteral= ruleLiteral EOF ;
    public final EObject entryRuleLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteral = null;


        try {
            // InternalResa.g:2283:48: (iv_ruleLiteral= ruleLiteral EOF )
            // InternalResa.g:2284:2: iv_ruleLiteral= ruleLiteral EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLiteralRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleLiteral=ruleLiteral();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLiteral; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalResa.g:2290:1: ruleLiteral returns [EObject current=null] : ( (lv_name_0_0= RULE_STRING ) ) ;
    public final EObject ruleLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;


        	enterRule();

        try {
            // InternalResa.g:2296:2: ( ( (lv_name_0_0= RULE_STRING ) ) )
            // InternalResa.g:2297:2: ( (lv_name_0_0= RULE_STRING ) )
            {
            // InternalResa.g:2297:2: ( (lv_name_0_0= RULE_STRING ) )
            // InternalResa.g:2298:3: (lv_name_0_0= RULE_STRING )
            {
            // InternalResa.g:2298:3: (lv_name_0_0= RULE_STRING )
            // InternalResa.g:2299:4: lv_name_0_0= RULE_STRING
            {
            lv_name_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(lv_name_0_0, grammarAccess.getLiteralAccess().getNameSTRINGTerminalRuleCall_0());
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getLiteralRule());
              				}
              				setWithLastConsumed(
              					current,
              					"name",
              					lv_name_0_0,
              					"org.eclipse.xtext.common.Terminals.STRING");
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleVariable"
    // InternalResa.g:2318:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalResa.g:2318:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalResa.g:2319:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalResa.g:2325:1: ruleVariable returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':var' ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalResa.g:2331:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':var' ) )
            // InternalResa.g:2332:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':var' )
            {
            // InternalResa.g:2332:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':var' )
            // InternalResa.g:2333:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':var'
            {
            // InternalResa.g:2333:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalResa.g:2334:4: (lv_name_0_0= RULE_ID )
            {
            // InternalResa.g:2334:4: (lv_name_0_0= RULE_ID )
            // InternalResa.g:2335:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_37); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_0_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getVariableRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_0_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_1=(Token)match(input,43,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getVariableAccess().getVarKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleADJ"
    // InternalResa.g:2359:1: entryRuleADJ returns [EObject current=null] : iv_ruleADJ= ruleADJ EOF ;
    public final EObject entryRuleADJ() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleADJ = null;


        try {
            // InternalResa.g:2359:44: (iv_ruleADJ= ruleADJ EOF )
            // InternalResa.g:2360:2: iv_ruleADJ= ruleADJ EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getADJRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleADJ=ruleADJ();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleADJ; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleADJ"


    // $ANTLR start "ruleADJ"
    // InternalResa.g:2366:1: ruleADJ returns [EObject current=null] : (this_State_0= ruleState | this_Mode_1= ruleMode | this_Comparison_2= ruleComparison ) ;
    public final EObject ruleADJ() throws RecognitionException {
        EObject current = null;

        EObject this_State_0 = null;

        EObject this_Mode_1 = null;

        EObject this_Comparison_2 = null;



        	enterRule();

        try {
            // InternalResa.g:2372:2: ( (this_State_0= ruleState | this_Mode_1= ruleMode | this_Comparison_2= ruleComparison ) )
            // InternalResa.g:2373:2: (this_State_0= ruleState | this_Mode_1= ruleMode | this_Comparison_2= ruleComparison )
            {
            // InternalResa.g:2373:2: (this_State_0= ruleState | this_Mode_1= ruleMode | this_Comparison_2= ruleComparison )
            int alt43=3;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==RULE_STRING) ) {
                int LA43_1 = input.LA(2);

                if ( (LA43_1==82) ) {
                    alt43=1;
                }
                else if ( (LA43_1==83) ) {
                    alt43=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 1, input);

                    throw nvae;
                }
            }
            else if ( ((LA43_0>=44 && LA43_0<=48)) ) {
                alt43=3;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }
            switch (alt43) {
                case 1 :
                    // InternalResa.g:2374:3: this_State_0= ruleState
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getADJAccess().getStateParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_State_0=ruleState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_State_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:2383:3: this_Mode_1= ruleMode
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getADJAccess().getModeParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Mode_1=ruleMode();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Mode_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:2392:3: this_Comparison_2= ruleComparison
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getADJAccess().getComparisonParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Comparison_2=ruleComparison();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Comparison_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleADJ"


    // $ANTLR start "entryRuleComparison"
    // InternalResa.g:2404:1: entryRuleComparison returns [EObject current=null] : iv_ruleComparison= ruleComparison EOF ;
    public final EObject entryRuleComparison() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparison = null;


        try {
            // InternalResa.g:2404:51: (iv_ruleComparison= ruleComparison EOF )
            // InternalResa.g:2405:2: iv_ruleComparison= ruleComparison EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComparisonRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComparison=ruleComparison();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComparison; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparison"


    // $ANTLR start "ruleComparison"
    // InternalResa.g:2411:1: ruleComparison returns [EObject current=null] : ( ( (lv_op_0_0= ruleComparisonOperator ) ) ( (lv_obj_1_0= ruleNP ) ) ) ;
    public final EObject ruleComparison() throws RecognitionException {
        EObject current = null;

        EObject lv_op_0_0 = null;

        EObject lv_obj_1_0 = null;



        	enterRule();

        try {
            // InternalResa.g:2417:2: ( ( ( (lv_op_0_0= ruleComparisonOperator ) ) ( (lv_obj_1_0= ruleNP ) ) ) )
            // InternalResa.g:2418:2: ( ( (lv_op_0_0= ruleComparisonOperator ) ) ( (lv_obj_1_0= ruleNP ) ) )
            {
            // InternalResa.g:2418:2: ( ( (lv_op_0_0= ruleComparisonOperator ) ) ( (lv_obj_1_0= ruleNP ) ) )
            // InternalResa.g:2419:3: ( (lv_op_0_0= ruleComparisonOperator ) ) ( (lv_obj_1_0= ruleNP ) )
            {
            // InternalResa.g:2419:3: ( (lv_op_0_0= ruleComparisonOperator ) )
            // InternalResa.g:2420:4: (lv_op_0_0= ruleComparisonOperator )
            {
            // InternalResa.g:2420:4: (lv_op_0_0= ruleComparisonOperator )
            // InternalResa.g:2421:5: lv_op_0_0= ruleComparisonOperator
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getComparisonAccess().getOpComparisonOperatorParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_7);
            lv_op_0_0=ruleComparisonOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getComparisonRule());
              					}
              					set(
              						current,
              						"op",
              						lv_op_0_0,
              						"org.verispec.resa.Resa.ComparisonOperator");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:2438:3: ( (lv_obj_1_0= ruleNP ) )
            // InternalResa.g:2439:4: (lv_obj_1_0= ruleNP )
            {
            // InternalResa.g:2439:4: (lv_obj_1_0= ruleNP )
            // InternalResa.g:2440:5: lv_obj_1_0= ruleNP
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getComparisonAccess().getObjNPParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_obj_1_0=ruleNP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getComparisonRule());
              					}
              					set(
              						current,
              						"obj",
              						lv_obj_1_0,
              						"org.verispec.resa.Resa.NP");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparison"


    // $ANTLR start "entryRuleComparisonOperator"
    // InternalResa.g:2461:1: entryRuleComparisonOperator returns [EObject current=null] : iv_ruleComparisonOperator= ruleComparisonOperator EOF ;
    public final EObject entryRuleComparisonOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComparisonOperator = null;


        try {
            // InternalResa.g:2461:59: (iv_ruleComparisonOperator= ruleComparisonOperator EOF )
            // InternalResa.g:2462:2: iv_ruleComparisonOperator= ruleComparisonOperator EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComparisonOperatorRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleComparisonOperator=ruleComparisonOperator();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComparisonOperator; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComparisonOperator"


    // $ANTLR start "ruleComparisonOperator"
    // InternalResa.g:2468:1: ruleComparisonOperator returns [EObject current=null] : ( ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) ) ) ;
    public final EObject ruleComparisonOperator() throws RecognitionException {
        EObject current = null;

        Token lv_compop_0_1=null;
        Token lv_compop_0_2=null;
        Token lv_compop_0_3=null;
        Token lv_compop_0_4=null;
        Token lv_compop_0_5=null;


        	enterRule();

        try {
            // InternalResa.g:2474:2: ( ( ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) ) ) )
            // InternalResa.g:2475:2: ( ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) ) )
            {
            // InternalResa.g:2475:2: ( ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) ) )
            // InternalResa.g:2476:3: ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) )
            {
            // InternalResa.g:2476:3: ( (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' ) )
            // InternalResa.g:2477:4: (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' )
            {
            // InternalResa.g:2477:4: (lv_compop_0_1= 'equal_to' | lv_compop_0_2= 'less_than' | lv_compop_0_3= 'greater_than' | lv_compop_0_4= 'less_than_or_equalto' | lv_compop_0_5= 'greater_than_or_equal_to' )
            int alt44=5;
            switch ( input.LA(1) ) {
            case 44:
                {
                alt44=1;
                }
                break;
            case 45:
                {
                alt44=2;
                }
                break;
            case 46:
                {
                alt44=3;
                }
                break;
            case 47:
                {
                alt44=4;
                }
                break;
            case 48:
                {
                alt44=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }

            switch (alt44) {
                case 1 :
                    // InternalResa.g:2478:5: lv_compop_0_1= 'equal_to'
                    {
                    lv_compop_0_1=(Token)match(input,44,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_compop_0_1, grammarAccess.getComparisonOperatorAccess().getCompopEqual_toKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getComparisonOperatorRule());
                      					}
                      					setWithLastConsumed(current, "compop", lv_compop_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:2489:5: lv_compop_0_2= 'less_than'
                    {
                    lv_compop_0_2=(Token)match(input,45,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_compop_0_2, grammarAccess.getComparisonOperatorAccess().getCompopLess_thanKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getComparisonOperatorRule());
                      					}
                      					setWithLastConsumed(current, "compop", lv_compop_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:2500:5: lv_compop_0_3= 'greater_than'
                    {
                    lv_compop_0_3=(Token)match(input,46,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_compop_0_3, grammarAccess.getComparisonOperatorAccess().getCompopGreater_thanKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getComparisonOperatorRule());
                      					}
                      					setWithLastConsumed(current, "compop", lv_compop_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:2511:5: lv_compop_0_4= 'less_than_or_equalto'
                    {
                    lv_compop_0_4=(Token)match(input,47,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_compop_0_4, grammarAccess.getComparisonOperatorAccess().getCompopLess_than_or_equaltoKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getComparisonOperatorRule());
                      					}
                      					setWithLastConsumed(current, "compop", lv_compop_0_4, null);
                      				
                    }

                    }
                    break;
                case 5 :
                    // InternalResa.g:2522:5: lv_compop_0_5= 'greater_than_or_equal_to'
                    {
                    lv_compop_0_5=(Token)match(input,48,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_compop_0_5, grammarAccess.getComparisonOperatorAccess().getCompopGreater_than_or_equal_toKeyword_0_4());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getComparisonOperatorRule());
                      					}
                      					setWithLastConsumed(current, "compop", lv_compop_0_5, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonOperator"


    // $ANTLR start "entryRuleTiming"
    // InternalResa.g:2538:1: entryRuleTiming returns [EObject current=null] : iv_ruleTiming= ruleTiming EOF ;
    public final EObject entryRuleTiming() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTiming = null;


        try {
            // InternalResa.g:2538:47: (iv_ruleTiming= ruleTiming EOF )
            // InternalResa.g:2539:2: iv_ruleTiming= ruleTiming EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimingRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTiming=ruleTiming();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTiming; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTiming"


    // $ANTLR start "ruleTiming"
    // InternalResa.g:2545:1: ruleTiming returns [EObject current=null] : ( ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) ) | ( (lv_between_2_0= ruleBetween ) ) | ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' ) ) ;
    public final EObject ruleTiming() throws RecognitionException {
        EObject current = null;

        Token lv_interval_3_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_timing_pp_0_0 = null;

        EObject lv_time_1_0 = null;

        EObject lv_between_2_0 = null;

        EObject lv_t1_4_0 = null;

        EObject lv_t2_6_0 = null;



        	enterRule();

        try {
            // InternalResa.g:2551:2: ( ( ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) ) | ( (lv_between_2_0= ruleBetween ) ) | ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' ) ) )
            // InternalResa.g:2552:2: ( ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) ) | ( (lv_between_2_0= ruleBetween ) ) | ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' ) )
            {
            // InternalResa.g:2552:2: ( ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) ) | ( (lv_between_2_0= ruleBetween ) ) | ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' ) )
            int alt45=3;
            switch ( input.LA(1) ) {
            case 38:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
                {
                alt45=1;
                }
                break;
            case 49:
                {
                alt45=2;
                }
                break;
            case 20:
                {
                alt45=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // InternalResa.g:2553:3: ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) )
                    {
                    // InternalResa.g:2553:3: ( ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) ) )
                    // InternalResa.g:2554:4: ( (lv_timing_pp_0_0= ruleTimingPP ) ) ( (lv_time_1_0= ruleTime ) )
                    {
                    // InternalResa.g:2554:4: ( (lv_timing_pp_0_0= ruleTimingPP ) )
                    // InternalResa.g:2555:5: (lv_timing_pp_0_0= ruleTimingPP )
                    {
                    // InternalResa.g:2555:5: (lv_timing_pp_0_0= ruleTimingPP )
                    // InternalResa.g:2556:6: lv_timing_pp_0_0= ruleTimingPP
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTimingAccess().getTiming_ppTimingPPParserRuleCall_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_38);
                    lv_timing_pp_0_0=ruleTimingPP();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTimingRule());
                      						}
                      						set(
                      							current,
                      							"timing_pp",
                      							lv_timing_pp_0_0,
                      							"org.verispec.resa.Resa.TimingPP");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalResa.g:2573:4: ( (lv_time_1_0= ruleTime ) )
                    // InternalResa.g:2574:5: (lv_time_1_0= ruleTime )
                    {
                    // InternalResa.g:2574:5: (lv_time_1_0= ruleTime )
                    // InternalResa.g:2575:6: lv_time_1_0= ruleTime
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTimingAccess().getTimeTimeParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_time_1_0=ruleTime();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTimingRule());
                      						}
                      						set(
                      							current,
                      							"time",
                      							lv_time_1_0,
                      							"org.verispec.resa.Resa.Time");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:2594:3: ( (lv_between_2_0= ruleBetween ) )
                    {
                    // InternalResa.g:2594:3: ( (lv_between_2_0= ruleBetween ) )
                    // InternalResa.g:2595:4: (lv_between_2_0= ruleBetween )
                    {
                    // InternalResa.g:2595:4: (lv_between_2_0= ruleBetween )
                    // InternalResa.g:2596:5: lv_between_2_0= ruleBetween
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getTimingAccess().getBetweenBetweenParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_between_2_0=ruleBetween();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getTimingRule());
                      					}
                      					set(
                      						current,
                      						"between",
                      						lv_between_2_0,
                      						"org.verispec.resa.Resa.Between");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalResa.g:2614:3: ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' )
                    {
                    // InternalResa.g:2614:3: ( ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']' )
                    // InternalResa.g:2615:4: ( (lv_interval_3_0= '[' ) ) ( (lv_t1_4_0= ruleTime ) ) otherlv_5= ',' ( (lv_t2_6_0= ruleTime ) ) otherlv_7= ']'
                    {
                    // InternalResa.g:2615:4: ( (lv_interval_3_0= '[' ) )
                    // InternalResa.g:2616:5: (lv_interval_3_0= '[' )
                    {
                    // InternalResa.g:2616:5: (lv_interval_3_0= '[' )
                    // InternalResa.g:2617:6: lv_interval_3_0= '['
                    {
                    lv_interval_3_0=(Token)match(input,20,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_interval_3_0, grammarAccess.getTimingAccess().getIntervalLeftSquareBracketKeyword_2_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getTimingRule());
                      						}
                      						setWithLastConsumed(current, "interval", lv_interval_3_0, "[");
                      					
                    }

                    }


                    }

                    // InternalResa.g:2629:4: ( (lv_t1_4_0= ruleTime ) )
                    // InternalResa.g:2630:5: (lv_t1_4_0= ruleTime )
                    {
                    // InternalResa.g:2630:5: (lv_t1_4_0= ruleTime )
                    // InternalResa.g:2631:6: lv_t1_4_0= ruleTime
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTimingAccess().getT1TimeParserRuleCall_2_1_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_t1_4_0=ruleTime();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTimingRule());
                      						}
                      						set(
                      							current,
                      							"t1",
                      							lv_t1_4_0,
                      							"org.verispec.resa.Resa.Time");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_5=(Token)match(input,19,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getTimingAccess().getCommaKeyword_2_2());
                      			
                    }
                    // InternalResa.g:2652:4: ( (lv_t2_6_0= ruleTime ) )
                    // InternalResa.g:2653:5: (lv_t2_6_0= ruleTime )
                    {
                    // InternalResa.g:2653:5: (lv_t2_6_0= ruleTime )
                    // InternalResa.g:2654:6: lv_t2_6_0= ruleTime
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getTimingAccess().getT2TimeParserRuleCall_2_3_0());
                      					
                    }
                    pushFollow(FOLLOW_18);
                    lv_t2_6_0=ruleTime();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getTimingRule());
                      						}
                      						set(
                      							current,
                      							"t2",
                      							lv_t2_6_0,
                      							"org.verispec.resa.Resa.Time");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_7=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_7, grammarAccess.getTimingAccess().getRightSquareBracketKeyword_2_4());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTiming"


    // $ANTLR start "entryRuleBetween"
    // InternalResa.g:2680:1: entryRuleBetween returns [EObject current=null] : iv_ruleBetween= ruleBetween EOF ;
    public final EObject entryRuleBetween() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBetween = null;


        try {
            // InternalResa.g:2680:48: (iv_ruleBetween= ruleBetween EOF )
            // InternalResa.g:2681:2: iv_ruleBetween= ruleBetween EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBetweenRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBetween=ruleBetween();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBetween; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBetween"


    // $ANTLR start "ruleBetween"
    // InternalResa.g:2687:1: ruleBetween returns [EObject current=null] : (otherlv_0= 'between' ( (lv_t1_1_0= ruleTime ) ) otherlv_2= 'and' ( (lv_t2_3_0= ruleTime ) ) ) ;
    public final EObject ruleBetween() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_t1_1_0 = null;

        EObject lv_t2_3_0 = null;



        	enterRule();

        try {
            // InternalResa.g:2693:2: ( (otherlv_0= 'between' ( (lv_t1_1_0= ruleTime ) ) otherlv_2= 'and' ( (lv_t2_3_0= ruleTime ) ) ) )
            // InternalResa.g:2694:2: (otherlv_0= 'between' ( (lv_t1_1_0= ruleTime ) ) otherlv_2= 'and' ( (lv_t2_3_0= ruleTime ) ) )
            {
            // InternalResa.g:2694:2: (otherlv_0= 'between' ( (lv_t1_1_0= ruleTime ) ) otherlv_2= 'and' ( (lv_t2_3_0= ruleTime ) ) )
            // InternalResa.g:2695:3: otherlv_0= 'between' ( (lv_t1_1_0= ruleTime ) ) otherlv_2= 'and' ( (lv_t2_3_0= ruleTime ) )
            {
            otherlv_0=(Token)match(input,49,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getBetweenAccess().getBetweenKeyword_0());
              		
            }
            // InternalResa.g:2699:3: ( (lv_t1_1_0= ruleTime ) )
            // InternalResa.g:2700:4: (lv_t1_1_0= ruleTime )
            {
            // InternalResa.g:2700:4: (lv_t1_1_0= ruleTime )
            // InternalResa.g:2701:5: lv_t1_1_0= ruleTime
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBetweenAccess().getT1TimeParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_39);
            lv_t1_1_0=ruleTime();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBetweenRule());
              					}
              					set(
              						current,
              						"t1",
              						lv_t1_1_0,
              						"org.verispec.resa.Resa.Time");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getBetweenAccess().getAndKeyword_2());
              		
            }
            // InternalResa.g:2722:3: ( (lv_t2_3_0= ruleTime ) )
            // InternalResa.g:2723:4: (lv_t2_3_0= ruleTime )
            {
            // InternalResa.g:2723:4: (lv_t2_3_0= ruleTime )
            // InternalResa.g:2724:5: lv_t2_3_0= ruleTime
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getBetweenAccess().getT2TimeParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_t2_3_0=ruleTime();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getBetweenRule());
              					}
              					set(
              						current,
              						"t2",
              						lv_t2_3_0,
              						"org.verispec.resa.Resa.Time");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBetween"


    // $ANTLR start "entryRuleTimingPP"
    // InternalResa.g:2745:1: entryRuleTimingPP returns [EObject current=null] : iv_ruleTimingPP= ruleTimingPP EOF ;
    public final EObject entryRuleTimingPP() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimingPP = null;


        try {
            // InternalResa.g:2745:49: (iv_ruleTimingPP= ruleTimingPP EOF )
            // InternalResa.g:2746:2: iv_ruleTimingPP= ruleTimingPP EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimingPPRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTimingPP=ruleTimingPP();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTimingPP; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimingPP"


    // $ANTLR start "ruleTimingPP"
    // InternalResa.g:2752:1: ruleTimingPP returns [EObject current=null] : ( ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) ) ) ;
    public final EObject ruleTimingPP() throws RecognitionException {
        EObject current = null;

        Token lv_tprep_0_1=null;
        Token lv_tprep_0_2=null;
        Token lv_tprep_0_3=null;
        Token lv_tprep_0_4=null;
        Token lv_tprep_0_5=null;
        Token lv_tprep_0_6=null;


        	enterRule();

        try {
            // InternalResa.g:2758:2: ( ( ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) ) ) )
            // InternalResa.g:2759:2: ( ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) ) )
            {
            // InternalResa.g:2759:2: ( ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) ) )
            // InternalResa.g:2760:3: ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) )
            {
            // InternalResa.g:2760:3: ( (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' ) )
            // InternalResa.g:2761:4: (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' )
            {
            // InternalResa.g:2761:4: (lv_tprep_0_1= 'within' | lv_tprep_0_2= 'after' | lv_tprep_0_3= 'before' | lv_tprep_0_4= 'at' | lv_tprep_0_5= 'every' | lv_tprep_0_6= 'for' )
            int alt46=6;
            switch ( input.LA(1) ) {
            case 50:
                {
                alt46=1;
                }
                break;
            case 51:
                {
                alt46=2;
                }
                break;
            case 52:
                {
                alt46=3;
                }
                break;
            case 53:
                {
                alt46=4;
                }
                break;
            case 54:
                {
                alt46=5;
                }
                break;
            case 38:
                {
                alt46=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // InternalResa.g:2762:5: lv_tprep_0_1= 'within'
                    {
                    lv_tprep_0_1=(Token)match(input,50,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_1, grammarAccess.getTimingPPAccess().getTprepWithinKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:2773:5: lv_tprep_0_2= 'after'
                    {
                    lv_tprep_0_2=(Token)match(input,51,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_2, grammarAccess.getTimingPPAccess().getTprepAfterKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:2784:5: lv_tprep_0_3= 'before'
                    {
                    lv_tprep_0_3=(Token)match(input,52,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_3, grammarAccess.getTimingPPAccess().getTprepBeforeKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:2795:5: lv_tprep_0_4= 'at'
                    {
                    lv_tprep_0_4=(Token)match(input,53,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_4, grammarAccess.getTimingPPAccess().getTprepAtKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_4, null);
                      				
                    }

                    }
                    break;
                case 5 :
                    // InternalResa.g:2806:5: lv_tprep_0_5= 'every'
                    {
                    lv_tprep_0_5=(Token)match(input,54,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_5, grammarAccess.getTimingPPAccess().getTprepEveryKeyword_0_4());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_5, null);
                      				
                    }

                    }
                    break;
                case 6 :
                    // InternalResa.g:2817:5: lv_tprep_0_6= 'for'
                    {
                    lv_tprep_0_6=(Token)match(input,38,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_tprep_0_6, grammarAccess.getTimingPPAccess().getTprepForKeyword_0_5());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getTimingPPRule());
                      					}
                      					setWithLastConsumed(current, "tprep", lv_tprep_0_6, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimingPP"


    // $ANTLR start "entryRuleTime"
    // InternalResa.g:2833:1: entryRuleTime returns [EObject current=null] : iv_ruleTime= ruleTime EOF ;
    public final EObject entryRuleTime() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTime = null;


        try {
            // InternalResa.g:2833:45: (iv_ruleTime= ruleTime EOF )
            // InternalResa.g:2834:2: iv_ruleTime= ruleTime EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTimeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleTime=ruleTime();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTime; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTime"


    // $ANTLR start "ruleTime"
    // InternalResa.g:2840:1: ruleTime returns [EObject current=null] : ( ( (lv_numeric_0_0= ruleNumeric ) ) ( (lv_unit_1_0= ruleUnit ) ) ) ;
    public final EObject ruleTime() throws RecognitionException {
        EObject current = null;

        EObject lv_numeric_0_0 = null;

        EObject lv_unit_1_0 = null;



        	enterRule();

        try {
            // InternalResa.g:2846:2: ( ( ( (lv_numeric_0_0= ruleNumeric ) ) ( (lv_unit_1_0= ruleUnit ) ) ) )
            // InternalResa.g:2847:2: ( ( (lv_numeric_0_0= ruleNumeric ) ) ( (lv_unit_1_0= ruleUnit ) ) )
            {
            // InternalResa.g:2847:2: ( ( (lv_numeric_0_0= ruleNumeric ) ) ( (lv_unit_1_0= ruleUnit ) ) )
            // InternalResa.g:2848:3: ( (lv_numeric_0_0= ruleNumeric ) ) ( (lv_unit_1_0= ruleUnit ) )
            {
            // InternalResa.g:2848:3: ( (lv_numeric_0_0= ruleNumeric ) )
            // InternalResa.g:2849:4: (lv_numeric_0_0= ruleNumeric )
            {
            // InternalResa.g:2849:4: (lv_numeric_0_0= ruleNumeric )
            // InternalResa.g:2850:5: lv_numeric_0_0= ruleNumeric
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTimeAccess().getNumericNumericParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_40);
            lv_numeric_0_0=ruleNumeric();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTimeRule());
              					}
              					set(
              						current,
              						"numeric",
              						lv_numeric_0_0,
              						"org.verispec.resa.Resa.Numeric");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:2867:3: ( (lv_unit_1_0= ruleUnit ) )
            // InternalResa.g:2868:4: (lv_unit_1_0= ruleUnit )
            {
            // InternalResa.g:2868:4: (lv_unit_1_0= ruleUnit )
            // InternalResa.g:2869:5: lv_unit_1_0= ruleUnit
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getTimeAccess().getUnitUnitParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_unit_1_0=ruleUnit();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getTimeRule());
              					}
              					set(
              						current,
              						"unit",
              						lv_unit_1_0,
              						"org.verispec.resa.Resa.Unit");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTime"


    // $ANTLR start "entryRuleUnit"
    // InternalResa.g:2890:1: entryRuleUnit returns [EObject current=null] : iv_ruleUnit= ruleUnit EOF ;
    public final EObject entryRuleUnit() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnit = null;


        try {
            // InternalResa.g:2890:45: (iv_ruleUnit= ruleUnit EOF )
            // InternalResa.g:2891:2: iv_ruleUnit= ruleUnit EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnitRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUnit=ruleUnit();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnit; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnit"


    // $ANTLR start "ruleUnit"
    // InternalResa.g:2897:1: ruleUnit returns [EObject current=null] : ( ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) ) ) ;
    public final EObject ruleUnit() throws RecognitionException {
        EObject current = null;

        Token lv_timeunit_0_1=null;
        Token lv_timeunit_0_2=null;
        Token lv_timeunit_0_3=null;
        Token lv_timeunit_0_4=null;


        	enterRule();

        try {
            // InternalResa.g:2903:2: ( ( ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) ) ) )
            // InternalResa.g:2904:2: ( ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) ) )
            {
            // InternalResa.g:2904:2: ( ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) ) )
            // InternalResa.g:2905:3: ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) )
            {
            // InternalResa.g:2905:3: ( (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' ) )
            // InternalResa.g:2906:4: (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' )
            {
            // InternalResa.g:2906:4: (lv_timeunit_0_1= 'micsec' | lv_timeunit_0_2= 'msec' | lv_timeunit_0_3= 'sec' | lv_timeunit_0_4= 'min' )
            int alt47=4;
            switch ( input.LA(1) ) {
            case 55:
                {
                alt47=1;
                }
                break;
            case 56:
                {
                alt47=2;
                }
                break;
            case 57:
                {
                alt47=3;
                }
                break;
            case 58:
                {
                alt47=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }

            switch (alt47) {
                case 1 :
                    // InternalResa.g:2907:5: lv_timeunit_0_1= 'micsec'
                    {
                    lv_timeunit_0_1=(Token)match(input,55,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_timeunit_0_1, grammarAccess.getUnitAccess().getTimeunitMicsecKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUnitRule());
                      					}
                      					setWithLastConsumed(current, "timeunit", lv_timeunit_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:2918:5: lv_timeunit_0_2= 'msec'
                    {
                    lv_timeunit_0_2=(Token)match(input,56,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_timeunit_0_2, grammarAccess.getUnitAccess().getTimeunitMsecKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUnitRule());
                      					}
                      					setWithLastConsumed(current, "timeunit", lv_timeunit_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:2929:5: lv_timeunit_0_3= 'sec'
                    {
                    lv_timeunit_0_3=(Token)match(input,57,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_timeunit_0_3, grammarAccess.getUnitAccess().getTimeunitSecKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUnitRule());
                      					}
                      					setWithLastConsumed(current, "timeunit", lv_timeunit_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:2940:5: lv_timeunit_0_4= 'min'
                    {
                    lv_timeunit_0_4=(Token)match(input,58,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_timeunit_0_4, grammarAccess.getUnitAccess().getTimeunitMinKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUnitRule());
                      					}
                      					setWithLastConsumed(current, "timeunit", lv_timeunit_0_4, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnit"


    // $ANTLR start "entryRuleNumeric"
    // InternalResa.g:2956:1: entryRuleNumeric returns [EObject current=null] : iv_ruleNumeric= ruleNumeric EOF ;
    public final EObject entryRuleNumeric() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumeric = null;



        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");

        try {
            // InternalResa.g:2958:2: (iv_ruleNumeric= ruleNumeric EOF )
            // InternalResa.g:2959:2: iv_ruleNumeric= ruleNumeric EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumericRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNumeric=ruleNumeric();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumeric; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleNumeric"


    // $ANTLR start "ruleNumeric"
    // InternalResa.g:2968:1: ruleNumeric returns [EObject current=null] : ( ( (lv_integerpart_0_0= RULE_INT ) ) (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )? ) ;
    public final EObject ruleNumeric() throws RecognitionException {
        EObject current = null;

        Token lv_integerpart_0_0=null;
        Token otherlv_1=null;
        Token lv_decimalpart_2_0=null;


        	enterRule();
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens("RULE_WS");

        try {
            // InternalResa.g:2975:2: ( ( ( (lv_integerpart_0_0= RULE_INT ) ) (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )? ) )
            // InternalResa.g:2976:2: ( ( (lv_integerpart_0_0= RULE_INT ) ) (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )? )
            {
            // InternalResa.g:2976:2: ( ( (lv_integerpart_0_0= RULE_INT ) ) (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )? )
            // InternalResa.g:2977:3: ( (lv_integerpart_0_0= RULE_INT ) ) (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )?
            {
            // InternalResa.g:2977:3: ( (lv_integerpart_0_0= RULE_INT ) )
            // InternalResa.g:2978:4: (lv_integerpart_0_0= RULE_INT )
            {
            // InternalResa.g:2978:4: (lv_integerpart_0_0= RULE_INT )
            // InternalResa.g:2979:5: lv_integerpart_0_0= RULE_INT
            {
            lv_integerpart_0_0=(Token)match(input,RULE_INT,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_integerpart_0_0, grammarAccess.getNumericAccess().getIntegerpartINTTerminalRuleCall_0_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getNumericRule());
              					}
              					setWithLastConsumed(
              						current,
              						"integerpart",
              						lv_integerpart_0_0,
              						"org.eclipse.xtext.common.Terminals.INT");
              				
            }

            }


            }

            // InternalResa.g:2995:3: (otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==11) ) {
                int LA48_1 = input.LA(2);

                if ( (LA48_1==RULE_INT) ) {
                    alt48=1;
                }
            }
            switch (alt48) {
                case 1 :
                    // InternalResa.g:2996:4: otherlv_1= '.' ( (lv_decimalpart_2_0= RULE_INT ) )
                    {
                    otherlv_1=(Token)match(input,11,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getNumericAccess().getFullStopKeyword_1_0());
                      			
                    }
                    // InternalResa.g:3000:4: ( (lv_decimalpart_2_0= RULE_INT ) )
                    // InternalResa.g:3001:5: (lv_decimalpart_2_0= RULE_INT )
                    {
                    // InternalResa.g:3001:5: (lv_decimalpart_2_0= RULE_INT )
                    // InternalResa.g:3002:6: lv_decimalpart_2_0= RULE_INT
                    {
                    lv_decimalpart_2_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_decimalpart_2_0, grammarAccess.getNumericAccess().getDecimalpartINTTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getNumericRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"decimalpart",
                      							lv_decimalpart_2_0,
                      							"org.eclipse.xtext.common.Terminals.INT");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleNumeric"


    // $ANTLR start "entryRuleOccurence"
    // InternalResa.g:3026:1: entryRuleOccurence returns [EObject current=null] : iv_ruleOccurence= ruleOccurence EOF ;
    public final EObject entryRuleOccurence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOccurence = null;


        try {
            // InternalResa.g:3026:50: (iv_ruleOccurence= ruleOccurence EOF )
            // InternalResa.g:3027:2: iv_ruleOccurence= ruleOccurence EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOccurenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOccurence=ruleOccurence();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOccurence; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOccurence"


    // $ANTLR start "ruleOccurence"
    // InternalResa.g:3033:1: ruleOccurence returns [EObject current=null] : ( ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) ) ( (lv_quantity_1_0= ruleQUANTITY ) ) otherlv_2= 'times' ) ;
    public final EObject ruleOccurence() throws RecognitionException {
        EObject current = null;

        Token lv_occurence_pp_0_1=null;
        Token lv_occurence_pp_0_2=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_quantity_1_0 = null;



        	enterRule();

        try {
            // InternalResa.g:3039:2: ( ( ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) ) ( (lv_quantity_1_0= ruleQUANTITY ) ) otherlv_2= 'times' ) )
            // InternalResa.g:3040:2: ( ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) ) ( (lv_quantity_1_0= ruleQUANTITY ) ) otherlv_2= 'times' )
            {
            // InternalResa.g:3040:2: ( ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) ) ( (lv_quantity_1_0= ruleQUANTITY ) ) otherlv_2= 'times' )
            // InternalResa.g:3041:3: ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) ) ( (lv_quantity_1_0= ruleQUANTITY ) ) otherlv_2= 'times'
            {
            // InternalResa.g:3041:3: ( ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) ) )
            // InternalResa.g:3042:4: ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) )
            {
            // InternalResa.g:3042:4: ( (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' ) )
            // InternalResa.g:3043:5: (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' )
            {
            // InternalResa.g:3043:5: (lv_occurence_pp_0_1= 'at_least' | lv_occurence_pp_0_2= 'at_most' )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==59) ) {
                alt49=1;
            }
            else if ( (LA49_0==60) ) {
                alt49=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // InternalResa.g:3044:6: lv_occurence_pp_0_1= 'at_least'
                    {
                    lv_occurence_pp_0_1=(Token)match(input,59,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_occurence_pp_0_1, grammarAccess.getOccurenceAccess().getOccurence_ppAt_leastKeyword_0_0_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOccurenceRule());
                      						}
                      						setWithLastConsumed(current, "occurence_pp", lv_occurence_pp_0_1, null);
                      					
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:3055:6: lv_occurence_pp_0_2= 'at_most'
                    {
                    lv_occurence_pp_0_2=(Token)match(input,60,FOLLOW_38); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_occurence_pp_0_2, grammarAccess.getOccurenceAccess().getOccurence_ppAt_mostKeyword_0_0_1());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOccurenceRule());
                      						}
                      						setWithLastConsumed(current, "occurence_pp", lv_occurence_pp_0_2, null);
                      					
                    }

                    }
                    break;

            }


            }


            }

            // InternalResa.g:3068:3: ( (lv_quantity_1_0= ruleQUANTITY ) )
            // InternalResa.g:3069:4: (lv_quantity_1_0= ruleQUANTITY )
            {
            // InternalResa.g:3069:4: (lv_quantity_1_0= ruleQUANTITY )
            // InternalResa.g:3070:5: lv_quantity_1_0= ruleQUANTITY
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOccurenceAccess().getQuantityQUANTITYParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_41);
            lv_quantity_1_0=ruleQUANTITY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOccurenceRule());
              					}
              					set(
              						current,
              						"quantity",
              						lv_quantity_1_0,
              						"org.verispec.resa.Resa.QUANTITY");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,61,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getOccurenceAccess().getTimesKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOccurence"


    // $ANTLR start "entryRuleQUANTITY"
    // InternalResa.g:3095:1: entryRuleQUANTITY returns [String current=null] : iv_ruleQUANTITY= ruleQUANTITY EOF ;
    public final String entryRuleQUANTITY() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQUANTITY = null;


        try {
            // InternalResa.g:3095:48: (iv_ruleQUANTITY= ruleQUANTITY EOF )
            // InternalResa.g:3096:2: iv_ruleQUANTITY= ruleQUANTITY EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQUANTITYRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQUANTITY=ruleQUANTITY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQUANTITY.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQUANTITY"


    // $ANTLR start "ruleQUANTITY"
    // InternalResa.g:3102:1: ruleQUANTITY returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_INT_0= RULE_INT ;
    public final AntlrDatatypeRuleToken ruleQUANTITY() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;


        	enterRule();

        try {
            // InternalResa.g:3108:2: (this_INT_0= RULE_INT )
            // InternalResa.g:3109:2: this_INT_0= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		current.merge(this_INT_0);
              	
            }
            if ( state.backtracking==0 ) {

              		newLeafNode(this_INT_0, grammarAccess.getQUANTITYAccess().getINTTerminalRuleCall());
              	
            }

            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQUANTITY"


    // $ANTLR start "entryRuleSubordinateClause"
    // InternalResa.g:3119:1: entryRuleSubordinateClause returns [EObject current=null] : iv_ruleSubordinateClause= ruleSubordinateClause EOF ;
    public final EObject entryRuleSubordinateClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubordinateClause = null;


        try {
            // InternalResa.g:3119:58: (iv_ruleSubordinateClause= ruleSubordinateClause EOF )
            // InternalResa.g:3120:2: iv_ruleSubordinateClause= ruleSubordinateClause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubordinateClauseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSubordinateClause=ruleSubordinateClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubordinateClause; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubordinateClause"


    // $ANTLR start "ruleSubordinateClause"
    // InternalResa.g:3126:1: ruleSubordinateClause returns [EObject current=null] : ( ( (lv_subconj_0_0= ruleSubordinateConjunctive ) ) ( (lv_clause_1_0= ruleClause ) ) ) ;
    public final EObject ruleSubordinateClause() throws RecognitionException {
        EObject current = null;

        EObject lv_subconj_0_0 = null;

        EObject lv_clause_1_0 = null;



        	enterRule();

        try {
            // InternalResa.g:3132:2: ( ( ( (lv_subconj_0_0= ruleSubordinateConjunctive ) ) ( (lv_clause_1_0= ruleClause ) ) ) )
            // InternalResa.g:3133:2: ( ( (lv_subconj_0_0= ruleSubordinateConjunctive ) ) ( (lv_clause_1_0= ruleClause ) ) )
            {
            // InternalResa.g:3133:2: ( ( (lv_subconj_0_0= ruleSubordinateConjunctive ) ) ( (lv_clause_1_0= ruleClause ) ) )
            // InternalResa.g:3134:3: ( (lv_subconj_0_0= ruleSubordinateConjunctive ) ) ( (lv_clause_1_0= ruleClause ) )
            {
            // InternalResa.g:3134:3: ( (lv_subconj_0_0= ruleSubordinateConjunctive ) )
            // InternalResa.g:3135:4: (lv_subconj_0_0= ruleSubordinateConjunctive )
            {
            // InternalResa.g:3135:4: (lv_subconj_0_0= ruleSubordinateConjunctive )
            // InternalResa.g:3136:5: lv_subconj_0_0= ruleSubordinateConjunctive
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSubordinateClauseAccess().getSubconjSubordinateConjunctiveParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_7);
            lv_subconj_0_0=ruleSubordinateConjunctive();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSubordinateClauseRule());
              					}
              					set(
              						current,
              						"subconj",
              						lv_subconj_0_0,
              						"org.verispec.resa.Resa.SubordinateConjunctive");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalResa.g:3153:3: ( (lv_clause_1_0= ruleClause ) )
            // InternalResa.g:3154:4: (lv_clause_1_0= ruleClause )
            {
            // InternalResa.g:3154:4: (lv_clause_1_0= ruleClause )
            // InternalResa.g:3155:5: lv_clause_1_0= ruleClause
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getSubordinateClauseAccess().getClauseClauseParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_clause_1_0=ruleClause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getSubordinateClauseRule());
              					}
              					set(
              						current,
              						"clause",
              						lv_clause_1_0,
              						"org.verispec.resa.Resa.Clause");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubordinateClause"


    // $ANTLR start "entryRuleSubordinateConjunctive"
    // InternalResa.g:3176:1: entryRuleSubordinateConjunctive returns [EObject current=null] : iv_ruleSubordinateConjunctive= ruleSubordinateConjunctive EOF ;
    public final EObject entryRuleSubordinateConjunctive() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubordinateConjunctive = null;


        try {
            // InternalResa.g:3176:63: (iv_ruleSubordinateConjunctive= ruleSubordinateConjunctive EOF )
            // InternalResa.g:3177:2: iv_ruleSubordinateConjunctive= ruleSubordinateConjunctive EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubordinateConjunctiveRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSubordinateConjunctive=ruleSubordinateConjunctive();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubordinateConjunctive; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubordinateConjunctive"


    // $ANTLR start "ruleSubordinateConjunctive"
    // InternalResa.g:3183:1: ruleSubordinateConjunctive returns [EObject current=null] : (this_SubConjTemporal_0= ruleSubConjTemporal | this_SubConjConditional_1= ruleSubConjConditional | this_SubConjCause_2= ruleSubConjCause ) ;
    public final EObject ruleSubordinateConjunctive() throws RecognitionException {
        EObject current = null;

        EObject this_SubConjTemporal_0 = null;

        EObject this_SubConjConditional_1 = null;

        EObject this_SubConjCause_2 = null;



        	enterRule();

        try {
            // InternalResa.g:3189:2: ( (this_SubConjTemporal_0= ruleSubConjTemporal | this_SubConjConditional_1= ruleSubConjConditional | this_SubConjCause_2= ruleSubConjCause ) )
            // InternalResa.g:3190:2: (this_SubConjTemporal_0= ruleSubConjTemporal | this_SubConjConditional_1= ruleSubConjConditional | this_SubConjCause_2= ruleSubConjCause )
            {
            // InternalResa.g:3190:2: (this_SubConjTemporal_0= ruleSubConjTemporal | this_SubConjConditional_1= ruleSubConjConditional | this_SubConjCause_2= ruleSubConjCause )
            int alt50=3;
            switch ( input.LA(1) ) {
            case 51:
            case 52:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
                {
                alt50=1;
                }
                break;
            case 14:
            case 66:
            case 67:
            case 68:
                {
                alt50=2;
                }
                break;
            case 62:
            case 63:
            case 64:
            case 65:
                {
                alt50=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // InternalResa.g:3191:3: this_SubConjTemporal_0= ruleSubConjTemporal
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSubordinateConjunctiveAccess().getSubConjTemporalParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SubConjTemporal_0=ruleSubConjTemporal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SubConjTemporal_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:3200:3: this_SubConjConditional_1= ruleSubConjConditional
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSubordinateConjunctiveAccess().getSubConjConditionalParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SubConjConditional_1=ruleSubConjConditional();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SubConjConditional_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3209:3: this_SubConjCause_2= ruleSubConjCause
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSubordinateConjunctiveAccess().getSubConjCauseParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_SubConjCause_2=ruleSubConjCause();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_SubConjCause_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubordinateConjunctive"


    // $ANTLR start "entryRuleSubConjCause"
    // InternalResa.g:3221:1: entryRuleSubConjCause returns [EObject current=null] : iv_ruleSubConjCause= ruleSubConjCause EOF ;
    public final EObject entryRuleSubConjCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubConjCause = null;


        try {
            // InternalResa.g:3221:53: (iv_ruleSubConjCause= ruleSubConjCause EOF )
            // InternalResa.g:3222:2: iv_ruleSubConjCause= ruleSubConjCause EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubConjCauseRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSubConjCause=ruleSubConjCause();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubConjCause; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubConjCause"


    // $ANTLR start "ruleSubConjCause"
    // InternalResa.g:3228:1: ruleSubConjCause returns [EObject current=null] : ( ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) ) ) ;
    public final EObject ruleSubConjCause() throws RecognitionException {
        EObject current = null;

        Token lv_subconj_0_1=null;
        Token lv_subconj_0_2=null;
        Token lv_subconj_0_3=null;
        Token lv_subconj_0_4=null;


        	enterRule();

        try {
            // InternalResa.g:3234:2: ( ( ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) ) ) )
            // InternalResa.g:3235:2: ( ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) ) )
            {
            // InternalResa.g:3235:2: ( ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) ) )
            // InternalResa.g:3236:3: ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) )
            {
            // InternalResa.g:3236:3: ( (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' ) )
            // InternalResa.g:3237:4: (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' )
            {
            // InternalResa.g:3237:4: (lv_subconj_0_1= 'because' | lv_subconj_0_2= 'as' | lv_subconj_0_3= 'in_order_that' | lv_subconj_0_4= 'since' )
            int alt51=4;
            switch ( input.LA(1) ) {
            case 62:
                {
                alt51=1;
                }
                break;
            case 63:
                {
                alt51=2;
                }
                break;
            case 64:
                {
                alt51=3;
                }
                break;
            case 65:
                {
                alt51=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;
            }

            switch (alt51) {
                case 1 :
                    // InternalResa.g:3238:5: lv_subconj_0_1= 'because'
                    {
                    lv_subconj_0_1=(Token)match(input,62,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_1, grammarAccess.getSubConjCauseAccess().getSubconjBecauseKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjCauseRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:3249:5: lv_subconj_0_2= 'as'
                    {
                    lv_subconj_0_2=(Token)match(input,63,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_2, grammarAccess.getSubConjCauseAccess().getSubconjAsKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjCauseRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3260:5: lv_subconj_0_3= 'in_order_that'
                    {
                    lv_subconj_0_3=(Token)match(input,64,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_3, grammarAccess.getSubConjCauseAccess().getSubconjIn_order_thatKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjCauseRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:3271:5: lv_subconj_0_4= 'since'
                    {
                    lv_subconj_0_4=(Token)match(input,65,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_4, grammarAccess.getSubConjCauseAccess().getSubconjSinceKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjCauseRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_4, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubConjCause"


    // $ANTLR start "entryRuleSubConjConditional"
    // InternalResa.g:3287:1: entryRuleSubConjConditional returns [EObject current=null] : iv_ruleSubConjConditional= ruleSubConjConditional EOF ;
    public final EObject entryRuleSubConjConditional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubConjConditional = null;


        try {
            // InternalResa.g:3287:59: (iv_ruleSubConjConditional= ruleSubConjConditional EOF )
            // InternalResa.g:3288:2: iv_ruleSubConjConditional= ruleSubConjConditional EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubConjConditionalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSubConjConditional=ruleSubConjConditional();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubConjConditional; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubConjConditional"


    // $ANTLR start "ruleSubConjConditional"
    // InternalResa.g:3294:1: ruleSubConjConditional returns [EObject current=null] : ( ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) ) ) ;
    public final EObject ruleSubConjConditional() throws RecognitionException {
        EObject current = null;

        Token lv_subconj_0_1=null;
        Token lv_subconj_0_2=null;
        Token lv_subconj_0_3=null;
        Token lv_subconj_0_4=null;


        	enterRule();

        try {
            // InternalResa.g:3300:2: ( ( ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) ) ) )
            // InternalResa.g:3301:2: ( ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) ) )
            {
            // InternalResa.g:3301:2: ( ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) ) )
            // InternalResa.g:3302:3: ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) )
            {
            // InternalResa.g:3302:3: ( (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' ) )
            // InternalResa.g:3303:4: (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' )
            {
            // InternalResa.g:3303:4: (lv_subconj_0_1= 'if' | lv_subconj_0_2= 'in_case' | lv_subconj_0_3= 'provided_that' | lv_subconj_0_4= 'unless' )
            int alt52=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt52=1;
                }
                break;
            case 66:
                {
                alt52=2;
                }
                break;
            case 67:
                {
                alt52=3;
                }
                break;
            case 68:
                {
                alt52=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }

            switch (alt52) {
                case 1 :
                    // InternalResa.g:3304:5: lv_subconj_0_1= 'if'
                    {
                    lv_subconj_0_1=(Token)match(input,14,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_1, grammarAccess.getSubConjConditionalAccess().getSubconjIfKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjConditionalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:3315:5: lv_subconj_0_2= 'in_case'
                    {
                    lv_subconj_0_2=(Token)match(input,66,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_2, grammarAccess.getSubConjConditionalAccess().getSubconjIn_caseKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjConditionalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3326:5: lv_subconj_0_3= 'provided_that'
                    {
                    lv_subconj_0_3=(Token)match(input,67,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_3, grammarAccess.getSubConjConditionalAccess().getSubconjProvided_thatKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjConditionalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:3337:5: lv_subconj_0_4= 'unless'
                    {
                    lv_subconj_0_4=(Token)match(input,68,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_4, grammarAccess.getSubConjConditionalAccess().getSubconjUnlessKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjConditionalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_4, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubConjConditional"


    // $ANTLR start "entryRuleSubConjTemporal"
    // InternalResa.g:3353:1: entryRuleSubConjTemporal returns [EObject current=null] : iv_ruleSubConjTemporal= ruleSubConjTemporal EOF ;
    public final EObject entryRuleSubConjTemporal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubConjTemporal = null;


        try {
            // InternalResa.g:3353:56: (iv_ruleSubConjTemporal= ruleSubConjTemporal EOF )
            // InternalResa.g:3354:2: iv_ruleSubConjTemporal= ruleSubConjTemporal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubConjTemporalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSubConjTemporal=ruleSubConjTemporal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubConjTemporal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubConjTemporal"


    // $ANTLR start "ruleSubConjTemporal"
    // InternalResa.g:3360:1: ruleSubConjTemporal returns [EObject current=null] : ( ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) ) ) ;
    public final EObject ruleSubConjTemporal() throws RecognitionException {
        EObject current = null;

        Token lv_subconj_0_1=null;
        Token lv_subconj_0_2=null;
        Token lv_subconj_0_3=null;
        Token lv_subconj_0_4=null;
        Token lv_subconj_0_5=null;
        Token lv_subconj_0_6=null;
        Token lv_subconj_0_7=null;
        Token lv_subconj_0_8=null;
        Token lv_subconj_0_9=null;


        	enterRule();

        try {
            // InternalResa.g:3366:2: ( ( ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) ) ) )
            // InternalResa.g:3367:2: ( ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) ) )
            {
            // InternalResa.g:3367:2: ( ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) ) )
            // InternalResa.g:3368:3: ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) )
            {
            // InternalResa.g:3368:3: ( (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' ) )
            // InternalResa.g:3369:4: (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' )
            {
            // InternalResa.g:3369:4: (lv_subconj_0_1= 'as_soon_as' | lv_subconj_0_2= 'as_long_as' | lv_subconj_0_3= 'before' | lv_subconj_0_4= 'once' | lv_subconj_0_5= 'till' | lv_subconj_0_6= 'until' | lv_subconj_0_7= 'when' | lv_subconj_0_8= 'while' | lv_subconj_0_9= 'after' )
            int alt53=9;
            switch ( input.LA(1) ) {
            case 69:
                {
                alt53=1;
                }
                break;
            case 70:
                {
                alt53=2;
                }
                break;
            case 52:
                {
                alt53=3;
                }
                break;
            case 71:
                {
                alt53=4;
                }
                break;
            case 72:
                {
                alt53=5;
                }
                break;
            case 73:
                {
                alt53=6;
                }
                break;
            case 74:
                {
                alt53=7;
                }
                break;
            case 75:
                {
                alt53=8;
                }
                break;
            case 51:
                {
                alt53=9;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 53, 0, input);

                throw nvae;
            }

            switch (alt53) {
                case 1 :
                    // InternalResa.g:3370:5: lv_subconj_0_1= 'as_soon_as'
                    {
                    lv_subconj_0_1=(Token)match(input,69,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_1, grammarAccess.getSubConjTemporalAccess().getSubconjAs_soon_asKeyword_0_0());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_1, null);
                      				
                    }

                    }
                    break;
                case 2 :
                    // InternalResa.g:3381:5: lv_subconj_0_2= 'as_long_as'
                    {
                    lv_subconj_0_2=(Token)match(input,70,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_2, grammarAccess.getSubConjTemporalAccess().getSubconjAs_long_asKeyword_0_1());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_2, null);
                      				
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3392:5: lv_subconj_0_3= 'before'
                    {
                    lv_subconj_0_3=(Token)match(input,52,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_3, grammarAccess.getSubConjTemporalAccess().getSubconjBeforeKeyword_0_2());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_3, null);
                      				
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:3403:5: lv_subconj_0_4= 'once'
                    {
                    lv_subconj_0_4=(Token)match(input,71,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_4, grammarAccess.getSubConjTemporalAccess().getSubconjOnceKeyword_0_3());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_4, null);
                      				
                    }

                    }
                    break;
                case 5 :
                    // InternalResa.g:3414:5: lv_subconj_0_5= 'till'
                    {
                    lv_subconj_0_5=(Token)match(input,72,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_5, grammarAccess.getSubConjTemporalAccess().getSubconjTillKeyword_0_4());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_5, null);
                      				
                    }

                    }
                    break;
                case 6 :
                    // InternalResa.g:3425:5: lv_subconj_0_6= 'until'
                    {
                    lv_subconj_0_6=(Token)match(input,73,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_6, grammarAccess.getSubConjTemporalAccess().getSubconjUntilKeyword_0_5());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_6, null);
                      				
                    }

                    }
                    break;
                case 7 :
                    // InternalResa.g:3436:5: lv_subconj_0_7= 'when'
                    {
                    lv_subconj_0_7=(Token)match(input,74,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_7, grammarAccess.getSubConjTemporalAccess().getSubconjWhenKeyword_0_6());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_7, null);
                      				
                    }

                    }
                    break;
                case 8 :
                    // InternalResa.g:3447:5: lv_subconj_0_8= 'while'
                    {
                    lv_subconj_0_8=(Token)match(input,75,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_8, grammarAccess.getSubConjTemporalAccess().getSubconjWhileKeyword_0_7());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_8, null);
                      				
                    }

                    }
                    break;
                case 9 :
                    // InternalResa.g:3458:5: lv_subconj_0_9= 'after'
                    {
                    lv_subconj_0_9=(Token)match(input,51,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(lv_subconj_0_9, grammarAccess.getSubConjTemporalAccess().getSubconjAfterKeyword_0_8());
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getSubConjTemporalRule());
                      					}
                      					setWithLastConsumed(current, "subconj", lv_subconj_0_9, null);
                      				
                    }

                    }
                    break;

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubConjTemporal"


    // $ANTLR start "entryRuleSystem"
    // InternalResa.g:3474:1: entryRuleSystem returns [EObject current=null] : iv_ruleSystem= ruleSystem EOF ;
    public final EObject entryRuleSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSystem = null;


        try {
            // InternalResa.g:3474:47: (iv_ruleSystem= ruleSystem EOF )
            // InternalResa.g:3475:2: iv_ruleSystem= ruleSystem EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSystemRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSystem=ruleSystem();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSystem; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // InternalResa.g:3481:1: ruleSystem returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':system' ) ;
    public final EObject ruleSystem() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3487:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':system' ) )
            // InternalResa.g:3488:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':system' )
            {
            // InternalResa.g:3488:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':system' )
            // InternalResa.g:3489:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':system'
            {
            // InternalResa.g:3489:3: ()
            // InternalResa.g:3490:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getSystemAccess().getDeviceAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3496:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:3497:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:3497:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:3498:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_42); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getSystemAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getSystemRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,76,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getSystemAccess().getSystemKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRuleDevice"
    // InternalResa.g:3522:1: entryRuleDevice returns [EObject current=null] : iv_ruleDevice= ruleDevice EOF ;
    public final EObject entryRuleDevice() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDevice = null;


        try {
            // InternalResa.g:3522:47: (iv_ruleDevice= ruleDevice EOF )
            // InternalResa.g:3523:2: iv_ruleDevice= ruleDevice EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeviceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDevice=ruleDevice();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDevice; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDevice"


    // $ANTLR start "ruleDevice"
    // InternalResa.g:3529:1: ruleDevice returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' ) | this_InDevice_3= ruleInDevice | this_OutDevice_4= ruleOutDevice ) ;
    public final EObject ruleDevice() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject this_InDevice_3 = null;

        EObject this_OutDevice_4 = null;



        	enterRule();

        try {
            // InternalResa.g:3535:2: ( ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' ) | this_InDevice_3= ruleInDevice | this_OutDevice_4= ruleOutDevice ) )
            // InternalResa.g:3536:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' ) | this_InDevice_3= ruleInDevice | this_OutDevice_4= ruleOutDevice )
            {
            // InternalResa.g:3536:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' ) | this_InDevice_3= ruleInDevice | this_OutDevice_4= ruleOutDevice )
            int alt54=3;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==RULE_STRING) ) {
                switch ( input.LA(2) ) {
                case 90:
                    {
                    alt54=2;
                    }
                    break;
                case 91:
                    {
                    alt54=3;
                    }
                    break;
                case 77:
                    {
                    alt54=1;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 54, 1, input);

                    throw nvae;
                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // InternalResa.g:3537:3: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' )
                    {
                    // InternalResa.g:3537:3: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device' )
                    // InternalResa.g:3538:4: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':device'
                    {
                    // InternalResa.g:3538:4: ()
                    // InternalResa.g:3539:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getDeviceAccess().getDeviceAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalResa.g:3545:4: ( (lv_name_1_0= RULE_STRING ) )
                    // InternalResa.g:3546:5: (lv_name_1_0= RULE_STRING )
                    {
                    // InternalResa.g:3546:5: (lv_name_1_0= RULE_STRING )
                    // InternalResa.g:3547:6: lv_name_1_0= RULE_STRING
                    {
                    lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_43); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_0, grammarAccess.getDeviceAccess().getNameSTRINGTerminalRuleCall_0_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getDeviceRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_1_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,77,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getDeviceAccess().getDeviceKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:3569:3: this_InDevice_3= ruleInDevice
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDeviceAccess().getInDeviceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InDevice_3=ruleInDevice();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InDevice_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3578:3: this_OutDevice_4= ruleOutDevice
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDeviceAccess().getOutDeviceParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_OutDevice_4=ruleOutDevice();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_OutDevice_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDevice"


    // $ANTLR start "entryRuleParameter"
    // InternalResa.g:3590:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalResa.g:3590:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalResa.g:3591:2: iv_ruleParameter= ruleParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParameterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParameter; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalResa.g:3597:1: ruleParameter returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' ) | this_InParameter_3= ruleInParameter | this_OutParameter_4= ruleOutParameter | this_Signal_5= ruleSignal ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject this_InParameter_3 = null;

        EObject this_OutParameter_4 = null;

        EObject this_Signal_5 = null;



        	enterRule();

        try {
            // InternalResa.g:3603:2: ( ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' ) | this_InParameter_3= ruleInParameter | this_OutParameter_4= ruleOutParameter | this_Signal_5= ruleSignal ) )
            // InternalResa.g:3604:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' ) | this_InParameter_3= ruleInParameter | this_OutParameter_4= ruleOutParameter | this_Signal_5= ruleSignal )
            {
            // InternalResa.g:3604:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' ) | this_InParameter_3= ruleInParameter | this_OutParameter_4= ruleOutParameter | this_Signal_5= ruleSignal )
            int alt55=4;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==RULE_STRING) ) {
                switch ( input.LA(2) ) {
                case 78:
                    {
                    alt55=1;
                    }
                    break;
                case 88:
                    {
                    alt55=2;
                    }
                    break;
                case 89:
                    {
                    alt55=3;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 55, 1, input);

                    throw nvae;
                }

            }
            else if ( (LA55_0==RULE_ID) ) {
                alt55=4;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // InternalResa.g:3605:3: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' )
                    {
                    // InternalResa.g:3605:3: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter' )
                    // InternalResa.g:3606:4: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':parameter'
                    {
                    // InternalResa.g:3606:4: ()
                    // InternalResa.g:3607:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getParameterAccess().getParameterAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalResa.g:3613:4: ( (lv_name_1_0= RULE_STRING ) )
                    // InternalResa.g:3614:5: (lv_name_1_0= RULE_STRING )
                    {
                    // InternalResa.g:3614:5: (lv_name_1_0= RULE_STRING )
                    // InternalResa.g:3615:6: lv_name_1_0= RULE_STRING
                    {
                    lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameSTRINGTerminalRuleCall_0_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getParameterRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_1_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,78,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getParameterAccess().getParameterKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:3637:3: this_InParameter_3= ruleInParameter
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getParameterAccess().getInParameterParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InParameter_3=ruleInParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InParameter_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3646:3: this_OutParameter_4= ruleOutParameter
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getParameterAccess().getOutParameterParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_OutParameter_4=ruleOutParameter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_OutParameter_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalResa.g:3655:3: this_Signal_5= ruleSignal
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getParameterAccess().getSignalParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Signal_5=ruleSignal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Signal_5;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleSignal"
    // InternalResa.g:3667:1: entryRuleSignal returns [EObject current=null] : iv_ruleSignal= ruleSignal EOF ;
    public final EObject entryRuleSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSignal = null;


        try {
            // InternalResa.g:3667:47: (iv_ruleSignal= ruleSignal EOF )
            // InternalResa.g:3668:2: iv_ruleSignal= ruleSignal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSignalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleSignal=ruleSignal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSignal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSignal"


    // $ANTLR start "ruleSignal"
    // InternalResa.g:3674:1: ruleSignal returns [EObject current=null] : ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' ) | this_InSignal_3= ruleInSignal | this_OutSignal_4= ruleOutSignal ) ;
    public final EObject ruleSignal() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject this_InSignal_3 = null;

        EObject this_OutSignal_4 = null;



        	enterRule();

        try {
            // InternalResa.g:3680:2: ( ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' ) | this_InSignal_3= ruleInSignal | this_OutSignal_4= ruleOutSignal ) )
            // InternalResa.g:3681:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' ) | this_InSignal_3= ruleInSignal | this_OutSignal_4= ruleOutSignal )
            {
            // InternalResa.g:3681:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' ) | this_InSignal_3= ruleInSignal | this_OutSignal_4= ruleOutSignal )
            int alt56=3;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 79:
                    {
                    alt56=1;
                    }
                    break;
                case 80:
                    {
                    alt56=3;
                    }
                    break;
                case 81:
                    {
                    alt56=2;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 56, 1, input);

                    throw nvae;
                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // InternalResa.g:3682:3: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' )
                    {
                    // InternalResa.g:3682:3: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal' )
                    // InternalResa.g:3683:4: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':signal'
                    {
                    // InternalResa.g:3683:4: ()
                    // InternalResa.g:3684:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getSignalAccess().getSignalAction_0_0(),
                      						current);
                      				
                    }

                    }

                    // InternalResa.g:3690:4: ( (lv_name_1_0= RULE_ID ) )
                    // InternalResa.g:3691:5: (lv_name_1_0= RULE_ID )
                    {
                    // InternalResa.g:3691:5: (lv_name_1_0= RULE_ID )
                    // InternalResa.g:3692:6: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_45); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_name_1_0, grammarAccess.getSignalAccess().getNameIDTerminalRuleCall_0_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getSignalRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"name",
                      							lv_name_1_0,
                      							"org.eclipse.xtext.common.Terminals.ID");
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,79,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getSignalAccess().getSignalKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalResa.g:3714:3: this_InSignal_3= ruleInSignal
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSignalAccess().getInSignalParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_InSignal_3=ruleInSignal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_InSignal_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalResa.g:3723:3: this_OutSignal_4= ruleOutSignal
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getSignalAccess().getOutSignalParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_OutSignal_4=ruleOutSignal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_OutSignal_4;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSignal"


    // $ANTLR start "entryRuleOutSignal"
    // InternalResa.g:3735:1: entryRuleOutSignal returns [EObject current=null] : iv_ruleOutSignal= ruleOutSignal EOF ;
    public final EObject entryRuleOutSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutSignal = null;


        try {
            // InternalResa.g:3735:50: (iv_ruleOutSignal= ruleOutSignal EOF )
            // InternalResa.g:3736:2: iv_ruleOutSignal= ruleOutSignal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutSignalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutSignal=ruleOutSignal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutSignal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutSignal"


    // $ANTLR start "ruleOutSignal"
    // InternalResa.g:3742:1: ruleOutSignal returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':outsignal' ) ;
    public final EObject ruleOutSignal() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3748:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':outsignal' ) )
            // InternalResa.g:3749:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':outsignal' )
            {
            // InternalResa.g:3749:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':outsignal' )
            // InternalResa.g:3750:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':outsignal'
            {
            // InternalResa.g:3750:3: ()
            // InternalResa.g:3751:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getOutSignalAccess().getOutSignalAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3757:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalResa.g:3758:4: (lv_name_1_0= RULE_ID )
            {
            // InternalResa.g:3758:4: (lv_name_1_0= RULE_ID )
            // InternalResa.g:3759:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getOutSignalAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getOutSignalRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,80,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getOutSignalAccess().getOutsignalKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutSignal"


    // $ANTLR start "entryRuleInSignal"
    // InternalResa.g:3783:1: entryRuleInSignal returns [EObject current=null] : iv_ruleInSignal= ruleInSignal EOF ;
    public final EObject entryRuleInSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInSignal = null;


        try {
            // InternalResa.g:3783:49: (iv_ruleInSignal= ruleInSignal EOF )
            // InternalResa.g:3784:2: iv_ruleInSignal= ruleInSignal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInSignalRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInSignal=ruleInSignal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInSignal; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInSignal"


    // $ANTLR start "ruleInSignal"
    // InternalResa.g:3790:1: ruleInSignal returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':insignal' ) ;
    public final EObject ruleInSignal() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3796:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':insignal' ) )
            // InternalResa.g:3797:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':insignal' )
            {
            // InternalResa.g:3797:2: ( () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':insignal' )
            // InternalResa.g:3798:3: () ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':insignal'
            {
            // InternalResa.g:3798:3: ()
            // InternalResa.g:3799:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getInSignalAccess().getInSignalAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3805:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalResa.g:3806:4: (lv_name_1_0= RULE_ID )
            {
            // InternalResa.g:3806:4: (lv_name_1_0= RULE_ID )
            // InternalResa.g:3807:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_47); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getInSignalAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getInSignalRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.ID");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,81,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInSignalAccess().getInsignalKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInSignal"


    // $ANTLR start "entryRuleState"
    // InternalResa.g:3831:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalResa.g:3831:46: (iv_ruleState= ruleState EOF )
            // InternalResa.g:3832:2: iv_ruleState= ruleState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStateRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleState; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalResa.g:3838:1: ruleState returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':state' ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3844:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':state' ) )
            // InternalResa.g:3845:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':state' )
            {
            // InternalResa.g:3845:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':state' )
            // InternalResa.g:3846:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':state'
            {
            // InternalResa.g:3846:3: ()
            // InternalResa.g:3847:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getStateAccess().getStateAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3853:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:3854:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:3854:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:3855:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_48); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getStateAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getStateRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,82,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getStateAccess().getStateKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleMode"
    // InternalResa.g:3879:1: entryRuleMode returns [EObject current=null] : iv_ruleMode= ruleMode EOF ;
    public final EObject entryRuleMode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMode = null;


        try {
            // InternalResa.g:3879:45: (iv_ruleMode= ruleMode EOF )
            // InternalResa.g:3880:2: iv_ruleMode= ruleMode EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMode=ruleMode();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMode; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMode"


    // $ANTLR start "ruleMode"
    // InternalResa.g:3886:1: ruleMode returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':mode' ) ;
    public final EObject ruleMode() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3892:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':mode' ) )
            // InternalResa.g:3893:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':mode' )
            {
            // InternalResa.g:3893:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':mode' )
            // InternalResa.g:3894:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':mode'
            {
            // InternalResa.g:3894:3: ()
            // InternalResa.g:3895:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getModeAccess().getModeAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3901:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:3902:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:3902:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:3903:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_49); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getModeAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getModeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,83,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getModeAccess().getModeKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMode"


    // $ANTLR start "entryRuleEvent"
    // InternalResa.g:3927:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalResa.g:3927:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalResa.g:3928:2: iv_ruleEvent= ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvent; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalResa.g:3934:1: ruleEvent returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':event' ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3940:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':event' ) )
            // InternalResa.g:3941:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':event' )
            {
            // InternalResa.g:3941:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':event' )
            // InternalResa.g:3942:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':event'
            {
            // InternalResa.g:3942:3: ()
            // InternalResa.g:3943:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getEventAccess().getEventAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3949:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:3950:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:3950:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:3951:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_50); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getEventAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getEventRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,84,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getEventAccess().getEventKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleStatus"
    // InternalResa.g:3975:1: entryRuleStatus returns [EObject current=null] : iv_ruleStatus= ruleStatus EOF ;
    public final EObject entryRuleStatus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatus = null;


        try {
            // InternalResa.g:3975:47: (iv_ruleStatus= ruleStatus EOF )
            // InternalResa.g:3976:2: iv_ruleStatus= ruleStatus EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatusRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStatus=ruleStatus();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatus; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatus"


    // $ANTLR start "ruleStatus"
    // InternalResa.g:3982:1: ruleStatus returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':status' ) ;
    public final EObject ruleStatus() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:3988:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':status' ) )
            // InternalResa.g:3989:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':status' )
            {
            // InternalResa.g:3989:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':status' )
            // InternalResa.g:3990:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':status'
            {
            // InternalResa.g:3990:3: ()
            // InternalResa.g:3991:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getStatusAccess().getStatusAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:3997:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:3998:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:3998:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:3999:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_51); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getStatusAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getStatusRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,85,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getStatusAccess().getStatusKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatus"


    // $ANTLR start "entryRuleUser"
    // InternalResa.g:4023:1: entryRuleUser returns [EObject current=null] : iv_ruleUser= ruleUser EOF ;
    public final EObject entryRuleUser() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUser = null;


        try {
            // InternalResa.g:4023:45: (iv_ruleUser= ruleUser EOF )
            // InternalResa.g:4024:2: iv_ruleUser= ruleUser EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUserRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUser=ruleUser();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUser; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUser"


    // $ANTLR start "ruleUser"
    // InternalResa.g:4030:1: ruleUser returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':user' ) ;
    public final EObject ruleUser() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4036:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':user' ) )
            // InternalResa.g:4037:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':user' )
            {
            // InternalResa.g:4037:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':user' )
            // InternalResa.g:4038:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':user'
            {
            // InternalResa.g:4038:3: ()
            // InternalResa.g:4039:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getUserAccess().getUserAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4045:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4046:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4046:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4047:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_52); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getUserAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getUserRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,86,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getUserAccess().getUserKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUser"


    // $ANTLR start "entryRuleEntity"
    // InternalResa.g:4071:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalResa.g:4071:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalResa.g:4072:2: iv_ruleEntity= ruleEntity EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEntityRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEntity; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalResa.g:4078:1: ruleEntity returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':entity' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4084:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':entity' ) )
            // InternalResa.g:4085:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':entity' )
            {
            // InternalResa.g:4085:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':entity' )
            // InternalResa.g:4086:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':entity'
            {
            // InternalResa.g:4086:3: ()
            // InternalResa.g:4087:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getEntityAccess().getEntityAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4093:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4094:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4094:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4095:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_53); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getEntityAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getEntityRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,87,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getEntityAccess().getEntityKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleInParameter"
    // InternalResa.g:4119:1: entryRuleInParameter returns [EObject current=null] : iv_ruleInParameter= ruleInParameter EOF ;
    public final EObject entryRuleInParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInParameter = null;


        try {
            // InternalResa.g:4119:52: (iv_ruleInParameter= ruleInParameter EOF )
            // InternalResa.g:4120:2: iv_ruleInParameter= ruleInParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInParameterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInParameter=ruleInParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInParameter; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInParameter"


    // $ANTLR start "ruleInParameter"
    // InternalResa.g:4126:1: ruleInParameter returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':inparameter' ) ;
    public final EObject ruleInParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4132:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':inparameter' ) )
            // InternalResa.g:4133:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':inparameter' )
            {
            // InternalResa.g:4133:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':inparameter' )
            // InternalResa.g:4134:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':inparameter'
            {
            // InternalResa.g:4134:3: ()
            // InternalResa.g:4135:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getInParameterAccess().getInParameterAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4141:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4142:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4142:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4143:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getInParameterAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getInParameterRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,88,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInParameterAccess().getInparameterKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInParameter"


    // $ANTLR start "entryRuleOutParameter"
    // InternalResa.g:4167:1: entryRuleOutParameter returns [EObject current=null] : iv_ruleOutParameter= ruleOutParameter EOF ;
    public final EObject entryRuleOutParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutParameter = null;


        try {
            // InternalResa.g:4167:53: (iv_ruleOutParameter= ruleOutParameter EOF )
            // InternalResa.g:4168:2: iv_ruleOutParameter= ruleOutParameter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutParameterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutParameter=ruleOutParameter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutParameter; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutParameter"


    // $ANTLR start "ruleOutParameter"
    // InternalResa.g:4174:1: ruleOutParameter returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outparameter' ) ;
    public final EObject ruleOutParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4180:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outparameter' ) )
            // InternalResa.g:4181:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outparameter' )
            {
            // InternalResa.g:4181:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outparameter' )
            // InternalResa.g:4182:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outparameter'
            {
            // InternalResa.g:4182:3: ()
            // InternalResa.g:4183:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getOutParameterAccess().getOutParameterAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4189:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4190:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4190:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4191:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_55); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getOutParameterAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getOutParameterRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,89,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getOutParameterAccess().getOutparameterKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutParameter"


    // $ANTLR start "entryRuleInDevice"
    // InternalResa.g:4215:1: entryRuleInDevice returns [EObject current=null] : iv_ruleInDevice= ruleInDevice EOF ;
    public final EObject entryRuleInDevice() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInDevice = null;


        try {
            // InternalResa.g:4215:49: (iv_ruleInDevice= ruleInDevice EOF )
            // InternalResa.g:4216:2: iv_ruleInDevice= ruleInDevice EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInDeviceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInDevice=ruleInDevice();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInDevice; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInDevice"


    // $ANTLR start "ruleInDevice"
    // InternalResa.g:4222:1: ruleInDevice returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':indevice' ) ;
    public final EObject ruleInDevice() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4228:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':indevice' ) )
            // InternalResa.g:4229:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':indevice' )
            {
            // InternalResa.g:4229:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':indevice' )
            // InternalResa.g:4230:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':indevice'
            {
            // InternalResa.g:4230:3: ()
            // InternalResa.g:4231:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getInDeviceAccess().getInDeviceAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4237:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4238:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4238:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4239:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_56); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getInDeviceAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getInDeviceRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,90,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getInDeviceAccess().getIndeviceKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInDevice"


    // $ANTLR start "entryRuleOutDevice"
    // InternalResa.g:4263:1: entryRuleOutDevice returns [EObject current=null] : iv_ruleOutDevice= ruleOutDevice EOF ;
    public final EObject entryRuleOutDevice() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutDevice = null;


        try {
            // InternalResa.g:4263:50: (iv_ruleOutDevice= ruleOutDevice EOF )
            // InternalResa.g:4264:2: iv_ruleOutDevice= ruleOutDevice EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutDeviceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutDevice=ruleOutDevice();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutDevice; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutDevice"


    // $ANTLR start "ruleOutDevice"
    // InternalResa.g:4270:1: ruleOutDevice returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outdevice' ) ;
    public final EObject ruleOutDevice() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4276:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outdevice' ) )
            // InternalResa.g:4277:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outdevice' )
            {
            // InternalResa.g:4277:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outdevice' )
            // InternalResa.g:4278:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':outdevice'
            {
            // InternalResa.g:4278:3: ()
            // InternalResa.g:4279:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getOutDeviceAccess().getOutDeviceAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4285:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4286:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4286:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4287:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_57); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getOutDeviceAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getOutDeviceRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,91,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getOutDeviceAccess().getOutdeviceKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutDevice"


    // $ANTLR start "entryRuleAttribute"
    // InternalResa.g:4311:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalResa.g:4311:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalResa.g:4312:2: iv_ruleAttribute= ruleAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalResa.g:4318:1: ruleAttribute returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':attribute' ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalResa.g:4324:2: ( ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':attribute' ) )
            // InternalResa.g:4325:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':attribute' )
            {
            // InternalResa.g:4325:2: ( () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':attribute' )
            // InternalResa.g:4326:3: () ( (lv_name_1_0= RULE_STRING ) ) otherlv_2= ':attribute'
            {
            // InternalResa.g:4326:3: ()
            // InternalResa.g:4327:4: 
            {
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getAttributeAccess().getAttributeAction_0(),
              					current);
              			
            }

            }

            // InternalResa.g:4333:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalResa.g:4334:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalResa.g:4334:4: (lv_name_1_0= RULE_STRING )
            // InternalResa.g:4335:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_58); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getAttributeAccess().getNameSTRINGTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAttributeRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.common.Terminals.STRING");
              				
            }

            }


            }

            otherlv_2=(Token)match(input,92,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getAttributeAccess().getAttributeKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"

    // $ANTLR start synpred1_InternalResa
    public final void synpred1_InternalResa_fragment() throws RecognitionException {   
        // InternalResa.g:1740:3: ( ( ( RULE_ID | RULE_ANY_OTHER ) ) )
        // InternalResa.g:1740:4: ( ( RULE_ID | RULE_ANY_OTHER ) )
        {
        // InternalResa.g:1740:4: ( ( RULE_ID | RULE_ANY_OTHER ) )
        // InternalResa.g:1741:4: ( RULE_ID | RULE_ANY_OTHER )
        {
        if ( (input.LA(1)>=RULE_ID && input.LA(1)<=RULE_ANY_OTHER) ) {
            input.consume();
            state.errorRecovery=false;state.failed=false;
        }
        else {
            if (state.backtracking>0) {state.failed=true; return ;}
            MismatchedSetException mse = new MismatchedSetException(null,input);
            throw mse;
        }


        }


        }
    }
    // $ANTLR end synpred1_InternalResa

    // $ANTLR start synpred2_InternalResa
    public final void synpred2_InternalResa_fragment() throws RecognitionException {   
        // InternalResa.g:2071:4: ( ( ruleText ) )
        // InternalResa.g:2071:5: ( ruleText )
        {
        // InternalResa.g:2071:5: ( ruleText )
        // InternalResa.g:2072:5: ruleText
        {
        pushFollow(FOLLOW_2);
        ruleText();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred2_InternalResa

    // $ANTLR start synpred3_InternalResa
    public final void synpred3_InternalResa_fragment() throws RecognitionException {   
        // InternalResa.g:2234:4: ( ruleVariable )
        // InternalResa.g:2234:5: ruleVariable
        {
        pushFollow(FOLLOW_2);
        ruleVariable();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred3_InternalResa

    // $ANTLR start synpred4_InternalResa
    public final void synpred4_InternalResa_fragment() throws RecognitionException {   
        // InternalResa.g:2246:4: ( ruleLiteral )
        // InternalResa.g:2246:5: ruleLiteral
        {
        pushFollow(FOLLOW_2);
        ruleLiteral();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred4_InternalResa

    // Delegated rules

    public final boolean synpred4_InternalResa() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_InternalResa_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred1_InternalResa() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalResa_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_InternalResa() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_InternalResa_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalResa() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalResa_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA14 dfa14 = new DFA14(this);
    protected DFA42 dfa42 = new DFA42(this);
    static final String dfa_1s = "\u021f\uffff";
    static final String dfa_2s = "\2\4\1\uffff\6\4\1\uffff\31\4\1\7\7\4\1\7\2\4\7\7\1\4\1\uffff\3\4\1\122\25\4\2\16\1\13\16\4\1\7\1\13\1\4\2\13\1\75\10\4\1\16\1\13\1\53\4\16\1\13\22\4\2\16\1\13\5\4\1\7\6\4\1\16\1\13\1\53\22\4\1\7\2\16\2\13\22\16\1\7\1\4\1\7\4\16\2\7\4\26\1\7\4\23\3\16\2\4\4\16\1\13\1\16\1\13\1\16\2\4\15\16\1\7\23\16\1\7\22\4\1\7\2\16\2\13\22\16\1\7\1\4\1\16\2\4\5\16\1\13\1\16\1\13\6\16\2\4\10\16\1\7\1\16\1\4\22\16\2\7\1\16\1\67\1\4\1\67\1\7\1\67\1\7\2\16\1\13\22\16\2\7\2\16\1\13\2\16\1\4\22\16\2\7\3\16\1\13\22\16\2\7\2\16\1\13\3\16\2\13\22\16\1\7\24\16\1\7\24\16\1\7\24\16\2\7\4\16\1\7\4\25\4\16\2\67\1\16";
    static final String dfa_3s = "\2\40\1\uffff\3\7\1\134\1\121\1\52\1\uffff\7\52\1\33\4\4\1\113\1\60\13\52\1\7\1\33\1\113\1\34\1\121\1\113\1\134\1\113\1\7\2\40\7\7\1\5\1\uffff\1\60\1\113\1\4\1\123\5\7\1\52\1\121\1\113\1\134\2\113\1\4\1\134\1\121\1\113\2\7\4\113\1\134\1\121\17\113\1\7\4\72\1\75\2\113\1\121\1\113\4\7\1\134\1\113\1\53\2\113\1\134\1\121\2\113\1\134\1\121\1\113\2\7\14\113\1\134\1\121\6\113\1\7\1\121\1\113\4\7\1\134\1\113\1\53\22\113\1\7\1\134\1\121\24\113\1\7\1\113\1\7\4\113\2\7\4\26\1\7\4\23\3\113\2\7\2\113\1\134\1\121\1\113\1\121\2\113\2\7\15\113\1\7\23\113\1\7\22\113\1\7\1\134\1\121\24\113\1\7\2\113\2\7\3\113\1\134\1\121\1\113\1\121\7\113\2\7\10\113\1\7\24\113\2\7\1\113\3\72\1\7\1\72\1\7\1\134\1\121\23\113\2\7\1\134\1\121\26\113\2\7\1\113\1\134\1\121\23\113\2\7\1\134\1\121\4\113\2\72\22\113\1\7\24\113\1\7\24\113\1\7\24\113\2\7\4\113\1\7\4\25\4\113\2\72\1\113";
    static final String dfa_4s = "\2\uffff\1\2\6\uffff\1\1\54\uffff\1\3\u01e8\uffff";
    static final String dfa_5s = "\u021f\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\1\uffff\2\2\20\uffff\1\1\5\uffff\3\2",
            "\1\7\1\uffff\1\6\1\10\20\uffff\1\11\5\uffff\1\3\1\4\1\5",
            "",
            "\1\7\1\uffff\1\6\1\10",
            "\1\7\1\uffff\1\6\1\10",
            "\1\7\1\uffff\1\6\1\10",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21\41\uffff\1\32\1\30\1\12\3\uffff\1\34\1\15\1\33\1\35\1\14\1\36\1\13\1\17\1\16\1\31\1\20",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21\1\41\43\uffff\1\40\1\37\1\42",
            "\1\26\6\uffff\1\43\25\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\45\25\uffff\1\44\1\46",
            "\1\26",
            "\1\26",
            "\1\26",
            "\1\26",
            "\1\47\1\50\1\51\1\52\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\70\1\uffff\1\72\23\uffff\1\67\1\uffff\1\71\17\uffff\1\73\1\74\1\75\1\76\1\77",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\100",
            "\1\45\26\uffff\1\46",
            "\1\101\1\102\1\103\1\104\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\105\27\uffff\1\106",
            "\1\110\1\50\1\107\1\111\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\4\uffff\1\114\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\116\1\117\1\115",
            "\1\47\1\50\1\51\1\52\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\127\1\133\1\137\3\uffff\1\132\1\126\1\135\1\134\1\124\1\136\1\123\1\140\1\130\1\125\1\131",
            "\1\121\1\uffff\1\120\1\122\3\uffff\1\141\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\142",
            "\1\66\1\uffff\1\66\1\143\26\uffff\3\66",
            "\1\66\1\uffff\1\66\1\143\26\uffff\3\66",
            "\1\142",
            "\1\142",
            "\1\142",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\146",
            "\1\147\1\150",
            "",
            "\1\70\1\uffff\1\72\25\uffff\1\71\17\uffff\1\73\1\74\1\75\1\76\1\77",
            "\1\151\1\152\1\157\1\160\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\155\1\156\1\153\1\154\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\161",
            "\1\162\1\163",
            "\1\165\1\uffff\1\164\1\166",
            "\1\165\1\uffff\1\164\1\166",
            "\1\165\1\uffff\1\164\1\166",
            "\1\165\1\uffff\1\164\1\166",
            "\1\165\1\uffff\1\164\1\166",
            "\1\26\34\uffff\1\22\1\23\1\24\1\25\4\uffff\1\27\1\21",
            "\1\171\1\102\1\170\1\172\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\4\uffff\1\177\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\175\1\176\1\167",
            "\1\101\1\102\1\103\1\104\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u008d\1\u0088\1\u0081\3\uffff\1\u008f\1\u0084\1\u008e\1\u0090\1\u0083\1\u0080\1\u0082\1\u0087\1\u0085\1\u008c\1\u0086",
            "\1\u008a\1\uffff\1\u0089\1\u008b\3\uffff\1\u0091\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0092\1\u0093\1\u0098\1\u0099\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\u0096\1\u0097\1\u0094\1\u0095\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u009a",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u00a2\1\u00a7\1\u009d\3\uffff\1\u00a3\1\u00a5\1\u00a8\1\u009f\1\u009c\1\u00a4\1\u009b\1\u00a0\1\u009e\1\u00a1\1\u00a6",
            "\1\110\1\50\1\107\1\111\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\4\uffff\1\u00ab\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u00aa\1\u00a9\1\u00ac",
            "\1\121\1\uffff\1\120\1\122\3\uffff\1\u00ad\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u00af\1\uffff\1\u00ae\1\u00b0",
            "\1\u00af\1\uffff\1\u00ae\1\u00b1",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u00b9\1\u00bd\1\u00b3\3\uffff\1\u00bc\1\u00b8\1\u00bf\1\u00be\1\u00b6\1\u00b2\1\u00b5\1\u00b4\1\u00ba\1\u00b7\1\u00bb",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u00c1\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u00c2\1\u00c0\1\u00c3",
            "\1\u00c4\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u00c5",
            "\1\u00c6\53\uffff\1\u00c7\1\u00c8\1\u00c9\1\u00ca",
            "\1\66\6\uffff\1\u00cb\25\uffff\4\66\4\uffff\2\66\14\uffff\1\u00c7\1\u00c8\1\u00c9\1\u00ca",
            "\1\u00cc\53\uffff\1\u00cd\1\u00ce\1\u00cf\1\u00d0",
            "\1\u00d1\53\uffff\1\u00d2\1\u00d3\1\u00d4\1\u00d5",
            "\1\u00d6",
            "\1\147\1\150\10\uffff\1\66\7\uffff\2\11\1\uffff\1\11\31\uffff\2\66\11\uffff\16\66",
            "\1\147\1\150\10\uffff\1\66\7\uffff\2\11\1\uffff\1\11\31\uffff\2\66\11\uffff\16\66",
            "\1\151\1\152\1\157\1\160\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\155\1\156\1\u00d9\1\u00da\2\uffff\1\u00db\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u00dc\1\u00d8\1\u00d7",
            "\1\151\1\152\1\157\1\160\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\155\1\156\1\153\1\154\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u00de\1\uffff\1\u00dd\1\u00df",
            "\1\u00de\1\uffff\1\u00dd\1\u00df",
            "\1\u00e0\1\uffff\1\157\1\160",
            "\1\u00e0\1\uffff\1\157\1\u00e1",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u00e9\1\u00ed\1\u00f1\3\uffff\1\u00ec\1\u00e8\1\u00ef\1\u00ee\1\u00e6\1\u00f0\1\u00e5\1\u00e2\1\u00ea\1\u00e7\1\u00eb",
            "\1\u00f2\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u00f3",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u00fc\1\u0100\1\u00f6\3\uffff\1\u00ff\1\u00fb\1\u0101\1\u00f4\1\u00f9\1\u00f5\1\u00f8\1\u00f7\1\u00fd\1\u00fa\1\u00fe",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u0105\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0103\1\u0104\1\u0102",
            "\1\u0106\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u0113\1\u0107\1\u010d\3\uffff\1\u0114\1\u010a\1\u0109\1\u010c\1\u010f\1\u0108\1\u010e\1\u0111\1\u0110\1\u0112\1\u010b",
            "\1\171\1\102\1\170\1\172\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\4\uffff\1\u0117\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0116\1\u0118\1\u0115",
            "\1\u008a\1\uffff\1\u0089\1\u008b\3\uffff\1\u0119\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u011b\1\uffff\1\u011a\1\u011c",
            "\1\u011b\1\uffff\1\u011a\1\u011d",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u012a\1\u011e\1\u0122\3\uffff\1\u012b\1\u0125\1\u0120\1\u011f\1\u0124\1\u0121\1\u0123\1\u0127\1\u0126\1\u0129\1\u0128",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u012d\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u012c\1\u012e\1\u012f",
            "\1\u0130\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0131",
            "\1\u0092\1\u0093\1\u0098\1\u0099\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\u0096\1\u0097\1\u0133\1\u0134\2\uffff\1\u0136\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0135\1\u0132\1\u0137",
            "\1\u0092\1\u0093\1\u0098\1\u0099\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\u0096\1\u0097\1\u0094\1\u0095\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0139\1\uffff\1\u0138\1\u013a",
            "\1\u0139\1\uffff\1\u0138\1\u013a",
            "\1\u013b\1\uffff\1\u0098\1\u0099",
            "\1\u013b\1\uffff\1\u0098\1\u013c",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u0147\1\u0145\1\u014c\3\uffff\1\u0149\1\u013f\1\u0148\1\u014a\1\u013e\1\u014b\1\u013d\1\u0141\1\u0140\1\u0146\1\u0142",
            "\1\u014d\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u014e",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u014f",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u0154\1\u0152\1\u0159\3\uffff\1\u0156\1\u015c\1\u0155\1\u0157\1\u015b\1\u0158\1\u015a\1\u0151\1\u015d\1\u0153\1\u0150",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u0161\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u015f\1\u0160\1\u015e",
            "\1\u0162\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0163\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0164",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0165",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\u0166",
            "\1\u0167",
            "\1\u0168",
            "\1\u0168",
            "\1\u0168",
            "\1\u0168",
            "\1\u0169",
            "\1\u016a",
            "\1\u016a",
            "\1\u016a",
            "\1\u016a",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u016c\1\uffff\1\u016b\1\u016d",
            "\1\u016c\1\uffff\1\u016b\1\u016d",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u0177\1\u0175\1\u016e\3\uffff\1\u0179\1\u0171\1\u0178\1\u017a\1\u0170\1\u017b\1\u016f\1\u0173\1\u0172\1\u0176\1\u0174",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u017e\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u017d\1\u017c\1\u017f",
            "\1\u0180\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\2\uffff\1\u00db\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u00dc\1\u00d8\1\u00d7",
            "\1\u0181\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0183\1\uffff\1\u0182\1\u0184",
            "\1\u0183\1\uffff\1\u0182\1\u0184",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0185",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0186",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0187",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u018f\1\u0193\1\u0189\3\uffff\1\u0192\1\u018d\1\u0195\1\u0194\1\u018c\1\u0188\1\u018b\1\u018a\1\u0190\1\u018e\1\u0191",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u0198\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0196\1\u0197\1\u0199",
            "\1\u019a\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u019b\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u019c",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u019e\1\uffff\1\u019d\1\u019f",
            "\1\u019e\1\uffff\1\u019d\1\u019f",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u01a8\1\u01ac\1\u01a2\3\uffff\1\u01ab\1\u01a7\1\u01ad\1\u01a0\1\u01a5\1\u01a1\1\u01a4\1\u01a3\1\u01a9\1\u01a6\1\u01aa",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u01b1\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u01af\1\u01b0\1\u01ae",
            "\1\u01b2\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\2\uffff\1\u0136\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0135\1\u0132\1\u0137",
            "\1\u01b3\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01b5\1\uffff\1\u01b4\1\u01b6",
            "\1\u01b5\1\uffff\1\u01b4\1\u01b6",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01b7",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\121\1\uffff\1\120\1\122\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\112\1\113\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01b8",
            "\1\u01b9",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u00c7\1\u00c8\1\u00c9\1\u00ca",
            "\1\66\34\uffff\4\66\4\uffff\2\66\14\uffff\1\u00c7\1\u00c8\1\u00c9\1\u00ca",
            "\1\u00cd\1\u00ce\1\u00cf\1\u00d0",
            "\1\u01ba",
            "\1\u00d2\1\u00d3\1\u00d4\1\u00d5",
            "\1\u01bb",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u01c4\1\u01c9\1\u01bc\3\uffff\1\u01c5\1\u01c7\1\u01bd\1\u01c0\1\u01bf\1\u01c6\1\u01be\1\u01c2\1\u01c1\1\u01c3\1\u01c8",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u01ca\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u01cb\1\u01cc\1\u01cd",
            "\1\u01ce\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01cf",
            "\1\u01d0",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u01d1\1\u01de\1\u01d6\3\uffff\1\u01d3\1\u01db\1\u01d2\1\u01d4\1\u01d9\1\u01d5\1\u01d8\1\u01d7\1\u01dc\1\u01da\1\u01dd",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u01df\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u01e0\1\u01e2\1\u01e1",
            "\1\u01e3\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u008a\1\uffff\1\u0089\1\u008b\6\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\7\uffff\1\173\1\174\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01e4",
            "\1\u01e5",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u01e7\1\u01ea\1\u01eb\3\uffff\1\u01e9\1\u01f1\1\u01ec\1\u01e6\1\u01ee\1\u01ef\1\u01ed\1\u01f2\1\u01f0\1\u01f3\1\u01e8",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u01f6\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u01f5\1\u01f7\1\u01f4",
            "\1\u01f8\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u01f9",
            "\1\u01fa",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\1\u01fb\1\u01ff\1\u0203\3\uffff\1\u01fe\1\u0206\1\u0201\1\u0200\1\u0205\1\u0202\1\u0204\1\u0207\1\u01fc\1\u0208\1\u01fd",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\4\uffff\1\u020b\5\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66\3\uffff\1\u0209\1\u020a\1\u020c",
            "\1\u020d\2\uffff\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\u020e\53\uffff\1\u020f\1\u0210\1\u0211\1\u0212",
            "\1\u0213\53\uffff\1\u0214\1\u0215\1\u0216\1\u0217",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0218",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u00e3\1\u00e4\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u0219",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u021a",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\1\u0143\1\u0144\10\uffff\1\61\1\53\1\54\1\55\1\56\1\57\1\u00c7\1\u00c8\1\u00c9\1\u00ca\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u021b",
            "\1\u021c",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66",
            "\1\u021d",
            "\1\u021e",
            "\1\u021e",
            "\1\u021e",
            "\1\u021e",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\66\5\uffff\1\62\1\uffff\2\11\1\uffff\1\11\3\uffff\1\65\10\uffff\1\60\12\uffff\1\61\1\53\1\54\1\55\1\56\1\57\4\uffff\1\63\1\64\1\uffff\16\66",
            "\1\u020f\1\u0210\1\u0211\1\u0212",
            "\1\u0214\1\u0215\1\u0216\1\u0217",
            "\1\66\7\uffff\2\11\1\uffff\1\11\3\uffff\1\65\25\uffff\2\66\11\uffff\16\66"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1003:2: ( (otherlv_0= '(' this_CompoundClause_1= ruleCompoundClause otherlv_2= ')' ) | ( (lv_clause_3_0= ruleClause ) ) | ( (lv_complexclause_4_0= ruleComplexClause ) ) )";
        }
    }
    static final String dfa_7s = "\100\uffff";
    static final String dfa_8s = "\1\uffff\1\51\1\77\75\uffff";
    static final String dfa_9s = "\3\4\75\uffff";
    static final String dfa_10s = "\1\7\1\134\1\121\75\uffff";
    static final String dfa_11s = "\3\uffff\1\15\1\10\1\5\1\6\1\3\1\11\1\4\1\2\1\7\1\1\1\12\60\14\1\13\1\16";
    static final String dfa_12s = "\1\uffff\1\0\1\1\75\uffff}>";
    static final String[] dfa_13s = {
            "\1\2\1\uffff\1\1\1\3",
            "\1\23\1\uffff\1\25\1\26\3\uffff\1\43\2\uffff\1\63\1\46\1\47\1\50\1\uffff\1\52\1\37\1\53\1\44\1\45\1\uffff\1\73\3\uffff\1\42\3\uffff\1\17\1\20\1\21\1\22\1\27\1\30\1\74\1\75\1\24\1\16\6\uffff\1\36\1\31\1\32\1\33\1\34\1\35\4\uffff\1\40\1\41\1\uffff\1\67\1\70\1\71\1\72\1\64\1\65\1\66\1\54\1\55\1\56\1\57\1\60\1\61\1\62\1\14\1\12\1\7\3\uffff\1\4\1\13\1\5\1\6\1\11\1\10\2\7\2\12\1\15",
            "\1\77\1\uffff\2\77\3\uffff\1\77\2\uffff\4\77\1\uffff\5\77\1\uffff\1\77\3\uffff\1\77\3\uffff\12\77\1\76\5\uffff\6\77\4\uffff\2\77\1\uffff\16\77\3\uffff\3\7",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA42 extends DFA {

        public DFA42(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 42;
            this.eot = dfa_7;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "2142:2: (this_System_0= ruleSystem | this_Device_1= ruleDevice | this_Parameter_2= ruleParameter | this_User_3= ruleUser | this_Event_4= ruleEvent | this_Status_5= ruleStatus | this_Mode_6= ruleMode | this_State_7= ruleState | this_Entity_8= ruleEntity | this_Attribute_9= ruleAttribute | ( ( ruleVariable )=>this_Variable_10= ruleVariable ) | ( ( ruleLiteral )=>this_Literal_11= ruleLiteral ) | this_Numeric_12= ruleNumeric | ( (otherlv_13= RULE_ID ) ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA42_1 = input.LA(1);

                         
                        int index42_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (LA42_1==82) ) {s = 4;}

                        else if ( (LA42_1==84) ) {s = 5;}

                        else if ( (LA42_1==85) ) {s = 6;}

                        else if ( (LA42_1==78||(LA42_1>=88 && LA42_1<=89)) ) {s = 7;}

                        else if ( (LA42_1==87) ) {s = 8;}

                        else if ( (LA42_1==86) ) {s = 9;}

                        else if ( (LA42_1==77||(LA42_1>=90 && LA42_1<=91)) ) {s = 10;}

                        else if ( (LA42_1==83) ) {s = 11;}

                        else if ( (LA42_1==76) ) {s = 12;}

                        else if ( (LA42_1==92) ) {s = 13;}

                        else if ( (LA42_1==42) && (synpred4_InternalResa())) {s = 14;}

                        else if ( (LA42_1==33) && (synpred4_InternalResa())) {s = 15;}

                        else if ( (LA42_1==34) && (synpred4_InternalResa())) {s = 16;}

                        else if ( (LA42_1==35) && (synpred4_InternalResa())) {s = 17;}

                        else if ( (LA42_1==36) && (synpred4_InternalResa())) {s = 18;}

                        else if ( (LA42_1==RULE_ID) && (synpred4_InternalResa())) {s = 19;}

                        else if ( (LA42_1==41) && (synpred4_InternalResa())) {s = 20;}

                        else if ( (LA42_1==RULE_STRING) && (synpred4_InternalResa())) {s = 21;}

                        else if ( (LA42_1==RULE_INT) && (synpred4_InternalResa())) {s = 22;}

                        else if ( (LA42_1==37) && (synpred4_InternalResa())) {s = 23;}

                        else if ( (LA42_1==38) && (synpred4_InternalResa())) {s = 24;}

                        else if ( (LA42_1==50) && (synpred4_InternalResa())) {s = 25;}

                        else if ( (LA42_1==51) && (synpred4_InternalResa())) {s = 26;}

                        else if ( (LA42_1==52) && (synpred4_InternalResa())) {s = 27;}

                        else if ( (LA42_1==53) && (synpred4_InternalResa())) {s = 28;}

                        else if ( (LA42_1==54) && (synpred4_InternalResa())) {s = 29;}

                        else if ( (LA42_1==49) && (synpred4_InternalResa())) {s = 30;}

                        else if ( (LA42_1==20) && (synpred4_InternalResa())) {s = 31;}

                        else if ( (LA42_1==59) && (synpred4_InternalResa())) {s = 32;}

                        else if ( (LA42_1==60) && (synpred4_InternalResa())) {s = 33;}

                        else if ( (LA42_1==29) && (synpred4_InternalResa())) {s = 34;}

                        else if ( (LA42_1==11) && (synpred4_InternalResa())) {s = 35;}

                        else if ( (LA42_1==22) && (synpred4_InternalResa())) {s = 36;}

                        else if ( (LA42_1==23) && (synpred4_InternalResa())) {s = 37;}

                        else if ( (LA42_1==15) && (synpred4_InternalResa())) {s = 38;}

                        else if ( (LA42_1==16) && (synpred4_InternalResa())) {s = 39;}

                        else if ( (LA42_1==17) && (synpred4_InternalResa())) {s = 40;}

                        else if ( (LA42_1==EOF) && (synpred4_InternalResa())) {s = 41;}

                        else if ( (LA42_1==19) && (synpred4_InternalResa())) {s = 42;}

                        else if ( (LA42_1==21) && (synpred4_InternalResa())) {s = 43;}

                        else if ( (LA42_1==69) && (synpred4_InternalResa())) {s = 44;}

                        else if ( (LA42_1==70) && (synpred4_InternalResa())) {s = 45;}

                        else if ( (LA42_1==71) && (synpred4_InternalResa())) {s = 46;}

                        else if ( (LA42_1==72) && (synpred4_InternalResa())) {s = 47;}

                        else if ( (LA42_1==73) && (synpred4_InternalResa())) {s = 48;}

                        else if ( (LA42_1==74) && (synpred4_InternalResa())) {s = 49;}

                        else if ( (LA42_1==75) && (synpred4_InternalResa())) {s = 50;}

                        else if ( (LA42_1==14) && (synpred4_InternalResa())) {s = 51;}

                        else if ( (LA42_1==66) && (synpred4_InternalResa())) {s = 52;}

                        else if ( (LA42_1==67) && (synpred4_InternalResa())) {s = 53;}

                        else if ( (LA42_1==68) && (synpred4_InternalResa())) {s = 54;}

                        else if ( (LA42_1==62) && (synpred4_InternalResa())) {s = 55;}

                        else if ( (LA42_1==63) && (synpred4_InternalResa())) {s = 56;}

                        else if ( (LA42_1==64) && (synpred4_InternalResa())) {s = 57;}

                        else if ( (LA42_1==65) && (synpred4_InternalResa())) {s = 58;}

                        else if ( (LA42_1==25) && (synpred4_InternalResa())) {s = 59;}

                        else if ( (LA42_1==39) && (synpred4_InternalResa())) {s = 60;}

                        else if ( (LA42_1==40) && (synpred4_InternalResa())) {s = 61;}

                         
                        input.seek(index42_1);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA42_2 = input.LA(1);

                         
                        int index42_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( ((LA42_2>=79 && LA42_2<=81)) ) {s = 7;}

                        else if ( (LA42_2==43) && (synpred3_InternalResa())) {s = 62;}

                        else if ( (LA42_2==EOF||LA42_2==RULE_ID||(LA42_2>=RULE_STRING && LA42_2<=RULE_INT)||LA42_2==11||(LA42_2>=14 && LA42_2<=17)||(LA42_2>=19 && LA42_2<=23)||LA42_2==25||LA42_2==29||(LA42_2>=33 && LA42_2<=42)||(LA42_2>=49 && LA42_2<=54)||(LA42_2>=59 && LA42_2<=60)||(LA42_2>=62 && LA42_2<=75)) ) {s = 63;}

                         
                        input.seek(index42_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 42, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000046002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000001C00000D0L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000001C10000D0L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000001C10460D0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0xC0180001C11040D0L,0x0000000000000FFFL});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000001C11000D0L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0xC0180001C10040D0L,0x0000000000000FFFL});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0xC018000000004000L,0x0000000000000FFFL});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0xC018000000004800L,0x0000000000000FFFL});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000C00002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000061E00000010L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000001E0C000010L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000001E08000010L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x187E0041E01000D2L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x187E004020100002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000001E10000010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x187E01E1E01000D2L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000001E00000010L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0001F01E14000050L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x00000061C00000D2L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000018000000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0780000000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000000L,0x0000000000001000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});

}