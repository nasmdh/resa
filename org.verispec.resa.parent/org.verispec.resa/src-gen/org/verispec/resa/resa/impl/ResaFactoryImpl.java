/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.verispec.resa.resa.ADJ;
import org.verispec.resa.resa.AVB;
import org.verispec.resa.resa.ActivePredicate;
import org.verispec.resa.resa.Attribute;
import org.verispec.resa.resa.Between;
import org.verispec.resa.resa.Clause;
import org.verispec.resa.resa.Comparison;
import org.verispec.resa.resa.ComparisonOperator;
import org.verispec.resa.resa.Complement;
import org.verispec.resa.resa.ComplexClause;
import org.verispec.resa.resa.ComplexSpec;
import org.verispec.resa.resa.CompoundClause;
import org.verispec.resa.resa.Conditional;
import org.verispec.resa.resa.Device;
import org.verispec.resa.resa.Ditransitive;
import org.verispec.resa.resa.Else;
import org.verispec.resa.resa.Entity;
import org.verispec.resa.resa.Event;
import org.verispec.resa.resa.IF;
import org.verispec.resa.resa.InDevice;
import org.verispec.resa.resa.InParameter;
import org.verispec.resa.resa.InSignal;
import org.verispec.resa.resa.LOP;
import org.verispec.resa.resa.Literal;
import org.verispec.resa.resa.LogicalOP;
import org.verispec.resa.resa.MOD;
import org.verispec.resa.resa.Mode;
import org.verispec.resa.resa.NP;
import org.verispec.resa.resa.Numeric;
import org.verispec.resa.resa.Occurence;
import org.verispec.resa.resa.OutDevice;
import org.verispec.resa.resa.OutParameter;
import org.verispec.resa.resa.OutSignal;
import org.verispec.resa.resa.Parameter;
import org.verispec.resa.resa.PassivePredicate;
import org.verispec.resa.resa.Possession;
import org.verispec.resa.resa.ReqSpec;
import org.verispec.resa.resa.ReqsSpecs;
import org.verispec.resa.resa.ResaDataType;
import org.verispec.resa.resa.ResaFactory;
import org.verispec.resa.resa.ResaPackage;
import org.verispec.resa.resa.SJImport;
import org.verispec.resa.resa.Signal;
import org.verispec.resa.resa.SimpleSpec;
import org.verispec.resa.resa.State;
import org.verispec.resa.resa.Status;
import org.verispec.resa.resa.StructuredSpec;
import org.verispec.resa.resa.SubConjCause;
import org.verispec.resa.resa.SubConjConditional;
import org.verispec.resa.resa.SubConjTemporal;
import org.verispec.resa.resa.SubordinateClause;
import org.verispec.resa.resa.SubordinateConjunctive;
import org.verispec.resa.resa.TOBE;
import org.verispec.resa.resa.Text;
import org.verispec.resa.resa.Time;
import org.verispec.resa.resa.Timing;
import org.verispec.resa.resa.TimingPP;
import org.verispec.resa.resa.Transitive;
import org.verispec.resa.resa.Unit;
import org.verispec.resa.resa.User;
import org.verispec.resa.resa.VB;
import org.verispec.resa.resa.Variable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResaFactoryImpl extends EFactoryImpl implements ResaFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static ResaFactory init()
  {
    try
    {
      ResaFactory theResaFactory = (ResaFactory)EPackage.Registry.INSTANCE.getEFactory(ResaPackage.eNS_URI);
      if (theResaFactory != null)
      {
        return theResaFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new ResaFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResaFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case ResaPackage.REQS_SPECS: return createReqsSpecs();
      case ResaPackage.SJ_IMPORT: return createSJImport();
      case ResaPackage.REQ_SPEC: return createReqSpec();
      case ResaPackage.SIMPLE_SPEC: return createSimpleSpec();
      case ResaPackage.STRUCTURED_SPEC: return createStructuredSpec();
      case ResaPackage.CONDITIONAL: return createConditional();
      case ResaPackage.ELSE: return createElse();
      case ResaPackage.COMPLEX_SPEC: return createComplexSpec();
      case ResaPackage.COMPOUND_CLAUSE: return createCompoundClause();
      case ResaPackage.LOGICAL_OP: return createLogicalOP();
      case ResaPackage.COMPLEX_CLAUSE: return createComplexClause();
      case ResaPackage.COMPLEMENT: return createComplement();
      case ResaPackage.CLAUSE: return createClause();
      case ResaPackage.POSSESSION: return createPossession();
      case ResaPackage.TEXT: return createText();
      case ResaPackage.ACTIVE_PREDICATE: return createActivePredicate();
      case ResaPackage.PASSIVE_PREDICATE: return createPassivePredicate();
      case ResaPackage.TOBE: return createTOBE();
      case ResaPackage.VB: return createVB();
      case ResaPackage.MOD: return createMOD();
      case ResaPackage.NP: return createNP();
      case ResaPackage.LITERAL: return createLiteral();
      case ResaPackage.VARIABLE: return createVariable();
      case ResaPackage.ADJ: return createADJ();
      case ResaPackage.COMPARISON: return createComparison();
      case ResaPackage.COMPARISON_OPERATOR: return createComparisonOperator();
      case ResaPackage.AVB: return createAVB();
      case ResaPackage.TIMING: return createTiming();
      case ResaPackage.BETWEEN: return createBetween();
      case ResaPackage.TIMING_PP: return createTimingPP();
      case ResaPackage.TIME: return createTime();
      case ResaPackage.UNIT: return createUnit();
      case ResaPackage.NUMERIC: return createNumeric();
      case ResaPackage.OCCURENCE: return createOccurence();
      case ResaPackage.SUBORDINATE_CLAUSE: return createSubordinateClause();
      case ResaPackage.SUBORDINATE_CONJUNCTIVE: return createSubordinateConjunctive();
      case ResaPackage.SUB_CONJ_CAUSE: return createSubConjCause();
      case ResaPackage.SUB_CONJ_CONDITIONAL: return createSubConjConditional();
      case ResaPackage.SUB_CONJ_TEMPORAL: return createSubConjTemporal();
      case ResaPackage.RESA_DATA_TYPE: return createResaDataType();
      case ResaPackage.SYSTEM: return createSystem();
      case ResaPackage.DEVICE: return createDevice();
      case ResaPackage.PARAMETER: return createParameter();
      case ResaPackage.SIGNAL: return createSignal();
      case ResaPackage.OUT_SIGNAL: return createOutSignal();
      case ResaPackage.IN_SIGNAL: return createInSignal();
      case ResaPackage.STATE: return createState();
      case ResaPackage.MODE: return createMode();
      case ResaPackage.EVENT: return createEvent();
      case ResaPackage.STATUS: return createStatus();
      case ResaPackage.USER: return createUser();
      case ResaPackage.ENTITY: return createEntity();
      case ResaPackage.IN_PARAMETER: return createInParameter();
      case ResaPackage.OUT_PARAMETER: return createOutParameter();
      case ResaPackage.IN_DEVICE: return createInDevice();
      case ResaPackage.OUT_DEVICE: return createOutDevice();
      case ResaPackage.ATTRIBUTE: return createAttribute();
      case ResaPackage.IF: return createIF();
      case ResaPackage.LOP: return createLOP();
      case ResaPackage.TRANSITIVE: return createTransitive();
      case ResaPackage.DITRANSITIVE: return createDitransitive();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReqsSpecs createReqsSpecs()
  {
    ReqsSpecsImpl reqsSpecs = new ReqsSpecsImpl();
    return reqsSpecs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SJImport createSJImport()
  {
    SJImportImpl sjImport = new SJImportImpl();
    return sjImport;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReqSpec createReqSpec()
  {
    ReqSpecImpl reqSpec = new ReqSpecImpl();
    return reqSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleSpec createSimpleSpec()
  {
    SimpleSpecImpl simpleSpec = new SimpleSpecImpl();
    return simpleSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public StructuredSpec createStructuredSpec()
  {
    StructuredSpecImpl structuredSpec = new StructuredSpecImpl();
    return structuredSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Conditional createConditional()
  {
    ConditionalImpl conditional = new ConditionalImpl();
    return conditional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Else createElse()
  {
    ElseImpl else_ = new ElseImpl();
    return else_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComplexSpec createComplexSpec()
  {
    ComplexSpecImpl complexSpec = new ComplexSpecImpl();
    return complexSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CompoundClause createCompoundClause()
  {
    CompoundClauseImpl compoundClause = new CompoundClauseImpl();
    return compoundClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LogicalOP createLogicalOP()
  {
    LogicalOPImpl logicalOP = new LogicalOPImpl();
    return logicalOP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComplexClause createComplexClause()
  {
    ComplexClauseImpl complexClause = new ComplexClauseImpl();
    return complexClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Complement createComplement()
  {
    ComplementImpl complement = new ComplementImpl();
    return complement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Clause createClause()
  {
    ClauseImpl clause = new ClauseImpl();
    return clause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Possession createPossession()
  {
    PossessionImpl possession = new PossessionImpl();
    return possession;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Text createText()
  {
    TextImpl text = new TextImpl();
    return text;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ActivePredicate createActivePredicate()
  {
    ActivePredicateImpl activePredicate = new ActivePredicateImpl();
    return activePredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PassivePredicate createPassivePredicate()
  {
    PassivePredicateImpl passivePredicate = new PassivePredicateImpl();
    return passivePredicate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TOBE createTOBE()
  {
    TOBEImpl tobe = new TOBEImpl();
    return tobe;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VB createVB()
  {
    VBImpl vb = new VBImpl();
    return vb;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MOD createMOD()
  {
    MODImpl mod = new MODImpl();
    return mod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NP createNP()
  {
    NPImpl np = new NPImpl();
    return np;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Literal createLiteral()
  {
    LiteralImpl literal = new LiteralImpl();
    return literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ADJ createADJ()
  {
    ADJImpl adj = new ADJImpl();
    return adj;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Comparison createComparison()
  {
    ComparisonImpl comparison = new ComparisonImpl();
    return comparison;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComparisonOperator createComparisonOperator()
  {
    ComparisonOperatorImpl comparisonOperator = new ComparisonOperatorImpl();
    return comparisonOperator;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AVB createAVB()
  {
    AVBImpl avb = new AVBImpl();
    return avb;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Timing createTiming()
  {
    TimingImpl timing = new TimingImpl();
    return timing;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Between createBetween()
  {
    BetweenImpl between = new BetweenImpl();
    return between;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TimingPP createTimingPP()
  {
    TimingPPImpl timingPP = new TimingPPImpl();
    return timingPP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Time createTime()
  {
    TimeImpl time = new TimeImpl();
    return time;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Unit createUnit()
  {
    UnitImpl unit = new UnitImpl();
    return unit;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Numeric createNumeric()
  {
    NumericImpl numeric = new NumericImpl();
    return numeric;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Occurence createOccurence()
  {
    OccurenceImpl occurence = new OccurenceImpl();
    return occurence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubordinateClause createSubordinateClause()
  {
    SubordinateClauseImpl subordinateClause = new SubordinateClauseImpl();
    return subordinateClause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubordinateConjunctive createSubordinateConjunctive()
  {
    SubordinateConjunctiveImpl subordinateConjunctive = new SubordinateConjunctiveImpl();
    return subordinateConjunctive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubConjCause createSubConjCause()
  {
    SubConjCauseImpl subConjCause = new SubConjCauseImpl();
    return subConjCause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubConjConditional createSubConjConditional()
  {
    SubConjConditionalImpl subConjConditional = new SubConjConditionalImpl();
    return subConjConditional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubConjTemporal createSubConjTemporal()
  {
    SubConjTemporalImpl subConjTemporal = new SubConjTemporalImpl();
    return subConjTemporal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResaDataType createResaDataType()
  {
    ResaDataTypeImpl resaDataType = new ResaDataTypeImpl();
    return resaDataType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public org.verispec.resa.resa.System createSystem()
  {
    SystemImpl system = new SystemImpl();
    return system;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Device createDevice()
  {
    DeviceImpl device = new DeviceImpl();
    return device;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Parameter createParameter()
  {
    ParameterImpl parameter = new ParameterImpl();
    return parameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Signal createSignal()
  {
    SignalImpl signal = new SignalImpl();
    return signal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutSignal createOutSignal()
  {
    OutSignalImpl outSignal = new OutSignalImpl();
    return outSignal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InSignal createInSignal()
  {
    InSignalImpl inSignal = new InSignalImpl();
    return inSignal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public State createState()
  {
    StateImpl state = new StateImpl();
    return state;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Mode createMode()
  {
    ModeImpl mode = new ModeImpl();
    return mode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event createEvent()
  {
    EventImpl event = new EventImpl();
    return event;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Status createStatus()
  {
    StatusImpl status = new StatusImpl();
    return status;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public User createUser()
  {
    UserImpl user = new UserImpl();
    return user;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Entity createEntity()
  {
    EntityImpl entity = new EntityImpl();
    return entity;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InParameter createInParameter()
  {
    InParameterImpl inParameter = new InParameterImpl();
    return inParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutParameter createOutParameter()
  {
    OutParameterImpl outParameter = new OutParameterImpl();
    return outParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InDevice createInDevice()
  {
    InDeviceImpl inDevice = new InDeviceImpl();
    return inDevice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutDevice createOutDevice()
  {
    OutDeviceImpl outDevice = new OutDeviceImpl();
    return outDevice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Attribute createAttribute()
  {
    AttributeImpl attribute = new AttributeImpl();
    return attribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IF createIF()
  {
    IFImpl if_ = new IFImpl();
    return if_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LOP createLOP()
  {
    LOPImpl lop = new LOPImpl();
    return lop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Transitive createTransitive()
  {
    TransitiveImpl transitive = new TransitiveImpl();
    return transitive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ditransitive createDitransitive()
  {
    DitransitiveImpl ditransitive = new DitransitiveImpl();
    return ditransitive;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ResaPackage getResaPackage()
  {
    return (ResaPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static ResaPackage getPackage()
  {
    return ResaPackage.eINSTANCE;
  }

} //ResaFactoryImpl
