/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.ecore.EClass;

import org.verispec.resa.resa.OutParameter;
import org.verispec.resa.resa.ResaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OutParameterImpl extends ParameterImpl implements OutParameter
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OutParameterImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.OUT_PARAMETER;
  }

} //OutParameterImpl
