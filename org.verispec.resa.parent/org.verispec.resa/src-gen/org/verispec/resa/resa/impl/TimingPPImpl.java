/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.verispec.resa.resa.ResaPackage;
import org.verispec.resa.resa.TimingPP;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timing PP</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.verispec.resa.resa.impl.TimingPPImpl#getTprep <em>Tprep</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimingPPImpl extends MinimalEObjectImpl.Container implements TimingPP
{
  /**
   * The default value of the '{@link #getTprep() <em>Tprep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTprep()
   * @generated
   * @ordered
   */
  protected static final String TPREP_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTprep() <em>Tprep</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTprep()
   * @generated
   * @ordered
   */
  protected String tprep = TPREP_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimingPPImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.TIMING_PP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTprep()
  {
    return tprep;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTprep(String newTprep)
  {
    String oldTprep = tprep;
    tprep = newTprep;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ResaPackage.TIMING_PP__TPREP, oldTprep, tprep));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ResaPackage.TIMING_PP__TPREP:
        return getTprep();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ResaPackage.TIMING_PP__TPREP:
        setTprep((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.TIMING_PP__TPREP:
        setTprep(TPREP_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.TIMING_PP__TPREP:
        return TPREP_EDEFAULT == null ? tprep != null : !TPREP_EDEFAULT.equals(tprep);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (tprep: ");
    result.append(tprep);
    result.append(')');
    return result.toString();
  }

} //TimingPPImpl
