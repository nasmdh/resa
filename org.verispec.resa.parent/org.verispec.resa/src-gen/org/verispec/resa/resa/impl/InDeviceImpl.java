/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.ecore.EClass;

import org.verispec.resa.resa.InDevice;
import org.verispec.resa.resa.ResaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Device</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InDeviceImpl extends DeviceImpl implements InDevice
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InDeviceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.IN_DEVICE;
  }

} //InDeviceImpl
