/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.verispec.resa.resa.Complement;
import org.verispec.resa.resa.ResaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ComplementImpl extends MinimalEObjectImpl.Container implements Complement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComplementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.COMPLEMENT;
  }

} //ComplementImpl
