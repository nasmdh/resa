/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.verispec.resa.resa.Clause;
import org.verispec.resa.resa.ResaPackage;
import org.verispec.resa.resa.SubordinateClause;
import org.verispec.resa.resa.SubordinateConjunctive;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subordinate Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.verispec.resa.resa.impl.SubordinateClauseImpl#getSubconj <em>Subconj</em>}</li>
 *   <li>{@link org.verispec.resa.resa.impl.SubordinateClauseImpl#getClause <em>Clause</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubordinateClauseImpl extends MinimalEObjectImpl.Container implements SubordinateClause
{
  /**
   * The cached value of the '{@link #getSubconj() <em>Subconj</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubconj()
   * @generated
   * @ordered
   */
  protected SubordinateConjunctive subconj;

  /**
   * The cached value of the '{@link #getClause() <em>Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getClause()
   * @generated
   * @ordered
   */
  protected Clause clause;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SubordinateClauseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.SUBORDINATE_CLAUSE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubordinateConjunctive getSubconj()
  {
    return subconj;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSubconj(SubordinateConjunctive newSubconj, NotificationChain msgs)
  {
    SubordinateConjunctive oldSubconj = subconj;
    subconj = newSubconj;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ, oldSubconj, newSubconj);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubconj(SubordinateConjunctive newSubconj)
  {
    if (newSubconj != subconj)
    {
      NotificationChain msgs = null;
      if (subconj != null)
        msgs = ((InternalEObject)subconj).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ, null, msgs);
      if (newSubconj != null)
        msgs = ((InternalEObject)newSubconj).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ, null, msgs);
      msgs = basicSetSubconj(newSubconj, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ, newSubconj, newSubconj));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Clause getClause()
  {
    return clause;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetClause(Clause newClause, NotificationChain msgs)
  {
    Clause oldClause = clause;
    clause = newClause;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResaPackage.SUBORDINATE_CLAUSE__CLAUSE, oldClause, newClause);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setClause(Clause newClause)
  {
    if (newClause != clause)
    {
      NotificationChain msgs = null;
      if (clause != null)
        msgs = ((InternalEObject)clause).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResaPackage.SUBORDINATE_CLAUSE__CLAUSE, null, msgs);
      if (newClause != null)
        msgs = ((InternalEObject)newClause).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResaPackage.SUBORDINATE_CLAUSE__CLAUSE, null, msgs);
      msgs = basicSetClause(newClause, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ResaPackage.SUBORDINATE_CLAUSE__CLAUSE, newClause, newClause));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ:
        return basicSetSubconj(null, msgs);
      case ResaPackage.SUBORDINATE_CLAUSE__CLAUSE:
        return basicSetClause(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ:
        return getSubconj();
      case ResaPackage.SUBORDINATE_CLAUSE__CLAUSE:
        return getClause();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ:
        setSubconj((SubordinateConjunctive)newValue);
        return;
      case ResaPackage.SUBORDINATE_CLAUSE__CLAUSE:
        setClause((Clause)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ:
        setSubconj((SubordinateConjunctive)null);
        return;
      case ResaPackage.SUBORDINATE_CLAUSE__CLAUSE:
        setClause((Clause)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CLAUSE__SUBCONJ:
        return subconj != null;
      case ResaPackage.SUBORDINATE_CLAUSE__CLAUSE:
        return clause != null;
    }
    return super.eIsSet(featureID);
  }

} //SubordinateClauseImpl
