/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.verispec.resa.resa.ResaPackage;
import org.verispec.resa.resa.SubordinateConjunctive;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subordinate Conjunctive</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.verispec.resa.resa.impl.SubordinateConjunctiveImpl#getSubconj <em>Subconj</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubordinateConjunctiveImpl extends MinimalEObjectImpl.Container implements SubordinateConjunctive
{
  /**
   * The default value of the '{@link #getSubconj() <em>Subconj</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubconj()
   * @generated
   * @ordered
   */
  protected static final String SUBCONJ_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSubconj() <em>Subconj</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubconj()
   * @generated
   * @ordered
   */
  protected String subconj = SUBCONJ_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SubordinateConjunctiveImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return ResaPackage.Literals.SUBORDINATE_CONJUNCTIVE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSubconj()
  {
    return subconj;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSubconj(String newSubconj)
  {
    String oldSubconj = subconj;
    subconj = newSubconj;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ResaPackage.SUBORDINATE_CONJUNCTIVE__SUBCONJ, oldSubconj, subconj));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CONJUNCTIVE__SUBCONJ:
        return getSubconj();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CONJUNCTIVE__SUBCONJ:
        setSubconj((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CONJUNCTIVE__SUBCONJ:
        setSubconj(SUBCONJ_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case ResaPackage.SUBORDINATE_CONJUNCTIVE__SUBCONJ:
        return SUBCONJ_EDEFAULT == null ? subconj != null : !SUBCONJ_EDEFAULT.equals(subconj);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (subconj: ");
    result.append(subconj);
    result.append(')');
    return result.toString();
  }

} //SubordinateConjunctiveImpl
