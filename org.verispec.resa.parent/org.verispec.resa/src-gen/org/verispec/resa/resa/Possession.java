/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Possession</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.verispec.resa.resa.Possession#getStatus <em>Status</em>}</li>
 *   <li>{@link org.verispec.resa.resa.Possession#getState <em>State</em>}</li>
 *   <li>{@link org.verispec.resa.resa.Possession#getMode <em>Mode</em>}</li>
 * </ul>
 *
 * @see org.verispec.resa.resa.ResaPackage#getPossession()
 * @model
 * @generated
 */
public interface Possession extends EObject
{
  /**
   * Returns the value of the '<em><b>Status</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Status</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Status</em>' attribute.
   * @see #setStatus(String)
   * @see org.verispec.resa.resa.ResaPackage#getPossession_Status()
   * @model
   * @generated
   */
  String getStatus();

  /**
   * Sets the value of the '{@link org.verispec.resa.resa.Possession#getStatus <em>Status</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Status</em>' attribute.
   * @see #getStatus()
   * @generated
   */
  void setStatus(String value);

  /**
   * Returns the value of the '<em><b>State</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>State</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>State</em>' attribute.
   * @see #setState(String)
   * @see org.verispec.resa.resa.ResaPackage#getPossession_State()
   * @model
   * @generated
   */
  String getState();

  /**
   * Sets the value of the '{@link org.verispec.resa.resa.Possession#getState <em>State</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>State</em>' attribute.
   * @see #getState()
   * @generated
   */
  void setState(String value);

  /**
   * Returns the value of the '<em><b>Mode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mode</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mode</em>' attribute.
   * @see #setMode(String)
   * @see org.verispec.resa.resa.ResaPackage#getPossession_Mode()
   * @model
   * @generated
   */
  String getMode();

  /**
   * Sets the value of the '{@link org.verispec.resa.resa.Possession#getMode <em>Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mode</em>' attribute.
   * @see #getMode()
   * @generated
   */
  void setMode(String value);

} // Possession
