/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.verispec.resa.resa.ResaPackage#getConditional()
 * @model
 * @generated
 */
public interface Conditional extends StructuredSpec, Else
{
} // Conditional
