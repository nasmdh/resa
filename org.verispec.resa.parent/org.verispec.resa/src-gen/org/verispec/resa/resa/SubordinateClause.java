/**
 * generated by Xtext 2.13.0
 */
package org.verispec.resa.resa;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subordinate Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.verispec.resa.resa.SubordinateClause#getSubconj <em>Subconj</em>}</li>
 *   <li>{@link org.verispec.resa.resa.SubordinateClause#getClause <em>Clause</em>}</li>
 * </ul>
 *
 * @see org.verispec.resa.resa.ResaPackage#getSubordinateClause()
 * @model
 * @generated
 */
public interface SubordinateClause extends EObject
{
  /**
   * Returns the value of the '<em><b>Subconj</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Subconj</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Subconj</em>' containment reference.
   * @see #setSubconj(SubordinateConjunctive)
   * @see org.verispec.resa.resa.ResaPackage#getSubordinateClause_Subconj()
   * @model containment="true"
   * @generated
   */
  SubordinateConjunctive getSubconj();

  /**
   * Sets the value of the '{@link org.verispec.resa.resa.SubordinateClause#getSubconj <em>Subconj</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Subconj</em>' containment reference.
   * @see #getSubconj()
   * @generated
   */
  void setSubconj(SubordinateConjunctive value);

  /**
   * Returns the value of the '<em><b>Clause</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Clause</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Clause</em>' containment reference.
   * @see #setClause(Clause)
   * @see org.verispec.resa.resa.ResaPackage#getSubordinateClause_Clause()
   * @model containment="true"
   * @generated
   */
  Clause getClause();

  /**
   * Sets the value of the '{@link org.verispec.resa.resa.SubordinateClause#getClause <em>Clause</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Clause</em>' containment reference.
   * @see #getClause()
   * @generated
   */
  void setClause(Clause value);

} // SubordinateClause
