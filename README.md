# README #

The ReSA toolchain is a requirements specification and analysis tool tailored for embedded software development. The requirements specifications are expressed in the ReSA language, a constrained natural language. Then, the specifications are checked for consistency using the Z3 solver.

### Tutorial: https://www.youtube.com/watch?v=-nlz4jooDC4&feature=youtu.be###

### The Repository ###
The source code contains xText artifacts and a Z3 plugin to check consistency of requirements specifications.

### Developers ###
* Download with Xtext SDK
* Clone the ReSA project and import it to workspace.

### Users ###
* Install the Xtest Redistribution.
* Extract the z3 build, then add its path to PATH env. variable.
* Note: The Z3 build is compatible to x64 AMD machine. So, for any other machine types, please build from the Z3 source.


### Contact ###

* Nesredin (nesredin.mahmud@mdh.se)