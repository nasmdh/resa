package org.verispec.resa.consistencycheck.handlers;

import java.util.HashMap;


public class Specification {

	String spec_pv;
	int local_id;
	static int spec_pv_index = 0;
	String specification;
	int specification_type; // 0 - Simple, 1 - Complex
	//BooleanExpression booleanexp = new BooleanExpression();
//	SMTLibFormat smtlibformat =  new SMTLibFormat();
	
	/* Constructor */
	public Specification(String specification, int type) {
		this.local_id = spec_pv_index;
		this.spec_pv = "R" + ++spec_pv_index;
		this.specification = specification;
		this.specification_type = type;
	}
	
	/* Setters 
	public void setSMTLIBFormat(SMTLibFormat smtlibformat)
	{
		this.smtlibformat = smtlibformat;
	}*/
	/* Getters */
	public String getSpecId()
	{
		return  this.spec_pv;
	}
	public int getId() { return this.local_id;}
	
	public String getSpecification()
	{
		return this.specification;
	}
//	public BooleanExpression getBooleanExp()
//	{
//		return this.booleanexp;
//	}
//	public SMTLibFormat getSMTLIBFormat()
//	{
//		return this.smtlibformat;
//	}
	

	
}
