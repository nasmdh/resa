package org.verispec.resa.consistencycheck.handlers;

import java.util.HashMap;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Log;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.Version;
import com.microsoft.z3.Z3Exception;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;

public class Z3Dialog extends Dialog
{
	//private java.util.List<String> booleanexp_list;// = new ArrayList<String>();
	private Table specTbl;
	
	private java.util.List<Specification> specifications_list;
	private java.util.List<BooleanExpression> booleanexps_list;
	private java.util.List<SMTLibFormat> smtlibformat_list;
	private HashMap<String, java.util.List<String>> equivalent_booleanexps_hm;
	private java.util.List<SMTLibFormat> eqSmtlibformat_list;
	
	private HashMap <String, String> propositions_hm = new HashMap<String, String>();
	
	//private Z3Response response;
	private Table propositionsTbl;
	private Table semanticsTbl;
	private Table modelproofTbl;
	Group grpSolverOutput;
	Composite specinfo_composite;
	FormData fd_propositionTbl; 
	org.eclipse.swt.layout.FormData fd_lblPropostions;
	private Label lblSpecifications;
	Composite solver_composite;
	Label verdictLbl;
	private Text modelprooflbl;
	private Table unsatcoreTbl;
	int k = 0;
	private Text statisticsTxt;
	private Text modelproofTxt;
	private Text text;
	private FormData fd_specTbl;
	
	/* Option */
	Button exploringChk;
	Button trackingChk;
	
	/* Tabs */
	TabItem analysisResult;
	TabFolder tabFolder;
	
	private Label lblPropostions;
	private Table equivalentPropostionsTbl;
	private Label lblEquivalentClauses;
	private FormData fd_lblEquivalentClauses;
	private FormData fd_semanticsTbl;
	private FormData fd_lblSemantics;
	private FormData fd_equivalentPropostionsTbl;
	private FormData fd_lblSpecifications;
	private FormData fd_lblSemantics_1;
	private FormData fd_equivalentPropostionsTbl_1;
	
	// Selection flags
	private TableItem prev_propostion_item_selected;

	public Z3Dialog(Shell parentShell) {
        super(parentShell);
    }
	/**
	 * @wbp.parser.constructor
	 */
	public Z3Dialog(Shell parentShell, 
			java.util.List<Specification> specifications_list, 
			java.util.List<BooleanExpression> booleanexps_list, 
			java.util.List<SMTLibFormat> smtlibformat_list,
			HashMap<String, java.util.List<String>> equivalent_booleanexps_hm, 
			java.util.List<SMTLibFormat> eqSmtlibformat_list
			) {
        super(parentShell);
        
        //this.equivalent = transform.getEquivalentClauses();
        //this.specifications_hm = transform.specifications_hm;
        
        
        this.specifications_list = specifications_list;
        this.booleanexps_list = booleanexps_list;
        this.smtlibformat_list = smtlibformat_list;
        this.equivalent_booleanexps_hm = equivalent_booleanexps_hm;
        this.eqSmtlibformat_list = eqSmtlibformat_list;
        
        // Collect all propositions in one hash map for convenient use and possibly better performance
        for(BooleanExpression booleanexp:booleanexps_list)
        {
        	for(String pv:booleanexp.getPropositions().keySet())
        	{
        		this.propositions_hm.put(pv, booleanexp.getPropositions().get(pv));
        	}
        }
    }
	
    @Override
    protected Control createDialogArea(Composite parent) 
    {
    	
        Composite analysis_composite = (Composite) super.createDialogArea(parent);
        analysis_composite.setBounds(20, 20, 600, 380);
        analysis_composite.setLayout(new FillLayout(SWT.HORIZONTAL));
        

        tabFolder = new TabFolder(analysis_composite, SWT.NONE);
        TabItem parseResult = new TabItem(tabFolder, SWT.NONE);
        parseResult.setText("Parse Result");
        
        specinfo_composite = new Composite(tabFolder, SWT.NONE);
        parseResult.setControl(specinfo_composite);
       
        specinfo_composite.setLayout(new org.eclipse.swt.layout.FormLayout());

        /* 
         * Parsing Tab 
         */
        // Create a table for the specifications
        createSpecTable();
        insertDataSpecTable();
        
        // Create a table for propositions 
        createPropostionsTable();
        insertDataPropostionsTable();
        insertDataPropostionsTableWithTracking();
        
        // Create a table for equivalent propositions
        createEquivalentPropositionsTable();
        insertDataEquivalentPropositionsTable();
        
        // Create a table for equivalent propositions
//        createEqPropostionsTable();
//        insertDataEqPropositions();
        
        // Create Option group
        optionCreate();

        

        
        
        return parent;
    }

    private void insertDataEquivalentPropositionsTable() 
    {
    	insertDataEquivalentPropositionsTablex(propositionsTbl.getItem(0));
    	propositionsTbl.addListener(SWT.Selection, new Listener() {
         	
     	public void handleEvent (Event event) 
     	{
     		equivalentPropostionsTbl.removeAll();
     		if(prev_propostion_item_selected!=null)
     			prev_propostion_item_selected.setBackground(null);
     		
     		TableItem item = (TableItem) event.item;
     		prev_propostion_item_selected = item;
     		insertDataEquivalentPropositionsTablex(item);
     	}});	
	}
    private void insertDataEquivalentPropositionsTablex(TableItem item)
    {
    	equivalentPropostionsTbl.removeAll();
    	Boolean break_flag = false;
    	// Traverse the meaning hash map and 
    	// collect the equivalent propositions to the selected proposition from the propositions table
        for(String meaning:ResaToZ3SmtLibFormat.meaning_hm.keySet())
        {
        	for(String pv:ResaToZ3SmtLibFormat.meaning_hm.get(meaning))
        	{
        		// Find the set of equivalent propositions, where pv belongs to
        		if(item.getText().equals(pv))
        		{
        			// Highlight the cell
        			item.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
        			prev_propostion_item_selected = item;
        			// Insert all the equivalent propositions
        			for(String pvx:ResaToZ3SmtLibFormat.meaning_hm.get(meaning))
                	{System.out.println("pvx: " + pvx);
                			TableItem item1 = new TableItem(equivalentPropostionsTbl, SWT.NULL);
        	        		item1.setText(0, pvx);
        	        		if(pvx.contains("not"))
        	        		{
        	        			// Change something like (not p1) to p1 
        	        			pvx = pvx.replaceAll("[(]", "");
        	        			pvx = pvx.replaceAll("not", "");
        	        			pvx = pvx.replaceAll("[)]", "");
        	        			pvx = pvx.trim();
        	        			item1.setText(1, String.format("not (%s)", propositions_hm.get(pvx)));
        	        		}else
        	        			item1.setText(1, propositions_hm.get(pvx));
                	}
	        		break_flag = true;
	        		break;
        		}
        	}
        	if(break_flag)
        		break;
        }	
    }
	private void insertDataPropostionsTable() 
    {
    	propositionsTbl.removeAll();
        //find the specification object with the id item*
        for(Specification spec:specifications_list)
        {
    		int i = spec.getId();
    		HashMap<String, String> hm = new HashMap<String, String>();
    		hm = booleanexps_list.get(i).getPropositions();
    		
    		for(String key:hm.keySet())
    		{
    			 TableItem item1 = new TableItem(propositionsTbl, SWT.NULL);
    			 item1.setText(0, key);
    			 item1.setText(1, hm.get(key));
    		}
        }
	}
	private void insertDataVerdict(Z3Response response) 
    {
		if(response.status == Status.SATISFIABLE)
		{
			//change the color of the verdict labele
			verdictLbl.setText("SATISFIABLE");
			verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
			modelproofTxt.setText(response.getSolver().getModel().toString());
			System.out.println(response.getSolver().getModel().toString());
			//insertDataModelProofTable(response.getSolver().getModel());
		}else if(response.status == Status.UNSATISFIABLE)
		{
			verdictLbl.setText("UNSATISFIABLE");
			verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
			modelproofTxt.setText(response.getSolver().getProof().toString());
		}else if(response.status == Status.UNSATISFIABLE)
		{
			verdictLbl.setText("UNKNOWN");			
		}else
			verdictLbl.setText("OOPS, BUG!!");	
	}

	private void createVerdictLabel()
    {
        
        verdictLbl = new Label(solver_composite, SWT.CENTER);
        verdictLbl.setAlignment(SWT.CENTER);
        verdictLbl.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
        verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
        verdictLbl.setBounds(25, 10, 270, 41);
        verdictLbl.setText("VERDICT");
        
        modelprooflbl = new Text(solver_composite, SWT.NONE);
        modelprooflbl.setBounds(10, 63, 108, 15);
        
        unsatcoreTbl = new Table(solver_composite, SWT.BORDER | SWT.FULL_SELECTION);
        unsatcoreTbl.setBounds(324, 84, 352, 344);
        unsatcoreTbl.setHeaderVisible(true);
        unsatcoreTbl.setLinesVisible(true);
        
	    TableColumn col_id = new TableColumn(unsatcoreTbl, SWT.NONE);
        TableColumn col_unsatcore = new TableColumn(unsatcoreTbl, SWT.NONE);
        col_id.setResizable(false);
        col_id.setText("Label");
        col_id.setWidth(50);
        col_unsatcore.setText("Proposition");
        col_unsatcore.setWidth(250);
        col_unsatcore.setResizable(false);
        
        Label statTxt = new Label(solver_composite, SWT.NONE);
        statTxt.setBounds(10, 449, 55, 15);
        statTxt.setText("Statistics");
        
        Label lblUnsarCore = new Label(solver_composite, SWT.NONE);
        lblUnsarCore.setBounds(324, 63, 86, 15);
        lblUnsarCore.setText("Unsar Core");
        
        statisticsTxt = new Text(solver_composite, SWT.BORDER | SWT.WRAP);
        statisticsTxt.setEditable(false);
        statisticsTxt.setBounds(10, 470, 299, 212);
        
        modelproofTxt = new Text(solver_composite, SWT.BORDER | SWT.WRAP);
        modelproofTxt.setEditable(false);
        modelproofTxt.setBounds(10, 84, 299, 344);
        
        text = new Text(solver_composite, SWT.BORDER | SWT.WRAP);
        text.setEditable(false);
        text.setBounds(321, 470, 355, 212);
        

    }
    private void insertDataEqPropositions() 
    {
		for(String meaning:ResaToZ3SmtLibFormat.meaning_hm.keySet())
		{
			 TableItem item = new TableItem(semanticsTbl, SWT.NULL);
 			 item.setText(0, meaning);
 			 String pv_str="";
 			 for(String s:ResaToZ3SmtLibFormat.meaning_hm.get(meaning)) { pv_str = pv_str + s + ", ";}
 			 pv_str = pv_str.substring(0,pv_str.length()-2);
 			 item.setText(1, pv_str);
		}
	}
    
	public void insertDataPropostionsTableWithTracking()
    {
    	specTbl.addListener(SWT.Selection, new Listener() {
         	
         	public void handleEvent (Event event) 
         	{
         		if(exploringChk.getSelection())// only if the tracking check box is enabled,...
        		{
                 TableItem item = (TableItem) event.item;
                 
                 //int index = specTbl.indexOf (item);
                 propositionsTbl.removeAll();
                 System.out.println(item.getText());
                 //find the specification object with the id item*
                 for(Specification spec:specifications_list)
                 {
                 	if(item.getText().equals(spec.getSpecId()))
                 	{
                 		int i = spec.getId();
                 		System.out.println("index = " + i);
                 		HashMap<String, String> hm = new HashMap<String, String>();
                 		hm = booleanexps_list.get(i).getPropositions();
                 		System.out.println("found: " + item);
                 		for(String key:hm.keySet())
                 		{
                 			 TableItem item1 = new TableItem(propositionsTbl, SWT.NULL);
                 			 item1.setText(0, key);
                 			 item1.setText(1, hm.get(key));
                 		}
                 	}
                 }
        		}
                 
             }

           });

    }
	
    public void createPropostionsTable()
    {
        lblPropostions = new Label(specinfo_composite, SWT.NONE);
        fd_lblPropostions = new org.eclipse.swt.layout.FormData();
        fd_lblPropostions.left = new FormAttachment(0, 10);
        fd_lblPropostions.top = new FormAttachment(specTbl, 6);
        lblPropostions.setLayoutData(fd_lblPropostions);
        lblPropostions.setText("Propositions");
             
        propositionsTbl = new Table(specinfo_composite, SWT.BORDER | SWT.FULL_SELECTION);
        fd_propositionTbl = new FormData();
        fd_propositionTbl.bottom = new FormAttachment(100, -10);
        fd_propositionTbl.top = new FormAttachment(0, 355);
        fd_propositionTbl.left = new FormAttachment(0, 10);
        propositionsTbl.setLayoutData(fd_propositionTbl);
        propositionsTbl.setHeaderVisible(true);
        propositionsTbl.setLinesVisible(true);
           
        TableColumn col_pv = new TableColumn(propositionsTbl, SWT.NONE);
        TableColumn col_proposition = new TableColumn(propositionsTbl, SWT.NONE);
        col_proposition.setResizable(false);
        col_pv.setText("PV");
        col_pv.setWidth(50);
        col_proposition.setText("Proposition");
        col_proposition.setWidth(250);     
    }
    
    public void createSemanticTable()
    {
    	 semanticsTbl = new Table(specinfo_composite, SWT.BORDER | SWT.FULL_SELECTION);
         semanticsTbl.setHeaderVisible(true);
         semanticsTbl.setLinesVisible(true);
         
         TableColumn col_eqpv = new TableColumn(semanticsTbl, SWT.NONE);
         TableColumn col_eqproposition = new TableColumn(semanticsTbl, SWT.NONE);
         col_eqpv.setText("Semantic");
         col_eqpv.setWidth(200);
         col_eqproposition.setText("Propositions");
         col_eqproposition.setWidth(100);
    }
    
    public void optionCreate()
    {
        Group grpOption = new Group(specinfo_composite, SWT.NONE);
        grpOption.setText("Option");
        FormData fd_grpOption = new FormData();
        fd_grpOption.right = new FormAttachment(100, -10);
        fd_grpOption.left = new FormAttachment(specTbl, 6);
        fd_grpOption.bottom = new FormAttachment(specTbl, 0, SWT.BOTTOM);
        fd_grpOption.top = new FormAttachment(0, 35);
        grpOption.setLayoutData(fd_grpOption);
        
    	createExploringChk(grpOption);
    	createTrackingChk(grpOption);	
    }
    
    public void createEquivalentPropositionsTable()
    {
    	equivalentPropostionsTbl = new Table(specinfo_composite, SWT.BORDER | SWT.FULL_SELECTION);
    	fd_propositionTbl.right = new FormAttachment(equivalentPropostionsTbl, -6);
    	fd_equivalentPropostionsTbl_1 = new FormData();
    	fd_equivalentPropostionsTbl_1.bottom = new FormAttachment(100, -172);
    	fd_equivalentPropostionsTbl_1.top = new FormAttachment(specTbl, 30);
    	fd_equivalentPropostionsTbl_1.right = new FormAttachment(100, -10);
    	fd_equivalentPropostionsTbl_1.left = new FormAttachment(0, 348);
    	equivalentPropostionsTbl.setLayoutData(fd_equivalentPropostionsTbl_1);
    	equivalentPropostionsTbl.setHeaderVisible(true);
    	equivalentPropostionsTbl.setLinesVisible(true);

    	
        TableColumn col_pv = new TableColumn(equivalentPropostionsTbl, SWT.NONE);
        TableColumn col_proposition = new TableColumn(equivalentPropostionsTbl, SWT.NONE);
        col_pv.setText("PV");
        col_pv.setWidth(50);
        col_proposition.setText("Proposition");
        col_proposition.setWidth(250);  
        
    	
    	Label lblEquivalentProposition = new Label(specinfo_composite, SWT.NONE);
    	FormData fd_lblEquivalentProposition = new FormData();
    	fd_lblEquivalentProposition.top = new FormAttachment(specTbl, 6);
    	fd_lblEquivalentProposition.left = new FormAttachment(equivalentPropostionsTbl, 0, SWT.LEFT);
    	lblEquivalentProposition.setLayoutData(fd_lblEquivalentProposition);
    	lblEquivalentProposition.setText("Equivalent Proposition");
    }
    private void createTrackingChk(Group grpOption) 
    {
        trackingChk = new Button(grpOption, SWT.CHECK);
        trackingChk.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) 
        	{
        		if(trackingChk.getSelection())
        		{
        			trackingChk.setSelection(true);
        		}
        		else 
        		{
        			trackingChk.setSelection(false);
        			for(TableItem item:specTbl.getItems())
        			{
        				item.setBackground(null);
        			}
        		}
        	}
        });
        trackingChk.setBounds(10, 48, 93, 16);
        trackingChk.setText("Track");
	}
	private void createExploringChk(Group grpOption) 
    {
    	exploringChk = new Button(grpOption, SWT.CHECK);
        exploringChk.setBounds(10, 26, 72, 16);
        exploringChk.setText("Explore");
        exploringChk.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				if(exploringChk.getSelection())
        		{
        			exploringChk.setSelection(true);
        		}
        		else 
        		{
        			exploringChk.setSelection(false);
        			insertDataPropostionsTable();	
        		}
			}	
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	public void createSpecTable()
    {
        lblSpecifications = new Label(specinfo_composite, SWT.NONE);
        fd_lblSpecifications = new FormData();
        lblSpecifications.setLayoutData(fd_lblSpecifications);
        lblSpecifications.setText("Specifications");
        
        specTbl = new Table(specinfo_composite, SWT.BORDER | SWT.FULL_SELECTION);
        fd_lblSpecifications.bottom = new FormAttachment(specTbl, -6);
        fd_lblSpecifications.left = new FormAttachment(specTbl, 0, SWT.LEFT);
        //fd_lblPropostions.top = new FormAttachment(specTbl, 10);
        fd_specTbl = new FormData();
        fd_specTbl.top = new FormAttachment(0, 25);
        fd_specTbl.bottom = new FormAttachment(100, -367);
        fd_specTbl.left = new FormAttachment(0, 10);
        fd_specTbl.right = new FormAttachment(100, -108);
        specTbl.setLayoutData(fd_specTbl);
        specTbl.setHeaderVisible(true);
        specTbl.setLinesVisible(true);
        

        TableColumn specCol_pv = new TableColumn(specTbl, SWT.NONE);
        TableColumn specCol_spec = new TableColumn(specTbl, SWT.NONE);
        TableColumn specCol_boolexp = new TableColumn(specTbl, SWT.NONE);
        
        specCol_pv.setText("PV");
        specCol_pv.setWidth(50);
        specCol_spec.setText("Specification");
        specCol_spec.setWidth(400);
        specCol_boolexp.setText("Boolean Expression");
        specCol_boolexp.pack();
    }
    public void insertDataSpecTable()
    {
        
        // Insert the id and specifications
        for (int i = 0; i < specifications_list.size(); ++i) 
        {
          TableItem item = new TableItem(specTbl, SWT.NULL);
          item.setText(0, specifications_list.get(i).getSpecId());
          item.setText(1, specifications_list.get(i).getSpecification());
          item.setText(2, booleanexps_list.get(i).getBooleanExp());
        }
        
        
        
    }
    public void createModelProofTable()
    {
        Label lbl_proof = new Label(grpSolverOutput, SWT.NONE);
        lbl_proof.setBounds(10, 62, 162, 21);
        lbl_proof.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
        lbl_proof.setForeground(SWTResourceManager.getColor(0, 0, 128));
        lbl_proof.setBackground(SWTResourceManager.getColor(248, 248, 255));
        
        modelproofTbl = new Table(grpSolverOutput, SWT.BORDER | SWT.FULL_SELECTION);
        modelproofTbl.setBounds(10, 89, 326, 573);
        modelproofTbl.setHeaderVisible(true);
        modelproofTbl.setLinesVisible(true);
        TableColumn col_eqpv = new TableColumn(semanticsTbl, SWT.NONE);
        TableColumn col_eqproposition = new TableColumn(semanticsTbl, SWT.NONE);
        col_eqpv.setText("PV");
        col_eqpv.setWidth(50);
        col_eqproposition.setText("Proposition");
        col_eqproposition.setWidth(250);
    }
    
	public void createUnsatCoreTable()
	{
		unsatcoreTbl = new Table(grpSolverOutput, SWT.BORDER | SWT.FULL_SELECTION);
	    unsatcoreTbl.setBounds(342, 89, 324, 573);
	    unsatcoreTbl.setHeaderVisible(true);
	    unsatcoreTbl.setLinesVisible(true);
	    
	    Label lblNewLabel = new Label(grpSolverOutput, SWT.NONE);
	    lblNewLabel.setBounds(342, 62, 170, 21);
	    lblNewLabel.setText("Unsat Core");
	    
	    TableColumn col_id = new TableColumn(unsatcoreTbl, SWT.NONE);
        TableColumn col_unsatcore = new TableColumn(unsatcoreTbl, SWT.NONE);
        col_id.setResizable(false);
        col_id.setText("Label");
        col_id.setWidth(50);
        col_unsatcore.setText("Proposition");
       // col_unsatcore.setWidth(250);
      //  col_unsatcore.setResizable(false);
	}
    // overriding this methods allows you to set the
    // title of the custom dialog
    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("SAT-based Consistency Analysis");
    }

    @Override
    protected Point getInitialSize() {
        return new Point(700, 800);
    }
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
		// create only Cancel buttons by default
    	 // Change parent layout data to fill the whole bar
       // parent.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, false, false));

        // Update layout of the parent composite to count the spacer
        GridLayout layout = (GridLayout)parent.getLayout();
        layout.numColumns++;
        layout.makeColumnsEqualWidth = false;
        
        org.eclipse.swt.widgets.Button sampleButton = createButton(parent, IDialogConstants.NO_ID, "Check Consistency", false);
        sampleButton.addSelectionListener(new SelectionAdapter() {
        	private Z3Response response;

			@Override
        	public void widgetSelected(SelectionEvent e) {
        		// call the consistency check function

    	        
    	        /*
    	         * Analysis Tab
    	         */
    	        analysisResult = new TabItem(tabFolder, SWT.NONE);
    	        analysisResult.setText("Analysis Result");

    	       //tabFolder.getItem(1).setControl(parent);
    	        solver_composite = new Composite(tabFolder, SWT.NONE);
    	        analysisResult.setControl(solver_composite);
    	        solver_composite.setLayout(null);
       			
    	        //create a label for the verdict
    	        createVerdictLabel();
    	        tabFolder.setSelection(analysisResult);
    	        
        		this.response = checkConsistency(smtlibformat_list);
        		if(this.response != null)
        		{
        	   		//Report
        			System.out.print("Consistency checking: Success!!");
        			insertDataVerdict(this.response);
 
   			
        		}else
        			System.out.print("Consistency checking: Fail!!");
        		
        		
        	}
			
			
        });
        Button button = createButton(parent, IDialogConstants.CANCEL_ID, "Close", true);
        button.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		Specification.spec_pv_index = 0;
    			BooleanExpression.pv_index = 0;
    			ResaToZ3SmtLibFormat.meaning_hm.clear();
    			ResaToZ3SmtLibFormat.booleanexp_list.clear();
    			ResaToZ3SmtLibFormat.arith_booleanexps_hm.clear();
    			close();
        	}
        });	
	}
    
    private Z3Response checkConsistency(java.util.List<SMTLibFormat> smtlibformat_list) 
    {
				System.out.println("\n/* -------------- Parsing SMT-LIB string -------*/\n\n");

		try
        {
            com.microsoft.z3.Global.ToggleWarningMessages(true);
            Log.open("test.log");

            System.out.print("Z3 Major Version: ");
            System.out.println(Version.getMajor());
            System.out.print("Z3 Full Version: ");
            System.out.println(Version.getString());
            System.out.println("Z3 Full Version String: ");
            System.out.println(Version.getFullVersion());

           {
        	 //  Date before = new Date();
        	   HashMap<String, String> cfg = new HashMap<String, String>();
        	   cfg.put("proof", "true");
        	   cfg.put("model", "true");
        	   
        	   Context ctx = new Context(cfg); 	// Create a context
        	   Solver s = ctx.mkSolver();		// Create a solver
        	  
        	   // Generate SMT-LIB string
        	   String smtlibstring = "";
        	   for(SMTLibFormat smtlibformat:smtlibformat_list)
        	   {
        		   smtlibstring = smtlibstring + " " + smtlibformat.generateCommands(ctx, s);									
        	   }
        	   smtlibstring = String.format("(benchmark tst %s)", smtlibstring); 
        	   System.out.println("SMT-LIB string:\n" + smtlibstring);
        	   
        	   // Assert
        	   s = assertCommands(smtlibstring, ctx, s);
               Status st = s.check(); 	// Invoke the solver
               System.out.println("Status " + st);
              
               switch (st)
               {
               case SATISFIABLE:
            	   verdictLbl.setText("SATISFIABLE");
            	   verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
            	   modelproofTxt.setText(s.getModel().toString());
            	   modelprooflbl.setText("Model");
                   System.out.println("SATISFIABLE, model: " + s.getModel().toString());
                   break;
               case UNSATISFIABLE:
            	   verdictLbl.setText("UNSATISFIABLE");
            	   verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
            	   modelproofTxt.setText(s.getProof().toString());
            	   modelprooflbl.setText("Proof");
            	   System.out.println("UNSATISFIABLE, proof: " + s.getProof().toString());
            	   System.out.println("UNSATISFIABLE, unsatcore:");
            	   unsatcoreTbl.removeAll();
            	   for (Expr c : s.getUnsatCore())
                   {
            		   System.out.println(c);
            		   TableItem item = new TableItem(unsatcoreTbl, SWT.NULL);
        	          item.setText(0, c.toString());
        	          item.setText(1, c.getSExpr());
                      
                   }
            	   trackUnsatCoreSpecs();// indicate the respected specifications on the specTbl
            	   break;
               case UNKNOWN:
                   System.out.println("UNKNOWN, because: " + s.getReasonUnknown());
                   verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
               }               
               
               System.out.println("\nStatistics:");
               String stat="";
               for(String str:s.getStatistics().getKeys())
               {
            	   System.out.println("Key: " + str);
            	   System.out.println("Value: " + s.getStatistics().get(str));
            	   stat = stat + "\n" + s.getStatistics().get(str);
               }
               
               statisticsTxt.setText(stat);
               
               return new Z3Response(st, s);
           
           }
        } catch (Z3Exception ex)
        {
            System.out.println("Z3 Managed Exception: " + ex.getMessage());
            System.out.println("Stack trace: ");
            ex.printStackTrace(System.out);
        } catch (Exception ex)
        {
            System.out.println("Unknown Exception: " + ex.getMessage());
            System.out.println("Stack trace: ");
            ex.printStackTrace(System.out);
        }
	
		
		return null;
				
	}
    @SuppressWarnings("serial")
    class TestFailedException extends Exception
    {
        public TestFailedException()
        {
            super("Check FAILED");
        }
    };
    
    public Solver assertCommands(String smtlibstring, Context ctx, Solver s)
	{
		
		/* Parse the SMT-LIB string */
        ctx.parseSMTLIBString(smtlibstring,
                null, null, null, null);
        
        /* Assert */
        for (FuncDecl decl : ctx.getSMTLIBDecls())
        { 
            System.out.println("Declaration: " + decl);
        }
        
       // System.out.println("assumptions = " + ctx.getSMTLIBAssumptions().length);
        for (BoolExpr f : ctx.getSMTLIBAssumptions())
        {
     	   //s.add(f);
        	String label = "R" + ++k;
     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
     	   	System.out.printf("Assumption: %s\tLabel: %s\n", f, label);
        }
       // System.out.println("formulas = " + ctx.getSMTLIBFormulas().length);
        for (BoolExpr f : ctx.getSMTLIBFormulas())
        {
     	   	String label = "R" + ++k;
     	  // s.add(f);   
     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
     	   System.out.printf("Formula: %s\tLabel: %s\n", f, label);
        }
        k = 0;
        return s;
	}
    
    void trackUnsatCoreSpecs()
    {

 		if(trackingChk.getSelection())// only if the tracking check box is enabled,...
		{
 			//for each item in the array
 			for(TableItem itemunsatspec:unsatcoreTbl.getItems()) 
 			{
 				// for each item in the specTbl
 		         for(TableItem itemspec:specTbl.getItems())
 		         {
 		        	if(itemspec.getText().equals(itemunsatspec.getText()))
 		        	{
 		        		System.out.println("Color it!!!");
 		        		itemspec.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));continue;
 		        	}
 		         }
			}
    	}       

    }
}
