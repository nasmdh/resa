package org.verispec.resa.consistencycheck.handlers;

public class MyStack
{
	private int maxSize;
	private String[] stackArray;
	private int[] types_stack_array;
	private int top;

	public static final int REQUIRMENT_TYPE = 0;
	public static final int CLAUSE_TYPE = 1;
	public static final int COMMA_TYPE = 2;
	public static final int SUBCONJUNCTIVE_TYPE = 3;
	public static final int COOCONJUNCTIVE_TYPE = 4;
	public static final int OTHER_TYPE = 5;
	public static final int SUBORDINATE_CLAUSE_TYPE = 6;
	public static final int COMPLEX_CLAUSE_TYPE = 7;
	public static final int COMPOUND_CLAUSE_TYPE = 8; 
	public static final int NESTED_COMPLEX_CLAUSE_TYPE = 9;
	public static final int REQUIREMENT_SPECIFICATION_TYPE = 10;
	
	public static final int LEFT_PARENTHESIS = 100; 
	public static final int RIGHT_PARENTHESIS = 101; 
	public static final int LEFT_BRACKET = 102; 
	public static final int RIGHT_BRACKET = 103; 
	public static final int DOT_TYPE = 104;
	
	boolean dontwait_flag = false;
	boolean dot_flag =  false;
	//boolean dot_flag = false;
	boolean right_bracket_flag = false;
	
	public MyStack(int s)
	{
		maxSize = s;
		stackArray = new String[maxSize];
		types_stack_array = new int[maxSize];
		top = -1;
	}

	public void push(String j, int type)
	{
		
		
		stackArray[++top] = j;
		types_stack_array[top] = type;
	}

	public String pop()
	{
		return stackArray[top--];
	}

	public String peek()
	{
		return stackArray[top];
	}
	public int getTop(){return this.top;}
	public boolean isEmpty()
	{
		return (top == -1);
	}

	public boolean isFull()
	{
		return (top == maxSize - 1);
	}

	public void flush()
	{
		top = -1;
	}

	public void conditionalPush(String element, int type)
	{
		this.printStack();
		System.out.println("\nelement = " + element + "\ttype = " + type);
		System.out.println("Dont wait flag = " + this.dontwait_flag);

		if (isEmpty())
			this.push(element, type);
		// subordinate clause
		else if (this.types_stack_array[top] == CLAUSE_TYPE && type == CLAUSE_TYPE)
		{
			//System.out.println("match: Subordinate conjunctive");
			String tmp = this.pop();//clause part of the subordinate
			String construct_subordinate_clause = this.pop() /* conjunctive */ + " " + tmp;

			//push the subordinate part of the complex clause
			this.push(construct_subordinate_clause, SUBORDINATE_CLAUSE_TYPE);
			
			//however, at this point, the new element type computation is unknown. The insertion depends on element coming after it.
			this.conditionalPush(element, type);
			

		}// subordinate clause
	  /*	else if (this.types_stack_array[top] == CLAUSE_TYPE && type == SUBCONJUNCTIVE_TYPE)
		{
			if(this.right_bracket_flag)
				this.push(element, type);
			else{
			String tmp = this.pop();//clause part of the subordinate
			String construct_subordinate_clause = this.pop()  + " " + tmp;
			
			//push the subordinate part of the complex clause
			this.push(construct_subordinate_clause, SUBORDINATE_CLAUSE_TYPE);
			//this.printStack();
			this.push(element, type);
			}
			

		}*/
		else if(this.types_stack_array[top] == SUBCONJUNCTIVE_TYPE && type == CLAUSE_TYPE)
		{
			if(this.dontwait_flag)
			{
				this.dontwait_flag = false;
				String construct_subordinate_clause = this.pop() + " " + element;
				this.conditionalPush(construct_subordinate_clause, SUBORDINATE_CLAUSE_TYPE);// e.g., (x1 while x2)--->(x1 (while x2))
			}else
				this.push(element,  type);
			
		}else if(this.types_stack_array[top] == CLAUSE_TYPE && type == SUBORDINATE_CLAUSE_TYPE)
		{
			String construct_complex_clause = "(" + element + " " + this.pop()  + " )";
			this.pop();// remove the right parenthesis
			String construct_clause  = construct_complex_clause;
			this.conditionalPush(construct_clause, CLAUSE_TYPE);
		}
		// compound clause
		else if(this.types_stack_array[top] == COOCONJUNCTIVE_TYPE && type == CLAUSE_TYPE  )
		{
			//System.out.println("match: compound clause");
			String compound_clause = "(" + this.pop() + " " + this.pop() + " " + element + ")";
			if(dontwait_flag)
			{
				this.pop();this.pop();// pop the parenthesis
				dontwait_flag = false;
			}
			element = compound_clause;
			type = CLAUSE_TYPE;
			
			this.conditionalPush(element, type);
		
		}/*else if(this.types_stack_array[top] == RIGHT_PARENTHESIS && (type == CLAUSE_TYPE || type == COMPOUND_CLAUSE_TYPE ))
		{
			this.pop();
			this.push(element, type);
			this.right_parenthesis_flag = false;
		}*/
		//End of a specification
		else if (type == DOT_TYPE)
		{
			this.dot_flag = true;
			
			int tp = this.types_stack_array[this.top];
			
			this.conditionalPush(this.pop(), tp);
		}
		
		//complex clause
		else if(this.types_stack_array[top] == SUBORDINATE_CLAUSE_TYPE && type == CLAUSE_TYPE)
		{
			if(this.dontwait_flag || this.dot_flag)
			{
				this.dontwait_flag = false;  
				String construct_complex_clause = "(" + this.pop() + " " + element + " )";
				String construct_clause  = construct_complex_clause;
				this.conditionalPush(construct_clause, type);
			}else
				this.push(element, type);
		}//Complex subordinate clause
		else if(this.types_stack_array[top] == REQUIRMENT_TYPE && type == CLAUSE_TYPE)
		{
			String z3booleanexp = String.format("( = %s %s )", this.pop(), element);
			this.push(z3booleanexp, REQUIREMENT_SPECIFICATION_TYPE);
			this.printStack();
		}
		else if(this.types_stack_array[top] == RIGHT_BRACKET && type == COMPLEX_CLAUSE_TYPE)
		{			
			this.pop();
			this.push(element, type);
			this.right_bracket_flag = false;
		}else
			this.push(element, type);

		
	}

	public void printStack()
	{
		System.out.println("\nStack:");
		for (int i = 0; i <= top; ++i)
		{
			int type = this.types_stack_array[i];
			String value = this.stackArray[i];
			System.out.print(value + ":" + type);
			System.out.print(" ");
		}
		System.out.println("");
	}
}