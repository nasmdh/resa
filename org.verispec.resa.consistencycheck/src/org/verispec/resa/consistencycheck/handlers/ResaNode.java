package org.verispec.resa.consistencycheck.handlers;
import java.util.ArrayList;
import java.util.List;

public class ResaNode<T> {
    private List<ResaNode<T>> children = new ArrayList<ResaNode<T>>();
    private ResaNode<T> parent = null;
    private T data = null;

    public ResaNode(T data) {
        this.data = data;
    }

    public ResaNode(T data, ResaNode<T> parent) {
        this.data = data;
        this.parent = parent;
    }

    public List<ResaNode<T>> getChildren() {
        return children;
    }

    public void setParent(ResaNode<T> parent) {
        //parent.addChildNode(this);
        
    	this.parent = parent;
        
        
    }

    public void addChild(T data) {
        ResaNode<T> child = new ResaNode<T>(data);
        child.parent = this;
        this.children.add(child);
        
        //this.children.add(child);
    }

    public void addChildNode(ResaNode<T> child) {
        child.setParent(this);
        this.children.add(child);
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        if(this.children.size() == 0) 
            return true;
        else 
            return false;
    }

    public void removeParent() {
        this.parent = null;
    }
}