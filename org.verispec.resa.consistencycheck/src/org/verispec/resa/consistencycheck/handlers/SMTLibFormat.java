package org.verispec.resa.consistencycheck.handlers;

import java.util.ArrayList;
import java.util.List;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Solver;

public class SMTLibFormat
{
	String formula = "";
	String assumption = "";
	List<String> declarations_list = new ArrayList<>();
	int type;// assumptioin, formula
	
	int k = 0;
	
	public SMTLibFormat() 
	{
		
	}
	
	/* Setters */
	public void setDeclaration(String decl)
	{
		this.declarations_list.add(decl);
	}
	public void setAssumption(String assumption)
	{
		this.assumption = assumption;
	}
	public void setFormula(String formula)
	{
		this.formula = formula;
	}
	/* Getters */
	public List<String> getDeclarations()
	{
		return this.declarations_list;
	}
	public String getAssumption()
	{
		return this.assumption;
	}
	public String getFormula()
	{
		return this.formula;
	}
	/* Generators */
	public static String generateFormula(String booleaxp)
	{
		return String.format(":formula %s", booleaxp);
	}
	
	public static String generateAssumption(String booleaxp)
	{
		return String.format(":assumption %s", booleaxp);
	}
		
	public static String generateDeclaration(String pv, String type)
	{
		switch(type)
		{
		case "Bool":
			return String.format("(%s Bool)", pv);
		case "Int" :
			return String.format("(%s Int)", pv);
		}
		return null;
	}
	
	public String generateCommands(Context ctx, Solver s) 
	{
		/* Construct SMT-LIB string */
		String smtlibstring ="";
		String decls = "";// = ":extrafuns (" + boolean_declarations + ")";;

		// Declarations
		for (String decl: this.declarations_list) 
		{
			decls = decls + " " + decl;
		}
		decls = String.format(":extrafuns (%s)", decls);
		// Formula or Assumption
		smtlibstring = String.format(""
				+ "%s "
				+ "%s "
				+ "%s ", decls, this.assumption, this.formula);
		
		//System.out.println("SMT-LIB string:\n" + smtlibstring);
		
		return smtlibstring;
	}
	
//	public void assertCommands(String smtlibstring, Context ctx, Solver s)
//	{
//		
//		/* Parse the SMT-LIB string */
//        ctx.parseSMTLIBString(smtlibstring,
//                null, null, null, null);
//        
//        /* Assert */
//        for (FuncDecl decl : ctx.getSMTLIBDecls())
//        { 
//            System.out.println("Declaration: " + decl);
//        }
//        
//       // System.out.println("assumptions = " + ctx.getSMTLIBAssumptions().length);
//        for (BoolExpr f : ctx.getSMTLIBAssumptions())
//        {
//     	  // s.add(f);
//        	String label = "R" + ++k;
//     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
//     	   	System.out.println("Assumption: " + f.getString());
//        }
//       // System.out.println("formulas = " + ctx.getSMTLIBFormulas().length);
//        for (BoolExpr f : ctx.getSMTLIBFormulas())
//        {
//     	   	String label = "R" + ++k;
//     	 //  s.add(f);   
//     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
//            System.out.println("Formula: " + f.getString());
//        }
//	}

}
