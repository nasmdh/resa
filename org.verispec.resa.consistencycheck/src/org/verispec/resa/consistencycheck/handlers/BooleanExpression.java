package org.verispec.resa.consistencycheck.handlers;

import java.util.HashMap;

public class BooleanExpression {

	static int count = 0;
	static int pv_index = 0;
	private String  pv;
	private String booleanexp;
	private HashMap<String, String> propositions_hm = new HashMap<String, String>();
	private HashMap<String, String> propositions_type_hm = new HashMap<String, String>();
	private int booleanexp_type;
	
	public static final int ASSUMPTION = 300;
	public static final int FORMULA = 301;
	
	public BooleanExpression(int type) {
		++count;
		booleanexp_type = type;
	}
	
	/* Setters */
	public String addPropostion(String value, String type)
	{
		this.pv = "p" + ++pv_index;
		this.propositions_hm.put(pv, value);
		this.propositions_type_hm.put(pv, type);
		return this.pv;
	}
	public String addPropostion(String pv, String value, String type)
	{
		this.pv = pv;
		this.propositions_hm.put(pv, value);
		this.propositions_type_hm.put(pv, type);
		
		return this.pv;
	}
	public String getPV() { return this.pv; }
	public void setBooleaExp(String booleanexp)
	{
		this.booleanexp = booleanexp;
	}
	public void setType(int type)
	{
		this.booleanexp_type = type;
	}
	
	/* Getters */
	public HashMap<String, String> getPropositions()
	{
		return this.propositions_hm;
	}
	public HashMap<String, String> getPropositionsType()
	{
		return this.propositions_type_hm;
	}
	public String getProposition(String pv)
	{
		return this.propositions_hm.get(pv);
	}
	public String getBooleanExp()
	{
		return this.booleanexp;
	}
	public SMTLibFormat generateSMTLIBFormat()
	{
		// Create the object
		SMTLibFormat obj = new SMTLibFormat();
		String command = null;
		
		// Create the declaration
		for(String pv:this.propositions_hm.keySet())
		{
			command = SMTLibFormat.generateDeclaration(pv, this.propositions_type_hm.get(pv));
			obj.setDeclaration(command);
		}
		if(booleanexp_type == ASSUMPTION)
		{
			//create the formula
			command = SMTLibFormat.generateAssumption(this.booleanexp);
			System.out.println(command);
			obj.setAssumption(command);
		}else if(booleanexp_type == FORMULA)
		{
			//create the formula
			command = SMTLibFormat.generateFormula(this.booleanexp);
			obj.setFormula(command);
		}

		
		//return the object
		return obj;
	}
}
