package org.verispec.resa.consistencycheck.handlers;

import java.util.ArrayList;
import java.util.List;

public class Proposition {

	String pv;
	String proposition;
	List<String> equivalent_pvs_list = new ArrayList<String>();
	Proposition ref = null;// if null, i) has equivalent pvs, ii) has no equivalent pvs
							// if !null, it is equivalent to the referenced pv 
	
	public Proposition(String pv, String proposition) 
	{
		this.pv = pv;
		this.proposition = proposition;
	}
	
	public String getPV()
	{
		return this.getPV();
	}
	
	public String getProposition()
	{
		return this.getProposition();
	}
	
	public List<String> getEquivalentPV()
	{
		return this.equivalent_pvs_list;
	}
}
