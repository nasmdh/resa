package org.verispec.resa.consistencycheck.handlers;

import java.io.InputStream;
import java.util.HashMap;

import org.eclipse.swt.*;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.FuncDecl;
import com.microsoft.z3.Log;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.Version;
import com.microsoft.z3.Z3Exception;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import org.eclipse.swt.layout.FormData;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.widgets.Button;

import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;


public class GUI
{
	
	private java.util.List<Specification> specifications_list;
	private java.util.List<BooleanExpression> booleanexps_list;
	private java.util.List<SMTLibFormat> smtlibformat_list;
	private HashMap<String, java.util.List<String>> equivalent_booleanexps_hm;
	private java.util.List<SMTLibFormat> eqSmtlibformat_list;
	
	private HashMap <String, String> propositions_hm = new HashMap<String, String>();
	Display display;
	//private Z3Response response;
	private Table propositionsTbl;
	private Table semanticsTbl;;
	Group grpSolverOutput;
	FormData fd_propositionTbl; 
	org.eclipse.swt.layout.FormData fd_lblPropostions;
	Composite solver_composite;
	HashMap<String, String> spec_booleanexp_hm = new HashMap<String, String>();


	int k = 0;
	int j = 0;

	
	/* Option */
	Button exploreChk;
	Button unsatcoreChk;
	
	/* Tabs */
	TabItem analysisResult;
	TabFolder tabFolder;
	

	// Selection flags
	private TableItem prev_propostion_item_selected;



	private Shell shlResasatbasedConsistencyChecking;
	private Composite resaComposite;


	private Composite mainBar;

	private Group grpSpecifications;
	private Table specificationsTbl;
	private Group grpProposition;
	private Group grpAnalysis;
	private Button arithChk;
	private Table eqPropositionsTbl;

	private Button rightarrowBtn;
	private Table analysisTbl;
	private Label verdictLbl;
	private Composite bottomBar;
	private Button checkConsistencyBtn;
	private Button Close;
	private Composite verdict;
	private Text statTxt;
	private Z3Response r;
	/**
	 * @wbp.parser.constructor
	 */
	public GUI( 
			java.util.List<Specification> specifications_list, 
			java.util.List<BooleanExpression> booleanexps_list, 
			java.util.List<SMTLibFormat> smtlibformat_list,
			HashMap<String, java.util.List<String>> equivalent_booleanexps_hm, 
			java.util.List<SMTLibFormat> eqSmtlibformat_list
			) {
    
        
        //this.equivalent = transform.getEquivalentClauses();
        //this.specifications_hm = transform.specifications_hm;
        
        
        this.specifications_list = specifications_list;
        this.booleanexps_list = booleanexps_list;
        this.smtlibformat_list = smtlibformat_list;
        this.equivalent_booleanexps_hm = equivalent_booleanexps_hm;
        this.eqSmtlibformat_list = eqSmtlibformat_list;
        
        // Collect all propositions in one hash map for convenient use and possibly better performance
        for(BooleanExpression booleanexp:booleanexps_list)
        {
        	for(String pv:booleanexp.getPropositions().keySet())
        	{
        		this.propositions_hm.put(pv, booleanexp.getPropositions().get(pv));
        	}
        }
        
        
    }
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void createGUI()
	{

        display = new Display();

        shlResasatbasedConsistencyChecking = new Shell(display);
        shlResasatbasedConsistencyChecking.setSize(700, 700);
        shlResasatbasedConsistencyChecking.setText("ReSA/SAT-based Consistency Checking");
//        shell.setMinimumSize(new Point(700, 800));
//        shell.setBounds(0, 0, 700, 800);

     // handle SWT close
//        shell.addListener(SWT.Close, new Listener() {
//            public void handleEvent(Event event) {
//            	resetStatic();
//            	event.doit = false;
//              
//            }
//          });
        
        createGUIArea(shlResasatbasedConsistencyChecking);
        new Label(shlResasatbasedConsistencyChecking, SWT.NONE);
        
        bottomBar = new Composite(shlResasatbasedConsistencyChecking, SWT.NONE);
        bottomBar.setLayout(new FillLayout(SWT.HORIZONTAL));
        GridData gd_bottomBar = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
        gd_bottomBar.heightHint = 27;
        gd_bottomBar.widthHint = 241;
        bottomBar.setLayoutData(gd_bottomBar);
        checkConsistencyBtn = new Button(bottomBar, SWT.NONE);
        checkConsistencyBtn.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) 
        	{		
        		unsatcoreChk.setEnabled(true);
        		spec_booleanexp_hm.clear();
        		k = j = 0;
            	// Call the solver with the SMT-LIB format
            	r = checkConsistency(smtlibformat_list);
            	generateFeedbackData(r);
            	
            	
        	}
        });
        checkConsistencyBtn.setText("Check Consistency");
        
        Close = new Button(bottomBar, SWT.NONE);
        Close.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) 
        	{
        		resetStatic();
        		k = 0;
        		
        		shlResasatbasedConsistencyChecking.close();
        	}

			
        });
        Close.setText("Close");

        shlResasatbasedConsistencyChecking.open();
        while (!shlResasatbasedConsistencyChecking.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        resetStatic();
        display.dispose();
	}

    public void createGUIArea(Shell shell) 
    {
        shell.setLayout(new GridLayout(1, false));
        
        resaComposite = new Composite(shell, SWT.NONE);
        GridData gd_resaComposite = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_resaComposite.widthHint = 680;
        gd_resaComposite.heightHint = 599;
        resaComposite.setLayoutData(gd_resaComposite);

    	// Create main part
    	createMain();
    	
    	// Create the buttom part
    	createButtomPart();
    }


	private void createMain() 
	{
    	resaComposite.setLayout(new GridLayout(1, false));
    	mainBar = new Composite(resaComposite, SWT.NONE);
    	GridData gd_mainBar = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
    	gd_mainBar.widthHint = 669;
    	gd_mainBar.heightHint = 586;
    	mainBar.setLayoutData(gd_mainBar);
    	mainBar.setLayout(new FillLayout(SWT.VERTICAL));
    	mainBar.setBackground(SWTResourceManager.getColor(SWT.COLOR_CYAN));
    	new Label(resaComposite, SWT.NONE);

    	
    	// Create specification subpart
    	createSpecificationSubpart();
    	
    	// Create the proposition part
    	createPropositionSubpart();
    	
    	// Create the analysis subpart
		createAnalysisSubpart();

    	

	}

	private void generateFeedbackData(Z3Response r) 
	{
        
        switch (r.status)
        {
        case SATISFIABLE:
     	   verdictLbl.setText("SATISFIABLE");
     	   verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
     	  analysisTbl.removeAll();
     	  analysisTbl.getColumn(1).setText("Interpretation");
     	  grpAnalysis.setText("Analysis: Model");
     	  for (FuncDecl f:r.solver.getModel().getDecls()) 
     	  {
     		 if(f.getName().toString().startsWith("R"))
   		   {
     		  TableItem item = new TableItem(analysisTbl, SWT.NULL);
 	          item.setText(0, f.getName().toString());
 	          item.setText(1, r.solver.getModel().getConstInterp(f).toString());
 	          
 	          System.out.printf("Assignment: %s = %s\n", f.getName(), r.solver.getModel().getConstInterp(f));
   		   }
     	  }
            System.out.println("SATISFIABLE, model: " + r.solver.getModel().toString());
            break;
        case UNSATISFIABLE:
     	   verdictLbl.setText("UNSATISFIABLE");
     	   verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
     	   System.out.println("UNSATISFIABLE, unsatcore:");
     	   analysisTbl.removeAll();
      	  analysisTbl.getColumn(1).setText("Expression");
      	  grpAnalysis.setText("Analysis: Model");
     	   for (Expr c : r.solver.getUnsatCore())
            {
     		   System.out.printf("id: %s\n", c.getId());
     		   if(c.toString().startsWith("R"))
     		   {
	     		   System.out.println(c);
	     		   TableItem item = new TableItem(analysisTbl, SWT.NULL);
	 	          item.setText(0, c.toString());
	 	          item.setText(1, spec_booleanexp_hm.get(c.toString()));
     		   }
               
            }
     	  unsatCore();// indicate the respected specifications on the specTbl
     	   break;
        case UNKNOWN:
            System.out.println("UNKNOWN, because: " + r.solver.getReasonUnknown());
            verdictLbl.setBackground(null);
        }               
        
        System.out.println("\nStatistics:");
        String stat="";
        for(String str:r.solver.getStatistics().getKeys())
        {
     	   System.out.println("Key: " + str);
     	   System.out.println("Value: " + r.solver.getStatistics().get(str));
     	   stat = stat + "\n" + r.solver.getStatistics().get(str);
        }
        
        statTxt.setText(stat);
	}

	private void createAnalysisSubpart() 
	{
		grpAnalysis = new Group(mainBar, SWT.NONE);
    	grpAnalysis.setText("Analysis");

    	
    	// Create a table for the result data from the solver
    	createUnsatCoreTable();

	}


	private void createPropositionSubpart() 
	{
		grpProposition = new Group(mainBar, SWT.NONE);
    	grpProposition.setText("Propositions");

    	// Create a table for propositions
    	createPropositionsTable();
    	insertPropositions(); // Insert data
    	
    	// Create a table equivalent propositions
    	createEquivalentPropositionsTable();
    	insertEquivalentPropositions(); // Insert data
		
	}

	private void createEquivalentPropositionsTable() 
	{  	
    	eqPropositionsTbl = new Table(grpProposition, SWT.BORDER | SWT.FULL_SELECTION);
    	eqPropositionsTbl.setBounds(365, 20, 299, 165);
    	eqPropositionsTbl.setHeaderVisible(true);
    	eqPropositionsTbl.setLinesVisible(true);
    	
    	TableColumn col_pv = new TableColumn(eqPropositionsTbl, SWT.NONE);
        TableColumn col_proposition = new TableColumn(eqPropositionsTbl, SWT.NONE);
        col_proposition.setResizable(false);
        col_pv.setText("PV");
        col_pv.setWidth(50);
        col_proposition.setText("Equivalent Proposition");
        col_proposition.setWidth(250);    
        
        rightarrowBtn = new Button(grpProposition, SWT.NONE|SWT.ARROW|SWT.RIGHT);
        rightarrowBtn.setText("Equivalent to");
        rightarrowBtn.setEnabled(false);
        rightarrowBtn.setBounds(330, 97, 29, 38);
        
		
	}

	private void createPropositionsTable() 
	{
    	grpProposition.setLayout(null);
    	propositionsTbl = new Table(grpProposition, SWT.BORDER | SWT.FULL_SELECTION);
    	propositionsTbl.setBounds(10, 20, 314, 165);
    	propositionsTbl.setHeaderVisible(true);
    	propositionsTbl.setLinesVisible(true);	
	
          
       TableColumn col_pv = new TableColumn(propositionsTbl, SWT.NONE);
       TableColumn col_proposition = new TableColumn(propositionsTbl, SWT.NONE);
       col_proposition.setResizable(false);
       col_pv.setText("PV");
       col_pv.setWidth(50);
       col_proposition.setText("Proposition");
       col_proposition.setWidth(250);     
	}

	private void createSpecificationSubpart() 
	{
    	grpSpecifications = new Group(mainBar, SWT.NONE);
    	grpSpecifications.setText("Specifications");
    	
    	// Create a table for the specifications
    	createSpecificationsTable(grpSpecifications);
    	// Insert data
    	insertSpecifications();
    	
    	// Options, check boxes
    	options();
    	
    	insertPropostionsTableWithTracking();
		
	}

	private void options() 
	{
		Button booleanChk = new Button(grpSpecifications, SWT.CHECK);
		booleanChk.setEnabled(false);
    	booleanChk.setBounds(578, 21, 93, 16);
    	booleanChk.setText("Boolean");
    	
    	arithChk = new Button(grpSpecifications, SWT.CHECK);
    	arithChk.setEnabled(false);
    	arithChk.setBounds(578, 43, 93, 16);
    	arithChk.setText("Arithmetic");
    	
    	exploreChk = new Button(grpSpecifications, SWT.CHECK);
    	exploreChk.addSelectionListener(new SelectionAdapter() {
    		@Override
    		public void widgetSelected(SelectionEvent e) 
    		{
    			if(exploreChk.getSelection())
        		{
        			exploreChk.setSelection(true);
        		}
        		else 
        		{
        			exploreChk.setSelection(false);
        			insertPropositions();	
        		}
    		}
    	});
    	exploreChk.setBounds(578, 65, 93, 16);
    	exploreChk.setText("Explore");
    	
    	unsatcoreChk = new Button(grpSpecifications, SWT.CHECK);
    	unsatcoreChk.setEnabled(false);
    	unsatcoreChk.addSelectionListener(new SelectionAdapter() {
    		@Override
    		public void widgetSelected(SelectionEvent e) 
    		{
    			if(unsatcoreChk.getSelection())
        		{
    				unsatcoreChk.setSelection(true);

    					if(r.status == Status.UNSATISFIABLE)
    					unsatCore();
    				
        		}
        		else 
        		{
        			unsatcoreChk.setSelection(false);
        			for(TableItem it:specificationsTbl.getItems())
        				it.setBackground(null);
        			
        				
        		}
    				
    		}
    	});
    	unsatcoreChk.setBounds(578, 87, 93, 16);
    	unsatcoreChk.setText("Unsatcore");
		
	}

	private void insertSpecifications() 
	{
		 // Insert the id and specifications
        for (int i = 0; i < specifications_list.size(); ++i) 
        {
          TableItem item = new TableItem(specificationsTbl, SWT.NULL);
          item.setText(0, specifications_list.get(i).getSpecId());
          item.setText(1, specifications_list.get(i).getSpecification());
          item.setText(2, booleanexps_list.get(i).getBooleanExp());
        }
		
	}
	private void insertPropositions() 
    {
    	propositionsTbl.removeAll();
        //find the specification object with the id item*
        for(Specification spec:specifications_list)
        {
    		int i = spec.getId();
    		HashMap<String, String> hm = new HashMap<String, String>();
    		hm = booleanexps_list.get(i).getPropositions();
    		
    		for(String key:hm.keySet())
    		{
    			 TableItem item1 = new TableItem(propositionsTbl, SWT.NULL);
    			 item1.setText(0, key);
    			 item1.setText(1, hm.get(key));
    		}
        }
	}
	

    private void insertEquivalentPropositions() 
    {
    	insertEquivalentPropositionsTablex(propositionsTbl.getItem(0));
    	propositionsTbl.addListener(SWT.Selection, new Listener() {
         	
     	public void handleEvent (Event event) 
     	{
     		eqPropositionsTbl.removeAll();
     		if(prev_propostion_item_selected!=null)
     			prev_propostion_item_selected.setBackground(null);
     		
     		TableItem item = (TableItem) event.item;
     		prev_propostion_item_selected = item;
     		insertEquivalentPropositionsTablex(item);
     	}});	
	}
    private void insertEquivalentPropositionsTablex(TableItem item)
    {
    	eqPropositionsTbl.removeAll();
    	Boolean break_flag = false;
    	// Traverse the meaning hash map and 
    	// collect the equivalent propositions to the selected proposition from the propositions table
        for(String meaning:ResaToZ3SmtLibFormat.meaning_hm.keySet())
        {
        	for(String pv:ResaToZ3SmtLibFormat.meaning_hm.get(meaning))
        	{
        		// Find the set of equivalent propositions, where pv belongs to
        		if(item.getText().equals(pv))
        		{
        			// Highlight the cell
        			item.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
        			prev_propostion_item_selected = item;
        			// Insert all the equivalent propositions
        			for(String pvx:ResaToZ3SmtLibFormat.meaning_hm.get(meaning))
                	{System.out.println("pvx: " + pvx);
                			TableItem item1 = new TableItem(eqPropositionsTbl, SWT.NULL);
        	        		item1.setText(0, pvx);
        	        		if(pvx.contains("not"))
        	        		{
        	        			// Change something like (not p1) to p1 
        	        			pvx = pvx.replaceAll("[(]", "");
        	        			pvx = pvx.replaceAll("not", "");
        	        			pvx = pvx.replaceAll("[)]", "");
        	        			pvx = pvx.trim();
        	        			item1.setText(1, String.format("not (%s)", propositions_hm.get(pvx)));
        	        		}else
        	        			item1.setText(1, propositions_hm.get(pvx));
                	}
	        		break_flag = true;
	        		break;
        		}
        	}
        	if(break_flag)
        		break;
        }	
    }
    
	private void createButtomPart() 
	{
		
	}

	private void createSpecificationsTable(Group grpSpecifications) 
    {
    	specificationsTbl = new Table(grpSpecifications, SWT.BORDER | SWT.FULL_SELECTION);
    	specificationsTbl.setBounds(10, 21, 547, 164);
    	specificationsTbl.setHeaderVisible(true);
    	specificationsTbl.setLinesVisible(true);

         TableColumn specCol_pv = new TableColumn(specificationsTbl, SWT.NONE);
         TableColumn specCol_spec = new TableColumn(specificationsTbl, SWT.NONE);
         TableColumn specCol_boolexp = new TableColumn(specificationsTbl, SWT.NONE);
         
         specCol_pv.setText("PV");
         specCol_pv.setWidth(50);
         specCol_spec.setText("Specification");
         specCol_spec.setWidth(350);
         specCol_boolexp.setText("Boolean Expression");
         specCol_boolexp.pack();	
	}

	private void insertPropostionsTableWithTracking()
    {
		specificationsTbl.addListener(SWT.Selection, new Listener() {
         	
         	public void handleEvent (Event event) 
         	{
         		if(exploreChk.getSelection())// only if the tracking check box is enabled,...
        		{
                 TableItem item = (TableItem) event.item;
                 
                 //int index = specTbl.indexOf (item);
                 propositionsTbl.removeAll();
                 System.out.println(item.getText());
                 //find the specification object with the id item*
                 for(Specification spec:specifications_list)
                 {
                 	if(item.getText().equals(spec.getSpecId()))
                 	{
                 		int i = spec.getId();
                 		System.out.println("index = " + i);
                 		HashMap<String, String> hm = new HashMap<String, String>();
                 		hm = booleanexps_list.get(i).getPropositions();
                 		System.out.println("found: " + item);
                 		for(String key:hm.keySet())
                 		{
                 			 TableItem item1 = new TableItem(propositionsTbl, SWT.NULL);
                 			 item1.setText(0, key);
                 			 item1.setText(1, hm.get(key));
                 		}
                 	}
                 }
        		}
                 
             }

           });

    }

	private void createSemanticTable(Composite parent)
    {
    	 semanticsTbl = new Table(parent, SWT.BORDER | SWT.FULL_SELECTION);
         semanticsTbl.setHeaderVisible(true);
         semanticsTbl.setLinesVisible(true);
         
         TableColumn col_eqpv = new TableColumn(semanticsTbl, SWT.NONE);
         TableColumn col_eqproposition = new TableColumn(semanticsTbl, SWT.NONE);
         col_eqpv.setText("Semantic");
         col_eqpv.setWidth(200);
         col_eqproposition.setText("Propositions");
         col_eqproposition.setWidth(100);
    }
    
	public void createUnsatCoreTable()
	{
    	grpAnalysis.setLayout(null);
    	
    	analysisTbl = new Table(grpAnalysis, SWT.BORDER | SWT.FULL_SELECTION);
    	analysisTbl.setBounds(10, 24, 331, 162);
    	analysisTbl.setHeaderVisible(true);
    	analysisTbl.setLinesVisible(true);
    	
    		    
        TableColumn col_pv = new TableColumn(analysisTbl, SWT.NONE);
        TableColumn col_proposition = new TableColumn(analysisTbl, SWT.NONE);
        col_proposition.setResizable(false);
        col_pv.setText("PV");
        col_pv.setWidth(50);
        col_proposition.setText("Expression");
        col_proposition.setWidth(250);  
    	
    	verdict = new Composite(grpAnalysis, SWT.NONE);
    	verdict.setBounds(347, 15, 317, 178);
    	verdict.setToolTipText("verdict");
    	verdict.setLayout(null);
    	
    	verdictLbl = new Label(verdict, SWT.CENTER);
    	verdictLbl.setBounds(45, 10, 255, 25);
    	verdictLbl.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
    	verdictLbl.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
    	verdictLbl.setText("");
    	
    	statTxt = new Text(verdict, SWT.WRAP);
    	statTxt.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));
    	statTxt.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_FOREGROUND));
    	statTxt.setEditable(false);
    	statTxt.setBounds(0, 55, 324, 113);
	}

	 void unsatCore()
	    {

	 		if(unsatcoreChk.getSelection())// only if the tracking check box is enabled,...
			{
	 			//for each item in the array
	 			for(TableItem itemunsatspec:analysisTbl.getItems()) 
	 			{
	 				// for each item in the specTbl
	 		         for(TableItem itemspec:specificationsTbl.getItems())
	 		         {
	 		        	if(itemspec.getText().equals(itemunsatspec.getText()))
	 		        	{
	 		        		System.out.println("Color it!!!");
	 		        		itemspec.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
	 		        		continue;
	 		        	}
	 		         }
				}
	    	}       

	    }
    private Z3Response checkConsistency(java.util.List<SMTLibFormat> smtlibformat_list) 
    {
				System.out.println("\n/* -------------- Parsing SMT-LIB string -------*/\n\n");

		try
        {
            com.microsoft.z3.Global.ToggleWarningMessages(true);
            Log.open("test.log");

            System.out.print("Z3 Major Version: ");
            System.out.println(Version.getMajor());
            System.out.print("Z3 Full Version: ");
            System.out.println(Version.getString());
            System.out.println("Z3 Full Version String: ");
            System.out.println(Version.getFullVersion());

           {
        	 //  Date before = new Date();
        	   HashMap<String, String> cfg = new HashMap<String, String>();
        	   cfg.put("proof", "true");
        	   cfg.put("model", "true");
        	   
        	   Context ctx = new Context(cfg); 	// Create a context
        	   Solver s = ctx.mkSolver();		// Create a solver
        	  
        	   // Generate SMT-LIB string
        	   String smtlibstring = "";
        	   for(SMTLibFormat smtlibformat:smtlibformat_list)
        	   {
        		   smtlibstring = smtlibstring + " " + smtlibformat.generateCommands(ctx, s);									
        	   }
        	   smtlibstring = String.format("(benchmark tst %s)", smtlibstring); 
        	   System.out.println("SMT-LIB string:\n" + smtlibstring);
        	   
        	   // Assert
        	   s = assertCommands(smtlibstring, ctx, s);
               Status st = s.check(); 	// Invoke the solver
               System.out.println("Status " + st);
               
               return new Z3Response(st, s);
           
           }
        } catch (Z3Exception ex)
        {
            System.out.println("Z3 Managed Exception: " + ex.getMessage());
            System.out.println("Stack trace: ");
            ex.printStackTrace(System.out);
        } catch (Exception ex)
        {
            System.out.println("Unknown Exception: " + ex.getMessage());
            System.out.println("Stack trace: ");
            ex.printStackTrace(System.out);
        }
	
		
		return null;
				
	}
    @SuppressWarnings("serial")
    class TestFailedException extends Exception
    {
        public TestFailedException()
        {
            super("Check FAILED");
        }
    };
    
    public Solver assertCommands(String smtlibstring, Context ctx, Solver s)
	{
		
		/* Parse the SMT-LIB string */
        ctx.parseSMTLIBString(smtlibstring,
                null, null, null, null);
        
        /* Assert */
        for (FuncDecl decl : ctx.getSMTLIBDecls())
        { 
            System.out.println("Declaration: " + decl);
        }
        
       // System.out.println("assumptions = " + ctx.getSMTLIBAssumptions().length);
        for (BoolExpr f : ctx.getSMTLIBAssumptions())
        {
     	   //s.add(f);
        	String label = "E" + ++j;
        	spec_booleanexp_hm.put(label, f.toString());
     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
     	   	System.out.printf("Assumption: %s\tLabel: %s\n", f, label);
        }
       // System.out.println("formulas = " + ctx.getSMTLIBFormulas().length);
        for (BoolExpr f : ctx.getSMTLIBFormulas())
        {
     	   	String label = "R" + ++k;
     	   spec_booleanexp_hm.put(label, f.toString());
     	  // s.add(f);   
     	   	s.assertAndTrack(f, ctx.mkBoolConst(label));
     	   System.out.printf("Formula: %s\tLabel: %s\n", f, label);
        }
        k = 0;
        j = 0;
        return s;
	}
    
    void trackUnsatCoreSpecs(Table table)
    {

 		if(unsatcoreChk.getSelection())// only if the tracking check box is enabled,...
		{
 			//for each item in the array
 			for(TableItem itemunsatspec:analysisTbl.getItems()) 
 			{
 				// for each item in the specTbl
 		         for(TableItem itemspec:table.getItems())
 		         {
 		        	if(itemspec.getText().equals(itemunsatspec.getText()))
 		        	{
 		        		System.out.println("Color it!!!");
 		        		itemspec.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));continue;
 		        	}
 		         }
			}
    	}       

    }
    public void resetStatic() 
	{
		ResaToZ3SmtLibFormat.arith_booleanexps_hm.clear();
		ResaToZ3SmtLibFormat.booleanexp_list.clear();
		ResaToZ3SmtLibFormat.meaning_hm.clear();
		
		BooleanExpression.count = 0;
		BooleanExpression.pv_index = 0;
		Specification.spec_pv_index = 0;
		
	}
}
