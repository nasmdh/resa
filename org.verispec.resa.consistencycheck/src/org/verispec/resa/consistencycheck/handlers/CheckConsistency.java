package org.verispec.resa.consistencycheck.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;



/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CheckConsistency extends AbstractHandler {

	String solver_message;
	String clauses_assignments;
	List<String> booleanexps_list;
	int k = 0;
	
	/**
	 * The constructor.
	 */
	public CheckConsistency() {
	}

    @SuppressWarnings("serial")
    class TestFailedException extends Exception
    {
        public TestFailedException()
        {
            super("Check FAILED");
        }
    };
    
	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException 
	{
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
       	System.out.println(System.getProperty("java.class.path"));

		ResaToZ3SmtLibFormat transform = new ResaToZ3SmtLibFormat();
		
		/* Transformation: ReSA to Boolean Expression */
		transform.resaToBoolean();
		// Get the boolean expressions
		List<BooleanExpression> booleanexps_list = ResaToZ3SmtLibFormat.booleanexp_list; //transform.getBooleanExpressions();
		System.out.println("\n\nSpecifications: After transformation to Boolean Expression");
		for (BooleanExpression booleanexp : booleanexps_list) 
		{
			for(String pv:booleanexp.getPropositions().keySet())
			{
				System.out.printf("Pv: %s\tProposition: %s\n", pv, booleanexp.getProposition(pv));
			}
			System.out.printf("Boolean Expression: %s\n", booleanexp.getBooleanExp());
		}
		// Get the arithmetic boolean expressions
		HashMap<String, BooleanExpression> arith_booleanexps_hm = ResaToZ3SmtLibFormat.arith_booleanexps_hm; 
		for (String pv : arith_booleanexps_hm.keySet()) 
		{
			for(String var:arith_booleanexps_hm.get(pv).getPropositions().keySet())
			{
				System.out.printf("Pv: %s\tType:%s\n", var, 
						arith_booleanexps_hm.get(pv).getPropositionsType().get(var));
			}
			System.out.printf("Pv: %s\tBoolean Expression: %s\n", pv, arith_booleanexps_hm.get(pv).getBooleanExp());
		}
		/* Check equivalence of propositions */
		HashMap<String, List<String>> equivalent_propostions_hm = transform.checkEquivalenceOfPropostions();

		/* Transformation: Boolean Expression to SMT-LIB format*/
		// The boolean expressions
		List<SMTLibFormat> smtlibformat_list = transform.booleanToZ3(booleanexps_list);
		// The arithmetic boolean expressions
		smtlibformat_list.addAll(transform.booleanToZ3(arith_booleanexps_hm));
		System.out.println("\n\nSMTLIB format: After transformation to SMT-LIB format");
		for (SMTLibFormat smtLibFormat : smtlibformat_list) 
		{
			for(String decl:smtLibFormat.getDeclarations())
			{
				System.out.printf("Declaration: %s\n", decl);
			}
			System.out.printf("Assertion (Assumption): %s\n", smtLibFormat.getAssumption());
			System.out.printf("Assertion (Formula): %s\n", smtLibFormat.getFormula());
		}
		
		// Equivalent propositions as assumptions
	//	List<SMTLibFormat> eqSmtlibformat_list = new ArrayList<SMTLibFormat>();
		if(!equivalent_propostions_hm.isEmpty())
		{
			//List<SMTLibFormat> eqSmtlibformat_list = new ArrayList<SMTLibFormat>();
			List<String> equivalent_booleanexpressions_list = transform.equiavlentBooleanExpressions();
			for (String eqbooleanexp : equivalent_booleanexpressions_list) 
			{
				SMTLibFormat tmp = new SMTLibFormat();
				String tmpstr = SMTLibFormat.generateAssumption(eqbooleanexp);
				tmp.setAssumption(tmpstr);
				smtlibformat_list.add(tmp);
				
				System.out.printf("Assertion (Equivalent Assumption): %s\n", tmp.getAssumption());
			}
		}
		
		   new Thread(new Runnable() {
			      public void run() {
			    	  GUI gui = new GUI(
			  				transform.getSpecificationsList(), 
							booleanexps_list,
							smtlibformat_list,
							equivalent_propostions_hm,
							smtlibformat_list);
			    	  gui.createGUI();
//			         while (true) {
//			            try { Thread.sleep(1000); } catch (Exception e) { }
//			            Display.getDefault().asyncExec(new Runnable() {
//			               public void run() {
//			            	   GUI.createGui();
//			               }
//			            });
//			         }
			      }
			   }).start();
		   
		
		
//		// Dialog
//		Z3Dialog z3dilog = new Z3Dialog(window.getShell(), 
//				transform.getSpecificationsList(), 
//				booleanexps_list,
//				smtlibformat_list,
//				equivalent_propostions_hm,
//				smtlibformat_list
//				);
//		z3dilog.open();
		
		return null;
	}

	
}
