package org.verispec.resa.consistencycheck.handlers;

import com.microsoft.z3.Solver;

import com.microsoft.z3.Status;

public class Z3Response 
{
	Status status = null;
	Solver solver = null;
	
	public Z3Response(Status st, Solver s)
	{
		this.status = st;
		this.solver = s;
	}
	
	/* Getters */
	public Status getStatus(){ return this.status; }
	public Solver getSolver(){ return this.solver; }
}
