package org.verispec.resa.consistencycheck.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.verispec.resa.resa.Clause;
import org.verispec.resa.resa.Comparison;
import org.verispec.resa.resa.ComparisonOperator;
import org.verispec.resa.resa.ComplexSpec;
import org.verispec.resa.resa.LogicalOP;
import org.verispec.resa.resa.NP;
import org.verispec.resa.resa.SimpleSpec;
import org.verispec.resa.resa.SubConjCause;
import org.verispec.resa.resa.SubConjConditional;
import org.verispec.resa.resa.SubConjTemporal;
import org.verispec.resa.resa.impl.InParameterImpl;
import org.verispec.resa.resa.impl.OutParameterImpl;
import org.verispec.resa.resa.impl.ParameterImpl;
import org.verispec.resa.resa.impl.VariableImpl;;


public class ResaToZ3SmtLibFormat
{
	/* Structures to hold boolean constructs */
	List<Specification> specifications_list = new ArrayList<Specification>();
	static List<BooleanExpression> booleanexp_list = new ArrayList<BooleanExpression>();
	
	
	/* Arithmetic boolean expressions */
	static HashMap<String, BooleanExpression> arith_booleanexps_hm = new HashMap<String, BooleanExpression>();
	
	Specification specification_obj;
	BooleanExpression booleanexp_obj;
	SMTLibFormat smtlibformat_obj;// = new SMTLibFormat();
	BooleanExpression arith_booleanexp_obj;
	
	/* propositions */
	List<Proposition> eqvpropositions_list = new ArrayList<Proposition>();
	
	HashMap<String, String> specifications_hm = new HashMap<String, String>();
	int req_index = 0;
	int specification_type;
	
	
	HashMap<String, String> propositional_vars_hm = new HashMap<String, String>();// propositional symbols/variables
	List<String> propositional_formulas_list =  new ArrayList<String>(); // propositional formulas/boolean expressions
	static HashMap<String, List<String>> meaning_hm = new HashMap<String, List<String>>();// meaning
	String propositional_var = "";
	String propositional_formula = "";
	String clauses_assignments = "";
	String equivalent_clauses = "";
	
	int i = 0, j = 0, n = 0, l = 0, child = 0;
	String booleanexp = "";

	public static final int REQUIRMENT_TYPE = 0;
	public static final int CLAUSE_TYPE = 1;
	public static final int SUBCONJUNCTIVE_TYPE = 3;
	public static final int COOCONJUNCTIVE_TYPE = 4;
	public static final int SUBORDINATE_CLAUSE_TYPE = 6;
	public static final int COMPLEX_CLAUSE_TYPE = 7;
	public static final int NESTED_COMPLEX_CLAUSE_TYPE = 9;
	public static final int REQUIREMENT_SPECIFICATION_TYPE = 10;
	public static final int SIMPLE_SPECIFICATION_TYPE = 11;
	public static final int COMPLEX_SPECIFICATION_TYPE = 12;
	
	public static final int LEFT_PARENTHESIS = 100;
	public static final int RIGHT_PARENTHESIS = 101;
	public static final int LEFT_BRACKET = 102;
	public static final int RIGHT_BRACKET = 103;
	public static final int DOT_TYPE = 104;
	
	public static final int UNKNOWN_TYPE = 200;
	
	public static final int ASSUMPTION = 300;
	public static final int FORMULA = 301;
	
	String sub, obj, operator;
	
	public ResaToZ3SmtLibFormat()
	{
		// TODO Auto-generated constructor stub
	}
	
	public HashMap<String, BooleanExpression> getArithBooleanexp()
	{
		return arith_booleanexps_hm;
	}
	public List<Specification> getSpecificationsList()
	{
		return this.specifications_list;
	}
	public HashMap<String, String> getSpecifications()
	{
		return this.specifications_hm;
	}
	public List<BooleanExpression> getBooleanExpressions()
	{
		return booleanexp_list;
	}
//	public List<SMTLibFormat> getSMTLibFormat()
//	{
//		return this.smtlibformat_list;
//	}
	
	public String conver_BooleanExps_to_Z3_format()
	{

		return "boolean expressions converted to Z3 SMT-LIB format";
	}


	public static String generateZ3Assertion(String z3formula)
	{
		return String.format(":formula %s", z3formula);
		//return String.format("( assert %s )", z3formula);
	}
	
	public static String generateZ3Assumption(String z3formula)
	{
		return String.format(":assumption %s", z3formula);
		//return String.format("( assert %s )", z3formula);
	}
	
	public static String generateZ3Declaration(String symbol)
	{
		return String.format("(%s Bool)", symbol);
		//return String.format("(declare-const %s Bool)", symbol);
	}
	
	public String getClauses()
	{
		return this.clauses_assignments;
	}
	
	public String getEquivalentClauses()
	{
		return this.equivalent_clauses;
	}
	
	//boolean exps
	public List<BooleanExpression> getBooleanExps()
	{
		return this.booleanexp_list;
	}
	
	public List<SMTLibFormat> booleanToZ3(List<BooleanExpression> booleanexp_list)
	{
		List<SMTLibFormat> smtlibformat_list = new ArrayList<SMTLibFormat>();
		
		/* For each specification generate its equivalent SMT-LIB format */
		for(BooleanExpression booleanexp:booleanexp_list)
		{ 
			// generate SMT-LIB format specification from the boolean expression
			smtlibformat_obj = booleanexp.generateSMTLIBFormat();
			//System.out.println("formula: " + smtlibformat_obj.getFormula());
			//spec.setSMTLIBFormat(smtlibformat_obj);
			smtlibformat_list.add(smtlibformat_obj);
		}
		
		return smtlibformat_list;
	}
	public List<SMTLibFormat> booleanToZ3(HashMap<String, BooleanExpression> booleanexp_hm)
	{
		List<SMTLibFormat> smtlibformat_list = new ArrayList<SMTLibFormat>();
		
		/* For each specification generate its equivalent SMT-LIB format */
		for(String pv: booleanexp_hm.keySet())
		{ 
			// generate SMT-LIB format specification from the boolean expression
			smtlibformat_obj = booleanexp_hm.get(pv).generateSMTLIBFormat();
			//System.out.println("formula: " + smtlibformat_obj.getFormula());
			//spec.setSMTLIBFormat(smtlibformat_obj);
			smtlibformat_list.add(smtlibformat_obj);
		}
		
		return smtlibformat_list;
	}

	/* 
	 * Parse the specification semantic model to generate the boolean contracts
	 * 
	 * - boolean variables (represents the clauses)
	 * - interpretations of the boolean variables (clauses)
	 * - boolean formulas (complex clauses, compound clauses, complex specification, simple specification)
	 * 
	 * */
	public void resaToBoolean()
	{
		System.out.println("\n/* --------------- Parse Debug ------------- */\n");
		MyStack z3symbols_stack = new MyStack(50);

		
		IXtextDocument myDocument = EditorUtils.getActiveXtextEditor().getDocument();
		myDocument.readOnly(new IUnitOfWork<String, XtextResource>()
		{	
			private boolean first_time = true;

			@Override
			public String exec(XtextResource resource) throws Exception
			{
				if (resource == null || resource.getParseResult() == null)	
					return null;
				// Containers for boolean constructs

				 String clause = "";
				TreeIterator<Object> content_it = EcoreUtil.getAllProperContents(resource, false);
				while (content_it.hasNext())
				{
					EObject element = (EObject) content_it.next();
					
					if (element instanceof ComplexSpec || element instanceof SimpleSpec)
					{

						
						
						if (!this.first_time)
						{ 
							
							z3symbols_stack.dot_flag = true;
							
							z3symbols_stack.conditionalPush(".", DOT_TYPE);//  End of requirement
							propositional_formula = z3symbols_stack.pop();//ResaToZ3SmtLibFormat.generateZ3Assertion(z3symbols_stack.pop());
							
							propositional_formulas_list.add(propositional_formula); // add the boolean expression into the container
							System.out.println("boolean expression:" + propositional_formula);
							// add to specification object
							booleanexp_obj.setBooleaExp(propositional_formula);

							z3symbols_stack.flush();
							z3symbols_stack.dot_flag = false;
							
							booleanexp = "";
						}else
							this.first_time = false;
						
						// add the object to the specification hash map
						ICompositeNode cnode = NodeModelUtils.findActualNodeFor(element);
						String specification = "";
						for (INode node : cnode.getChildren())
						{
							// System.out.println(node.getText());
							if(node.getText().contains("/*") || node.getText().contains("//"))
								continue;
							
							specification = specification + node.getText();
							
						}

						
						//create a Specification object
						specification_obj = new Specification(specification, specification_type);
						//create a BooleanExpression object
						booleanexp_obj = new BooleanExpression(specification_type);
						
						// Add the objects to their respective lists
						specifications_list.add(specification_obj);
						booleanexp_list.add(booleanexp_obj);
						
						System.out.println("Specification : " + specification);
						specifications_hm.put("R" + ++req_index, specification);
						
						booleanexp = "true";// + ++i;
						
						
						if (element instanceof ComplexSpec)
						{
							propositional_vars_hm.put(booleanexp, "Complex");
							specification_type = FORMULA;//SIMPLE_SPECIFICATION_TYPE;
							booleanexp_obj.setType(FORMULA);
							
							
						}else 
						{
							propositional_vars_hm.put(booleanexp, "Simple");
							specification_type = FORMULA;//COMPLEX_SPECIFICATION_TYPE;
							booleanexp_obj.setType(FORMULA);
							
						}
						z3symbols_stack.conditionalPush(booleanexp, REQUIRMENT_TYPE);
						
					}
					else if (element instanceof Clause)
					{

						propositional_var = "p" + ++j;
						System.out.print(propositional_var + " ");
						z3symbols_stack.conditionalPush(propositional_var, CLAUSE_TYPE);

						booleanexp = booleanexp + propositional_var;
						ICompositeNode cnode = NodeModelUtils.findActualNodeFor(element);
						clause = "";
		
						for (INode node : cnode.getChildren())
						{
////							System.out.print(node.getSemanticElement().);
//							if(node.getSemanticElement() instanceof Parameter)
//							{
//								tmpoperand = ((Parameter)node.getSemanticElement()).getName();
//								if(sub == null)
//									sub = tmpoperand;
//								else
//									obj = tmpoperand;
//								System.out.print("Sub/Obj (Parameter) comparison = " + tmpoperand);
//								
//							}else if(node.getSemanticElement() instanceof Variable)
//							{
//								tmpoperand = ((Variable)node.getSemanticElement()).getVar();
//								if(sub == null)
//									sub = tmpoperand;
//								else
//									obj = tmpoperand;
//								System.out.print("Sub/Obj (Variable) comparison = " + tmpoperand);
//							}else if (node.getSemanticElement() instanceof ComparisonOperator)
//							{
//								operator = ((ComparisonOperator)node.getSemanticElement()).getCompop();
//								System.out.print("Operator = " + operator);
//							}

							// System.out.println(node.getText());
							clause = clause + node.getText();
//							if(node.getText().contains(""))
						}System.out.println();
						

						propositional_vars_hm.put(propositional_var, clause.trim());
						booleanexp_obj.addPropostion(clause.trim(), "Bool");

						// Generate an arithmetic boolean expression if the clause is a comparison type
						if(((Clause) element).getAdj() instanceof Comparison)
						{
							NP sub = ((Clause) element).getSub();
							ComparisonOperator compop = ((Comparison)((Clause) element).getAdj()).getOp();
							NP obj = ((Comparison)((Clause) element).getAdj()).getObj();
							arith_booleanexp_obj = new BooleanExpression(FORMULA);
							
							String sub_str = null;
							String obj_str = null;
							
							sub_str = sub.getClass().getMethod("getName", (Class<?>[])null).invoke(sub, (Object[] )null).toString();
							sub_str = sub_str.toString().replaceAll(" +", "");// variables must be free of spaces
							obj_str = obj.getClass().getMethod("getName", (Class<?>[])null).invoke(obj, (Object[] )null).toString();
							obj_str = obj_str.toString().replaceAll(" +", "");// variables must be free of spaces		
							
							System.out.println(sub.getClass() + " " + VariableImpl.class.getSimpleName());
							if(sub.getClass().getSimpleName().equals(VariableImpl.class.getSimpleName()) ||
									sub.getClass().getSimpleName().equals(ParameterImpl.class.getSimpleName())||
									sub.getClass().getSimpleName().equals(InParameterImpl.class.getSimpleName())||
									sub.getClass().getSimpleName().equals(OutParameterImpl.class.getSimpleName()))// Variable.class || sub_sub instanceof Parameter)
							{
		
								arith_booleanexp_obj.addPropostion(sub_str, sub_str, "Int");
								
							}
							
							if(obj.getClass().getSimpleName().equals(VariableImpl.class.getSimpleName()) ||
									obj.getClass().getSimpleName().equals(ParameterImpl.class.getSimpleName())||
									obj.getClass().getSimpleName().equals(InParameterImpl.class.getSimpleName())||
									obj.getClass().getSimpleName().equals(OutParameterImpl.class.getSimpleName()))// Variable.class || sub_obj instanceof Parameter)
							{
								
								arith_booleanexp_obj.addPropostion(obj_str, obj_str, "Int");
							}
							System.out.printf("sub : %s\tobj :%s\n", sub_str, obj_str);
							String arith_booleanexp = String.format("(= %s (%s %s %s))", 
									booleanexp_obj.getPV(), getCompSymbol(compop.getCompop()), 
									sub_str, obj_str);
							System.out.println("Arithmetic boolean expression: " + arith_booleanexp);
							
							arith_booleanexp_obj.setBooleaExp(arith_booleanexp);
							arith_booleanexps_hm.put(booleanexp_obj.getPV(), arith_booleanexp_obj);
						}

					} 
					else if (element instanceof SubConjTemporal)
					{
						booleanexp = booleanexp + " " + ((SubConjTemporal) element).getSubconj() + " ";
						System.out.print(((SubConjTemporal) element).getSubconj());
						z3symbols_stack.conditionalPush("=>", SUBCONJUNCTIVE_TYPE);
						
					} 
					else if (element instanceof SubConjConditional)
					{
						booleanexp = booleanexp + " " + "=>"/*((SubConjConditional) element).getSubconj()*/ + " ";
						System.out.print(((SubConjConditional) element).getSubconj() + " ");
						z3symbols_stack.conditionalPush("=>"/*((SubConjConditional) element).getSubconj()*/, SUBCONJUNCTIVE_TYPE);
					} 
					else if (element instanceof SubConjCause)
					{
						booleanexp = booleanexp + " " + ((SubConjCause) element).getSubconj() + " ";
						System.out.print(((SubConjCause) element).getSubconj() + " ");
						z3symbols_stack.conditionalPush("=>", SUBCONJUNCTIVE_TYPE);
					} 
					else if (element instanceof LogicalOP)
					{
						booleanexp = booleanexp + " " + ((LogicalOP) element).getLop() + " ";
						System.out.print(((LogicalOP) element).getLop() + " ");
						z3symbols_stack.push(((LogicalOP) element).getLop(), COOCONJUNCTIVE_TYPE);
					}
//					else if(element instanceof ComplexClause)
//					{
//						ICompositeNode complexclause_cnode = NodeModelUtils.findActualNodeFor(element);
//						for (INode node : complexclause_cnode.getChildren())
//						{
//							if (node.getGrammarElement() instanceof Keyword)
//							{
//								if (node.getText().equals(")"))
//								{
//									z3symbols_stack.dontwait_flag =  true;
//									z3symbols_stack.push(")", RIGHT_PARENTHESIS);
//									booleanexp = booleanexp + " " + node.getText();
//									System.out.print(" " + node.getText());
//								}else if (node.getText().equals("("))
//								{
//									//z3symbols_stack.push("(", LEFT_PARENTHESIS);
//									booleanexp = booleanexp + node.getText() + " ";
//									System.out.print(" " +node.getText());
//								}
//							}
//						}
//					}else if(element instanceof Comparison)
//					{
						
//						operator = ((Comparison) element).getOp().getCompop();
//						NP left_operand = ((Comparison) element).getObj();
//						ICompositeNode left_operand_cnode = NodeModelUtils.findActualNodeFor(left_operand);
//						System.out.println("Comparison: " );
//						for (INode node : left_operand_cnode.getChildren())
//						{
//							System.out.println(node.getText());
//						}
//						//System.out.println("comparison left operand : " + ((Comparison) element).getObj().getClass().getMethod("getName",  (Class<?>[])null).invoke(null));
//						System.out.println("class name:  " + element.getClass().getSuperclass().get);
//						//NP right_operand = (NP)element.getClass().getSuperclass().getSuperclass().getMethod("getObj", (Class<?>[])null).invoke(null);
//					}
//					else if(element instanceof NP)
//					{
//						if(sub == null)
//						{
//							ICompositeNode sub_cnode = NodeModelUtils.findActualNodeFor(element);
//							for (INode node : sub_cnode.getChildren())
//							{
//								sub = node.getText();//System.out.println(node.getText());
//							}
//						}else
//						{
//							ICompositeNode obj_cnode = NodeModelUtils.findActualNodeFor(element);
//							for (INode node : obj_cnode.getChildren())
//							{
//								obj = node.getText();
//								//System.out.println(node.getText());
//							}
//						}			
//					}else if (element instanceof Clause)
//					{System.out.printf("Clause comparison: %s %s %s\n", sub, operator, obj);
//						if(operator != null)
//						{
//							System.out.printf("Clause comparison: %s %s %s\n", sub, operator, obj);
//							operator = null;
//							sub = null;
//							obj = null;
//						}
//						
//					}
					
					if (element instanceof ComplexSpec)
					{
						ICompositeNode cnode = NodeModelUtils.findActualNodeFor(element);
						for (INode node : cnode.getChildren())
						{
							if (node.getGrammarElement() instanceof Keyword)
							{
								if (node.getText().equals(","))
								{
									booleanexp = booleanexp + node.getText();
									System.out.print(" " +node.getText());
									// z3symbols_stack.conditionalPush(",",
									// OTHER_TYPE);
								} else if (node.getText().equals("]"))
								{
									//z3symbols_stack.push("]", RIGHT_BRACKET);
									booleanexp = booleanexp + " " + node.getText();
									System.out.print(" " +node.getText());
								} else if (node.getText().equals("["))
								{
									booleanexp = booleanexp + node.getText() + " ";
									System.out.print(" " +node.getText());
								} /*else if (node.getText().equals(")"))
								{
									z3symbols_stack.push(")", RIGHT_PARENTHESIS);
									booleanexp = booleanexp + " " + node.getText();
									System.out.print(" " + node.getText());
								} else if (node.getText().equals("("))
								{
									//z3symbols_stack.push("(", LEFT_PARENTHESIS);
									booleanexp = booleanexp + node.getText() + " ";
									System.out.print(" " +node.getText());
								} */else if (node.getText().equals("."))
								{
									booleanexp = booleanexp + node.getText();
									System.out.print(node.getText());
								}

							}
						}
					}

				} // end of while
		
				// End of a requirement specification
				z3symbols_stack.conditionalPush(".", DOT_TYPE);
				propositional_formula = z3symbols_stack.pop();//ResaToZ3SmtLibFormat.generateZ3Assertion(z3symbols_stack.pop());
				
				//propositional_formula = propositional_formula + String.format("\n%s", z3reqfurmula);
				propositional_formulas_list.add(propositional_formula); // add the boolean expression into the container
				booleanexp_obj.setBooleaExp(propositional_formula);

				return "SUCCESS";
			}

			private String getCompSymbol(String compop) 
			{

				switch(compop.replaceAll("^ +| +$|_|( )+", "$1"))
				{
				case "equalto":
					return "=";
				case "lessthan":
					return "<";
				case "greaterthan":
					return ">";
				case "lessthanorequalto":
					return "<=";
				case "greaterthanorequalto":
					return ">=";
				default:
					System.out.println("unknown comparison operator: " + compop);
				}
				return null;
			}
			
			
		});
	}
	
	public HashMap<String, List<String>> checkEquivalenceOfPropostions()
	{
		// construct the clauses hash map
	//	List<String> pv_list = new ArrayList<String>();
		for (BooleanExpression booleanexp : booleanexp_list)
		{
			for(String pv:booleanexp.getPropositions().keySet())
			{
				//String key = name.toString();
				String value = booleanexp.getPropositions().get(pv).toString();
				System.out.printf("%15s = %s\n", pv, value);
				
				//create a hash map based on clauses of the propositions
				// the value of the map contains variables of the same meaning
					//resolve negations
					if(value.contains(" not "))
					{
						System.out.println("negation detected!");
						value = value.replace("not ", "");
						pv = "(" + "not " + pv + ")"; 
						System.out.println(pv + " = " + value);
					}
					//construct logical form: remove modals, verb to
					value = value.replace(" shall ", " ");
					value = value.replace(" be ", " ");
					value = value.replace(" is ", " ");
					
					//remove type tags, like :system, ..
			//		System.out.println("before" + value);
					value = value.replaceAll("(\\\")(.+)(\\\"):[a-z]+", "$2");
				//	System.out.println("after" + value);
					meaning_hm.putIfAbsent(value, new ArrayList<String>());
					meaning_hm.get(value).add(pv);
				//meaning_hm.putIfAbsent(value, new ArrayList<String>());
				//}//else
				//System.out.println("Requirement symbol entry!");
			}
		}
		
		
		// Determine equivalence clauses	

	    for(Iterator<Map.Entry<String, List<String>>> it = meaning_hm.entrySet().iterator(); it.hasNext(); ) {
	        Entry<String, List<String>> entry = it.next();

	        if(entry.getValue().size() <= 1) {
	          it.remove();
	        }
	      }
	    
//	    for(String key:meaning_hm.keySet())
//		{
//			System.out.printf("key: %s\tvalue: %s\n", key, meaning_hm.get(key));
//			//the list of equal propositions should be greater than 1
//			if(meaning_hm.get(key).size() <= 1)
//				meaning_hm.remove(key);
//		}
		
		return meaning_hm;
	}
	
	public List<String> equiavlentBooleanExpressions()
	{
		
		// Determine equivalence clauses	
		List<String> equivalent_propositions_list= new ArrayList<String>();
		for(String meaning:meaning_hm.keySet())
		{
			String equal_pvs = "";
			//create the equal prop variables
			for(String p:meaning_hm.get(meaning))
			{
				//System.out.print(p);
				equal_pvs = equal_pvs + p + " ";
			} 
			equivalent_propositions_list.add("( = " +  equal_pvs + ")");
		}
		
		return equivalent_propositions_list;
	}
	

}
